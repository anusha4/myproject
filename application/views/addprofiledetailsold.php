

<!--header start-->

<!--header end-->
<!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Add Profile Details
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">

                        <div id="wizard">
                            

                            <section>
                            
                            

                        <div class="personal_details">
                            <h2>Personal Details</h2>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">First Name</label>
                                    <input type="text" class="form-control" placeholder="Enter First Name" id="fname">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Last Name</label>
                                    <input type="text" class="form-control" placeholder="Enter Last Name" id="lname">
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="text" class="form-control" placeholder="Enter Email" id="emailid">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Date of Birth</label>
                                    <input type="text" class="form-control default-date-picker" placeholder="Enter Date of Birth" id="dob">
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Mobile Number</label>
                                    <input type="text" onkeypress="return isNumberKey(event)" class="form-control" placeholder="Enter Mobile Number" id="mobileno">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Telephone</label>
                                    <input type="text" onkeypress="return isNumberKey(event)" class="form-control" placeholder="Enter Telephone" id="telephone">
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Company Name</label>
                                    <input type="text" class="form-control" placeholder="Enter Company Name" id="companyname">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Designation</label>
                                    <input type="text" class="form-control" placeholder="Enter Designation" id="designation">
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Industry</label>
                                    <select class="form-control" id="industrytype">
                                        <option value="0">Select Industry</option>
                                        <option value="Industry011">Industry011</option>
                                        <option value="Industry021">Industry021</option>
                                        <option value="Industry031">Industry031</option>
                                        <option value="Industry041">Industry041</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Specality</label>
                                    <input type="text" class="form-control" placeholder="Enter no of Vacancies" id="speciality">
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">About me</label>
                                    <textarea type="text" class="form-control" placeholder="Enter no of Vacancies" id="aboutme"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Profile Pic</label>
                                    <input type="file" class="form-control" placeholder="Enter no of Vacancies" id="form-register-photo">
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>



                            <div class="qualification_div">
                                <h2>Qualification and Skills</h2>
                                <div class="col-md-6">
                                    <div class="degree_full_div">
                                    <div class="form-group">
                                        <label class="control-label">College Name</label>
                                        <input type="text" class="form-control" placeholder="Enter Name of College">
                                        <div class="little_gap"></div>
                                        <label class="control-label">Degree</label>
                                        <input type="text" class="form-control" placeholder="Enter Degree">
                                        <div class="little_gap"></div>
                                        <label class="control-label" style="display: block;">Duration</label>
                                        <div class="col-md-6 pl0">
                                            <input type="text" class="form-control default-date-picker" placeholder="From">
                                        </div>
                                        <div class="col-md-6 pr0">
                                            <input type="text" class="form-control default-date-picker" placeholder="to">
                                        </div>
                                        <div class="clear"></div>
                                        <button class="btn btn-danger btn-xs btn_delte_div disabled_btn"><i><span class="fa fa-trash-o"></span></i></button>
                                    </div>
                                    <div class="large_gap"></div>
                                    </div>
                                    <div class="text-right"><button class="btn btn-info btn-xs add_degree">Add Degree</button></div>
                                </div>



                                <div class="col-md-6">
                                    <div class="skill_full_div">
                                    <div class="form-group">
                                        <label class="control-label">Skill Name</label>
                                        <input type="text" class="form-control" placeholder="Enter no of Vacancies">
                                        <div class="little_gap"></div>
                                        <label class="control-label">Level</label>
                                        <select class="form-control">
                                            <option value="0">Select Level</option>
                                            <option value="Low" selected>Low</option>
                                            <option value="Moderate">Moderate</option>
                                            <option value="High">High</option>
                                        </select>
                                        <button class="btn btn-danger btn-xs btn_delte_div disabled_btn"><i><span class="fa fa-trash-o"></span></i></button>
                                    </div>
                                    <div class="large_gap"></div>
                                    </div>
                                    <div class="text-right"><button class="btn btn-info btn-xs add_Skills">Add Skills</button></div>
                                </div>
                                <div class="clear"></div>
                                </div>





                                <div class="experience_div">
                                    <h2>Experience</h2>
                                    <div class="experience_full_div">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label">Company Name</label>
                                                <input type="text" class="form-control" placeholder="Enter Name of College">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Degree</label>
                                                <input type="text" class="form-control" placeholder="Enter Degree">
                                            </div>
                                            <div class="clear"></div>
                                            <div class="little_gap"></div>
                                            <div class="col-md-6">
                                                <label class="control-label" style="display: block;">Duration</label>
                                                <div class="col-md-6 pl0">
                                                    <input type="text" class="form-control default-date-picker" placeholder="From">
                                                </div>
                                                <div class="col-md-6 pr0">
                                                    <input type="text" class="form-control default-date-picker" placeholder="to">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Location</label>
                                                <input type="text" class="form-control" placeholder="Enter Location">
                                            </div>
                                            <div class="clear"></div>
                                            <button class="btn btn-danger btn-xs btn_delte_div disabled_btn"><i><span class="fa fa-trash-o"></span></i></button>
                                        </div>
                                    <div class="large_gap"></div>
                                    </div>
                                    <div class="text-right"><button class="btn btn-info btn-xs add_experience">Add Experience</button></div>
                                </div>




                                <div class="portfolio_socials_div">
                                    <h2>Portfolio and Socials</h2>
                                    <div class="col-md-6">
                                        <div class="portfolio_full_div">
                                            <div class="form-group">
                                                <label class="control-label">Project Name</label>
                                                <input type="text" class="form-control" placeholder="Enter Project Name">
                                                <div class="little_gap"></div>
                                                <label class="control-label">Project Short Description</label>
                                                <textarea class="form-control" placeholder="Project Short Description"></textarea>
                                                <div class="little_gap"></div>
                                                <label class="control-label" style="display: block;">Duration</label>
                                                <div class="col-md-6 pl0">
                                                    <input type="text" class="form-control default-date-picker" placeholder="From">
                                                </div>
                                                <div class="col-md-6 pr0">
                                                    <input type="text" class="form-control default-date-picker" placeholder="to">
                                                </div>
                                                <div class="clear"></div>
                                                <div class="little_gap"></div>
                                                <label class="control-label">Project Name</label>
                                                <input type="file" class="form-control" id="portfolioimage" name="portfolioimage">
                                                <button class="btn btn-danger btn-xs btn_delte_div disabled_btn"><i><span class="fa fa-trash-o"></span></i></button>
                                            </div>
                                            <div class="large_gap"></div>
                                        </div>
                                        <div class="text-right"><button class="btn btn-info btn-xs add_portfolio">Add Experience</button></div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="social_div">
                                            <div class="form-group">
                                                <label class="control-label">Facebook</label>
                                                <input type="text" class="form-control" placeholder="Enter Facebook Profile URL">
                                                <div class="little_gap"></div>
                                                <label class="control-label">Twitter</label>
                                                <input type="text" class="form-control" placeholder="Enter Twitter Profile URL">
                                                <div class="little_gap"></div>
                                                <label class="control-label">Linkedin</label>
                                                <input type="text" class="form-control" placeholder="Enter Linkedin Profile URL">
                                                <div class="little_gap"></div>
                                                <label class="control-label">Google-Plus</label>
                                                <input type="text" class="form-control" placeholder="Enter Google-Plus Profile URL">
                                                <div class="little_gap"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>   









                                
                             <!--    <div class="form-group">
                                    <label class="control-label">Skills</label>
                                    <input id="tags_1" type="text" class="tags form-control" value="HTML5,CSS3,BootStrap,JQuery" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Qualification</label>
                                    <div class="">
                                        <select multiple name="e9" id="e1" class="populate" style="width: 100%;">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                        </select>
                                    </div>
                                </div> -->
                                
                            </div>
    
                            <div class="clear"></div>

                                <div class="large_gap"></div>
                                <div class="text-center message_div">
                                <span class="Error_msg1"></span>
                              <span class="sucess_msg1"></span>
                              </div>

                          <div class="little_gap"></div>     
                                <div class="text-center">
                                    <button type="button" class="btn btn-primary btn-lg" id="recruiter_personal">Save Profile</button>
                                </div>
                                <div class="large_gap"></div>
                            </section>

                            

                           
                        </div>
                    </div>
                </section>
                
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->
<!--right sidebar start-->
<div class="right-sidebar">


            
                
                
                    <div class="target-sell">
                    </div>
                
            
            
         
            
            
        

</div>
<!--right sidebar end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->


<script src="assets/js/jquery.js"></script>
<script src="assets/js/jquery-1.10.2.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/easypiechart/jquery.easypiechart.js"></script>
<script src="assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/bootstrap-switch.js"></script>

<script type="text/javascript" src="assets/js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="assets/js/jquery-multi-select/js/jquery.multi-select.js"></script>


<script src="assets/js/jquery-tags-input/jquery.tagsinput.js"></script>
<script src="assets/js/select2/select2.js"></script>
<script src="assets/js/select-init.js"></script>


<!--common script init for all pages-->
<script src="assets/js/scripts.js"></script>
<script src="assets/js/toggle-init.js"></script>
<script src="assets/js/advanced-form.js"></script>

<script>
    $(function ()
    {
        $("#wizard").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft"
        });

        $("#wizard-vertical").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            stepsOrientation: "vertical"
        });
    });
</script>

<script type="text/javascript">
    $(document).on('click','.add_degree',function(){
        var degree_div = '<div class="form-group"><label class="control-label">College Name</label><input type="text" class="form-control" placeholder="Enter Name of College"><div class="little_gap"></div><label class="control-label">Degree</label><input type="text" class="form-control" placeholder="Enter Degree"><div class="little_gap"></div><label class="control-label" style="display: block;">Duration</label><div class="col-md-6 pl0"><input type="text" class="form-control default-date-picker" placeholder="From"></div><div class="col-md-6 pr0"><input type="text" class="form-control default-date-picker" placeholder="to"></div><div class="clear"></div><button class="btn btn-danger btn-xs btn_delte_div disabled_btn"><i><span class="fa fa-trash-o"></span></i></button></div><div class="large_gap"></div>';
        $('.degree_full_div').append(degree_div);
         $('.default-date-picker').datepicker();
     
        var degree_div_length =$('.degree_full_div .form-group').length;
        if (degree_div_length == 1) {
            $('.degree_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.degree_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        }
    });  
    $(document).on('click','.degree_full_div .form-group .btn_delte_div',function(){
        $(this).parent().next('.large_gap').remove();
        $(this).parent().remove().next('.large_gap').remove();
        var degree_div_length =$('.degree_full_div .form-group').length;
        if (degree_div_length == 1) {
            $('.degree_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.degree_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        }
    });


    $(document).on('click','.add_Skills',function(){
        var skills_div = '<div class="form-group"><label class="control-label">Skill Name</label><input type="text" class="form-control" placeholder="Enter no of Vacancies"><div class="little_gap"></div><label class="control-label">Level</label><select class="form-control"><option value="0">Select Level</option><option value="Low" selected>Low</option><option value="Moderate">Moderate</option><option value="High">High</option></select><button class="btn btn-danger btn-xs btn_delte_div disabled_btn"><i><span class="fa fa-trash-o"></span></i></button></div><div class="large_gap"></div>';
        $('.skill_full_div').append(skills_div);
        var skills_div_length = $('.skill_full_div .form-group').length;
        if (skills_div_length == 1) {
            $('.skill_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.skill_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        }
    });
    $(document).on('click','.skill_full_div .form-group .btn_delte_div',function(){
        $(this).parent().next('.large_gap').remove();
        $(this).parent().remove().next('.large_gap').remove();
        var skills_div_length =$('.skill_full_div .form-group').length;
        if (skills_div_length == 1) {
            $('.skill_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.skill_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        }
    });   

    $(document).on('click','.add_experience',function(){
        var experience_div = '<div class="form-group"><div class="col-md-6"><label class="control-label">Company Name</label><input type="text" class="form-control" placeholder="Enter Name of College"></div><div class="col-md-6"><label class="control-label">Degree</label><input type="text" class="form-control" placeholder="Enter Degree"></div><div class="clear"></div><div class="little_gap"></div><div class="col-md-6"><label class="control-label" style="display: block;">Duration</label><div class="col-md-6 pl0"><input type="text" class="form-control default-date-picker" placeholder="From"></div><div class="col-md-6 pr0"><input type="text" class="form-control default-date-picker" placeholder="to"></div></div><div class="col-md-6"><label class="control-label">Location</label><input type="text" class="form-control" placeholder="Enter Location"></div><div class="clear"></div><button class="btn btn-danger btn-xs btn_delte_div disabled_btn"><i><span class="fa fa-trash-o"></span></i></button></div><div class="large_gap"></div>';
        $('.experience_full_div').append(experience_div);
        $('.default-date-picker').datepicker();
        var experience_div_length = $('.experience_full_div .form-group').length;
        if (experience_div_length == 1) {
            $('.experience_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.experience_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        }
    });
    $(document).on('click','.experience_full_div .form-group .btn_delte_div',function(){
        $(this).parent().next('.large_gap').remove();
        $(this).parent().remove().next('.large_gap').remove();
        var experience_div_length =$('.experience_full_div .form-group').length;
        if (experience_div_length == 1) {
            $('.experience_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.experience_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        }
    });

    $(document).on('click','.add_portfolio',function(){
        var portfolio_div = '<div class="form-group"><label class="control-label">Project Name</label><input type="text" class="form-control" placeholder="Enter Project Name"><div class="little_gap"></div><label class="control-label">Project Short Description</label><textarea class="form-control" placeholder="Project Short Description"></textarea><div class="little_gap"></div><label class="control-label" style="display: block;">Duration</label><div class="col-md-6 pl0"><input type="text" class="form-control default-date-picker" placeholder="From"></div><div class="col-md-6 pr0"><input type="text" class="form-control default-date-picker" placeholder="to"></div><div class="clear"></div><div class="little_gap"></div><label class="control-label">Project Name</label><input type="file" class="form-control"><button class="btn btn-danger btn-xs btn_delte_div disabled_btn"><i><span class="fa fa-trash-o"></span></i></button></div><div class="large_gap"></div>';
        $('.portfolio_full_div').append(portfolio_div);
        $('.default-date-picker').datepicker();
        var portfolio_div_length = $('.portfolio_full_div .form-group').length;
        if (portfolio_div_length == 1) {
            $('.portfolio_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.portfolio_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        } 
    });
    $(document).on('click','.portfolio_full_div .form-group .btn_delte_div',function(){
        $(this).parent().next('.large_gap').remove();
        $(this).parent().remove().next('.large_gap').remove();
        var portfolio_div_length =$('.portfolio_full_div .form-group').length;
        if (portfolio_div_length == 1) {
            $('.portfolio_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.portfolio_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        } 
    });
</script>









<!--add profile script start by koti-->

<script type="text/javascript">

$('#recruiter_personal').on('click',function(){

            var fname = $('#fname').val();  
            var lname = $('#lname').val();
            var emailid = $('#emailid').val();
            var dob = $('#dob').val();
            var mobileno = $('#mobileno').val();
            var telephone = $('#telephone').val();
            var companyname = $('#companyname').val();
            var designation = $('#designation').val();
            var industrytype = $('#industrytype').val();
            var speciality = $('#speciality').val();
            var aboutme = $('#aboutme').val();

            var profile = $('#form-register-photo').val();

            var recruiterid = '<?php echo $this->session->userdata('recruiter_id'); ?>';

            var re = /^[ A-Za-z-']*$/;
            var re1 = /^[ 0-9-+]*$/;


           if(fname.length==0 || lname.length==0 || dob.length==0 || mobileno.length==0 || telephone.length==0 || companyname.length==0 || designation.length==0 || industrytype.length==0 || speciality.length==0 || aboutme.length==0)
           {
             $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Fill Personal Details!");
           }

            else if(fname.length>50 || lname.length>50)  
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i>maximum of 50 characters allowed!");


            }
            else if(!re.test(fname))
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> FirstName Accepts Only Singlequotes And Alphanumarics!");

            }
            else if(!re.test(lname))
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Lastname Accepts Only Singlequotes And Alphanumarics!");

            }
            
            else if(mobileno.length>14 || mobileno.length<14)
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid Mobile Number!");

            }
            else if(!re1.test(mobileno))
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Lastname Accepts Only Singlequotes And Alphanumarics!");

            }
            
            else if(telephone.length>14 || telephone.length<14)
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid Telephone Number!");

            }
             else if(!re1.test(telephone))
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Mobile Number Accepts Only Plus And hiphen Symbols!");

            }
           
            else if(companyname.length>100 || companyname.length<5)
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid Company Name!");

            }
            else if(!re.test(companyname))
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company name Accepts Only Plus And hiphen Symbols!");

            }
            else if(designation.length>100 || designation.length<5)
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid Designation!");

            }
            else if(!re.test(designation))
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Designation Accepts Only Plus And hiphen Symbols!");

            }
            else if(industrytype.length>100 || industrytype.length<5)
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid Industry Type!");

            }
            // else if(!re.test(industrytype))
            // {
            //     $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Industry Type Accepts Only Plus And hiphen Symbols!");

            // }
            else if(speciality.length>100 || speciality.length<5)
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid Specality!");

            }
            else if(!re.test(speciality))
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Specality Accepts Only Plus And hiphen Symbols!");

            }
            else if(aboutme.length>100 || aboutme.length<5)
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid Company Name!");

            }
            else if(!re.test(aboutme))
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Aboutme Accepts Only Plus And hiphen Symbols!");

            }

          
            else
            {

            //Add Degree
            var divlength=$('.qualification_div .col-md-6 .degree_full_div .form-group').length;

            var div1length=$('.qualification_div .col-md-6 .skill_full_div .form-group').length;


            localStorage.setItem("divlength",divlength);
            localStorage.setItem("div1length",div1length);
            $.ajax({

              type: "post",
              url: "<?php echo base_url();  ?>api/recruiter_profileupdate.php",
              data: {fname:fname,lname:lname,emailid:emailid,dob:dob,mobileno:mobileno,telephone:telephone,companyname:companyname,designation:designation,industrytype:industrytype,speciality:speciality,aboutme:aboutme,recruiterid:recruiterid},

              success:function(data){
                jsondata = JSON.parse(data);


                if(jsondata.status == 1)
                {
                   var recruiterid = jsondata.recruiterid; 
                   localStorage.setItem("recruiterid",recruiterid); 

                       profilePicture(recruiterid);
                       recursively_ajax(0);

                       //alert("Success");
                  
                }

                else
                {
                  alert("Failure");
                }
              }

          });
             
            // recursively1_ajax(0);

            // addexperience();
             
            // recruiterportfolio();

            // personaldetails();


         }
        

        });
  </script>



  <script type="text/javascript">
      function personaldetails()
             {
                var fname = $('#fname').val();  
            var lname = $('#lname').val();
            var emailid = $('#emailid').val();
            var dob = $('#dob').val();
            var mobileno = $('#mobileno').val();
            var telephone = $('#telephone').val();
            var companyname = $('#companyname').val();
            var designation = $('#designation').val();
            var industrytype = $('#industrytype').val();
            var speciality = $('#speciality').val();
            var aboutme = $('#aboutme').val();

            var profile = $('#form-register-photo').val();

            var recruiterid = '<?php echo $this->session->userdata('recruiter_id'); ?>';

            $.ajax({

              type: "post",
              url: "<?php echo base_url();  ?>api/recruiter_profileupdate.php",
              data: {fname:fname,lname:lname,emailid:emailid,dob:dob,mobileno:mobileno,telephone:telephone,companyname:companyname,designation:designation,industrytype:industrytype,speciality:speciality,aboutme:aboutme,recruiterid:recruiterid},

              success:function(data){
                jsondata = JSON.parse(data);


                if(jsondata.status == 1)
                {
                   var recruiterid = jsondata.recruiterid; 
                   localStorage.setItem("recruiterid",recruiterid); 

                       profilePicture(recruiterid);

                       alert("Success");
                  
                }

                else
                {
                  alert("Failure");
                }
              }

          });
        }
    
  </script>


<!-- Profile Image -->

  <script type="text/javascript">

   function profilePicture(recruiterid)
    {
     var form_data = new FormData();  
    var file_data = $("#form-register-photo").prop("files")[0]; 
    for(var loopvar=0;loopvar<$("#form-register-photo").prop("files").length;loopvar++)
    {
       var file_data = $("#form-register-photo").prop("files")[loopvar]; 
      form_data.append("file[]", file_data);
    }
    if($("#form-register-photo").prop("files").length==0)
      form_data.append("file[]", "");

  
      form_data.append("recruiterid", recruiterid);
      
      $.ajax({
            url: "api/recruiter_profileupdateimage.php",
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(php_script_response){
              var jsondata=JSON.parse(php_script_response);
                if(jsondata.status==1)
                {
                    //alert("success");
                }
                else
                {
                   // alert("failed");
                }
        }
      });

    }

</script>


<script type="text/javascript">
// $('.recruiterexperience').on('click',function(){

function addexperience()
{
    
var experiencedivid=$('.experience_div .experience_full_div .form-group').length;
 //alert(experiencedivid);

 for(var expid=0;expid<experiencedivid;expid++)
{
    var proceedtonextflag=true;
    var form_data = new FormData();
    var recruiteridd = $('#recruiteridd').val();


    var companyname = $('.experience_div  .experience_full_div .form-group:eq('+expid+')').children('.col-md-6:eq(0)').children('input').val();
    var website = $('.experience_div  .experience_full_div .form-group:eq('+expid+')').children('.col-md-6:eq(1)').children('input').val();
    
    var startdate = $('.experience_div  .experience_full_div .form-group:eq('+expid+')').children('.col-md-6:eq(2)').children('.col-md-6:eq(0)').children('input').val();
    var enddate = $('.experience_div  .experience_full_div .form-group:eq('+expid+')').children('.col-md-6:eq(2)').children('.col-md-6:eq(1)').children('input').val();

    var location = $('.experience_div  .experience_full_div .form-group:eq('+expid+')').children('.col-md-6:eq(3)').children('input').val();

    if(companyname.length==0 || website.length==0 || startdate.length==0 || enddate.length==0 || location.length==0)
         {

            proceedtonextflag=false;
            

         }
}

 //alert(proceedtonextflag);


if(proceedtonextflag)
{
    

for(var expid=0;expid<experiencedivid;expid++)
{
    var form_data = new FormData();
    var recruiteridd = $('#recruiteridd').val();
    var companyname = $('.experience_div  .experience_full_div .form-group:eq('+expid+')').children('.col-md-6:eq(0)').children('input').val();
    var website = $('.experience_div  .experience_full_div .form-group:eq('+expid+')').children('.col-md-6:eq(1)').children('input').val();
    
    var startdate = $('.experience_div  .experience_full_div .form-group:eq('+expid+')').children('.col-md-6:eq(2)').children('.col-md-6:eq(0)').children('input').val();
    var enddate = $('.experience_div  .experience_full_div .form-group:eq('+expid+')').children('.col-md-6:eq(2)').children('.col-md-6:eq(1)').children('input').val();

    var location = $('.experience_div  .experience_full_div .form-group:eq('+expid+')').children('.col-md-6:eq(3)').children('input').val();

    var recruiterid = '<?php echo $this->session->userdata('recruiter_id'); ?>';

      

        
                    form_data.append("companyname", companyname);
                    form_data.append("website", website);
                    form_data.append("location", location);

                    form_data.append("startdate", startdate);
                    form_data.append("enddate", enddate);
                  
                     form_data.append("recruiteridd", recruiterid);
                      $.ajax({
                            url: "<?php base_url() ?>api/recruiter_experience.php",
                            dataType: 'text',  // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,                         
                            type: 'post',
                            success: function(php_script_response){
                              var jsondata=JSON.parse(php_script_response);

                              if(jsondata.status==1)
                              {
                                recruiterportfolio();

           
                               // alert("experience suceess");

                              }
                              else
                              {
                                alert("experience failure");

                              }

                              
                        } 
                      });
                  }

}

 else
{

    $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Fill All Experience Details!");
//alert("please fill all experience details the details");

}
//}
}
// });



</script>

<script type="text/javascript">
    
function recruiterportfolio()
{

var portfoliodiv=$('.portfolio_socials_div .col-md-6:eq(0) .portfolio_full_div .form-group').length;
 //alert(portfoliodiv);

 for(var portfolio=0;portfolio<portfoliodiv;portfolio++)
{

    var proceedtonextflag=true;
    var projectname = $('.portfolio_socials_div  .col-md-6:eq(0) .portfolio_full_div').children('.form-group:eq('+portfolio+')').children('input:eq(0)').val();
    var projectdesc = $('.portfolio_socials_div  .col-md-6:eq(0) .portfolio_full_div').children('.form-group:eq('+portfolio+')').children('textarea').val();

     var startdate = $('.portfolio_socials_div  .col-md-6:eq(0) .portfolio_full_div').children('.form-group:eq('+portfolio+')').children('.col-md-6:eq(0)').children('input').val();

    var enddate = $('.portfolio_socials_div  .col-md-6:eq(0) .portfolio_full_div').children('.form-group:eq('+portfolio+')').children('.col-md-6:eq(1)').children('input').val();

    

    var facebook = $('.social_div').children('.form-group').children('input:eq(0)').val();
    var twitter = $('.social_div').children('.form-group').children('input:eq(1)').val();
    var linkedin = $('.social_div').children('.form-group').children('input:eq(2)').val();
    var googleplus = $('.social_div').children('.form-group').children('input:eq(3)').val();
      //  else if(fname.length>50 || lname.length>50)  
           //  {
           //      $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i>maximum of 50 characters allowed!");


           //  }
           //  else if(!re.test(fname))
           //  {
           //      $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> FirstName Accepts Only Singlequotes And Alphanumarics!");

           //  }

    if(projectname.length==0 || projectdesc.length==0 || startdate.length==0 || enddate.length==0 || facebook.length==0 ||twitter.lenght==0 || linkedin.length==0 || googleplus.length==0)
         {

            proceedtonextflag=false;
            

         }

   
    
}


 if(proceedtonextflag)
{

   

for(var portfolio=0;portfolio<portfoliodiv;portfolio++)
{
    var form_data = new FormData();
    var recruiteridd = $('#recruiteridd').val();

    var projectname = $('.portfolio_socials_div  .col-md-6:eq(0) .portfolio_full_div').children('.form-group:eq('+portfolio+')').children('input:eq(0)').val();
    var projectdesc = $('.portfolio_socials_div  .col-md-6:eq(0) .portfolio_full_div').children('.form-group:eq('+portfolio+')').children('textarea').val();

     var startdate = $('.portfolio_socials_div  .col-md-6:eq(0) .portfolio_full_div').children('.form-group:eq('+portfolio+')').children('.col-md-6:eq(0)').children('input').val();

    var enddate = $('.portfolio_socials_div  .col-md-6:eq(0) .portfolio_full_div').children('.form-group:eq('+portfolio+')').children('.col-md-6:eq(1)').children('input').val();

    

    var facebook = $('.social_div').children('.form-group').children('input:eq(0)').val();

    var twitter = $('.social_div').children('.form-group').children('input:eq(1)').val();
    var linkedin = $('.social_div').children('.form-group').children('input:eq(2)').val();
    var googleplus = $('.social_div').children('.form-group').children('input:eq(3)').val();


    var recruiteridd = '<?php echo $this->session->userdata('recruiter_id'); ?>';

   
   
    
                    form_data.append("projectname", projectname);
                    form_data.append("projectdesc", projectdesc);
                    form_data.append("facebook", facebook);
                    form_data.append("twitter", twitter);
                    form_data.append("linkedin", linkedin);
                    form_data.append("googleplus", googleplus);

                    form_data.append("startdate", startdate);
                    form_data.append("enddate", enddate);
                  
                     form_data.append("recruiteridd", recruiteridd);
                      $.ajax({
                            url: "<?php base_url() ?>api/recruiter_portfolio.php",
                            dataType: 'text',  // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,                         
                            type: 'post',
                            success: function(php_script_response){
                              var jsondata=JSON.parse(php_script_response);
                              if(jsondata.status==1)
                              {
                                // alert("Success");
                                 personaldetails();

                                var lastjob_slno=jsondata.lastid;

                                 var form_data = new FormData();

                                
                                var lastjob_slno=lastjob_slno;


                                // for(var loopvar=0;loopvar<$("#portfolioimage").prop("files").length;loopvar++)
                                // {
                                   var file_data = $("#portfolioimage").prop("files")[0]; 
                                  form_data.append("file[]", file_data);
                                // }

                                
                                if(file_data.length==0)
                                  form_data.append("file[]", "");
                              else
                                form_data.append("file[]", file_data);

                              // alert(file_data);

                              
                                  form_data.append("lastjob_slno", lastjob_slno);
                                  
                                  $.ajax({
                                        url: "api/recruiter_jobportfolioimage.php",
                                        dataType: 'text',  // what to expect back from the PHP script, if anything
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,                         
                                        type: 'post',
                                        success: function(php_script_response){
                                          var jsondata=JSON.parse(php_script_response);
                                            if(jsondata.status==1)
                                            {
                                                //alert("success");
                                            }
                                            else
                                            {
                                               // alert("failed");
                                            }
                                    }
                                  });
                              }
                              else
                              {
                                alert("Failure");

                              }

                              
                        } 
                      });
                  }
}
else
{
    $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Fill Portfolio Details!");
    // alert('please fill portfolio');
}
}

</script>



<script type="text/javascript">

    function recursively_ajax(loopedu){



        var divlength = localStorage.getItem("divlength");
         //alert(divlength+"  "+loopedu);


         for(var expid=0;expid<divlength;expid++)
            {
                var proceedtonextflag=true;
                var form_data = new FormData();


      var form_data = new FormData();
      
       var recruiteridd = '<?php echo $this->session->userdata('recruiter_id'); ?>';

        // var divlength=$('.qualification_div .col-md-6 .degree_full_div .form-group').length;

          var college = $('.qualification_div  .col-md-6:eq(0) .degree_full_div').children('.form-group:eq('+expid+')').children('input:eq(0)').val();
          var degree = $('.qualification_div  .col-md-6:eq(0) .degree_full_div').children('.form-group:eq('+expid+')').children('input:eq(1)').val();


         var startdate = $('.qualification_div  .col-md-6:eq(0) .degree_full_div').children('.form-group:eq('+expid+')').children('.col-md-6:eq(0)').children('input').val();

         var enddate = $('.qualification_div  .col-md-6:eq(0) .degree_full_div').children('.form-group:eq('+expid+')').children('.col-md-6:eq(1)').children('input').val();

         if(college.length==0 || degree.length==0 || startdate.length==0 || enddate.lenght==0)
         {
            proceedtonextflag=false;

         }
    }

    //alert(proceedtonextflag);
    if(proceedtonextflag)
    {

       


         var form_data = new FormData();
      
       var recruiteridd = '<?php echo $this->session->userdata('recruiter_id'); ?>';

        // var divlength=$('.qualification_div .col-md-6 .degree_full_div .form-group').length;

          var college = $('.qualification_div  .col-md-6:eq(0) .degree_full_div').children('.form-group:eq('+loopedu+')').children('input:eq(0)').val();
          var degree = $('.qualification_div  .col-md-6:eq(0) .degree_full_div').children('.form-group:eq('+loopedu+')').children('input:eq(1)').val();


         var startdate = $('.qualification_div  .col-md-6:eq(0) .degree_full_div').children('.form-group:eq('+loopedu+')').children('.col-md-6:eq(0)').children('input').val();

         var enddate = $('.qualification_div  .col-md-6:eq(0) .degree_full_div').children('.form-group:eq('+loopedu+')').children('.col-md-6:eq(1)').children('input').val();

            form_data.append("degree", degree);
            form_data.append("college", college);
            // form_data.append("location", location);
            form_data.append("startdate", startdate);
            form_data.append("enddate", enddate);
          
             form_data.append("recruiteridd", recruiteridd);
              $.ajax({
                    url: "<?php base_url() ?>api/recruiter_qualification.php",
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,                         
                    type: 'post',
                    success: function(php_script_response){
                      var jsondata=JSON.parse(php_script_response);

                      if(jsondata.status==1)
                      {

                        recursively1_ajax(0);

            

                      }
                      else
                      {
                        alert('Failure');

                      }



                      loopedu++;
                      if(loopedu<divlength) 
                      {
                        
                 recursively_ajax(loopedu);   
              }
             }
                      
                });
          }
          else
          {
            alert('Please Fill Degree Details');
          }
          }

              // });
              
         // } 




          function recursively1_ajax(loopedu){
        var div1length = localStorage.getItem("div1length");
         //alert(div1length+"  "+loopedu);

         for(var expid=0;expid<div1length;expid++)
            {
                var proceedtonextflag=true;
                

      var form_data = new FormData();
       var recruiteridd = '<?php echo $this->session->userdata('recruiter_id'); ?>';

     
        var skill = $('.skill_full_div').children('.form-group:eq('+expid+')').children('input:eq(0)').val();
        var skilllevel = $('.skill_full_div').children('.form-group:eq('+expid+')').children('select').val();
        
       if(skill.length==0 || skilllevel.length==0)
         {
            proceedtonextflag=false;

         }
     }

   //alert(proceedtonextflag);
     if(proceedtonextflag)
     {

      

         var form_data = new FormData();
       var recruiteridd = '<?php echo $this->session->userdata('recruiter_id'); ?>';

        var skill = $('.about_skills_div  .skills_div:eq('+loopedu+')').children('.col-sm-6:eq(0)').children('.form-group').children('input').val();
         var skilllevel = $('.about_skills_div  .skills_div:eq('+loopedu+')').children('.col-sm-6:eq(1)').children('.form-group').children('input').val();


     
            form_data.append("skill", skill);
            form_data.append("skilllevel", skilllevel);
            
             form_data.append("recruiteridd", recruiteridd);


              $.ajax({
                    url: "<?php base_url() ?>api/recruiter_skills.php",
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,                         
                    type: 'post',
                    success: function(php_script_response){
                      var jsondata=JSON.parse(php_script_response);

                      if(jsondata.status==1)
                      {
                        addexperience();
             
            
                        //alert("Success");

                      }
                      else
                      {
                        alert("Failure");

                      }

                      loopedu++;
                      if(loopedu<div1length) 
                      {
                 recursively1_ajax(loopedu);   
            
                      
                } 
            }
              });
          }
          else
          {
            alert('Please Enter Skills');
          }
          //}


              
          } 

          </script>

          <script type="text/javascript">
      
      function isNumberKey(evt){
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && charCode != 43 && charCode != 45 && (charCode < 48 || charCode > 57))
            return false;
          return true;
      }

    </script>


    

<!--add profile script end by koti-->











</body>

</html>
