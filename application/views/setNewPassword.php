<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">

    <title>Set New Password</title>

    <!--Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/main.css">

</head>

  <body class="login-body">

    <div class="container">

      <div class="form-signin">
        <h2 class="form-signin-heading">Set New Password</h2>
        <div class="login-wrap">
            <div class="user-login-info">
            
                <input type="password" class="form-control userId" placeholder="New Password" name="userId" autofocus>
                <div class="little_gap"></div>
                <input type="password" class="form-control password" placeholder="Confirm New Password" name="password">
            </div>
            
            <span class="Error_msg"></span>
            <span class="sucess_msg"></span>
            <button class="btn btn-lg btn-login btn-block signIn_Btn" type="submit">Submit</button>

            

        </div>
  </div>
          <!-- Modal -->
          
          <!-- modal -->

     

    </div>



    <!-- Placed js at the end of the document so the pages load faster -->

    <!--Core js-->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
      
    </script>

  </body>


</html>
