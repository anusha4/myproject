<div class="popup text-center">
    <span class="clse_btn"><i class="fa fa-times"></i></span>
    <p>Sure you want to delete?</p>
    <button class="btn btn-info">Delete</button>
    <button class="btn btn-danger">Cancel</button>
</div>

<!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->
            <section class="panel">
                    <header class="panel-heading">
                        Jobs Posted By Recruiter
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <!-- page start-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="adv-table">
                                    <table class="display table table-bordered table-striped" id="dynamic-table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Job Title</th>
                                                <th>Comapany</th>
                                                <th>Posted On</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><p class="pos_j_desg">Web UI/UX Developer</p> 
                                                    <span class="pos_j_lctn"><i class="fa fa-map-marker"></i> Hyderabad, Telangana</span>
                                                </td>
                                                <td>Company Name</td>
                                                <td><p class="pstd_dte"><i class="fa fa-calendar"></i> 04/11/2016</p> <p class="pstd_tme"><i class="fa fa-clock-o"></i> 11:35 AM</p></td>
                                                <td><button class="btn btn-info">View</button> <button class="btn btn-danger confo_popup">Delete</button> <button class="btn btn-success">Applied Uers (5)</button></td>
                                           </tr>
                                           <tr>
                                                <td>2</td>
                                                <td><p class="pos_j_desg">Web Developer</p> 
                                                    <span class="pos_j_lctn"><i class="fa fa-map-marker"></i> Hyderabad, Telangana</span>
                                                </td>
                                                <td>Company Name1 Pvt Limited</td>
                                                <td><p class="pstd_dte"><i class="fa fa-calendar"></i> 04/11/2016</p> <p class="pstd_tme"><i class="fa fa-clock-o"></i> 11:35 AM</p></td>
                                                <td><button class="btn btn-info">View</button> <button class="btn btn-danger confo_popup">Delete</button> <button class="btn btn-success">Applied Uers (14)</button></td>
                                           </tr>
                                           <tr>
                                                <td>3</td>
                                                <td><p class="pos_j_desg">Web Front-End Developer</p> 
                                                    <span class="pos_j_lctn"><i class="fa fa-map-marker"></i> Hyderabad, Telangana</span>
                                                </td>
                                                <td>Company Name2 Pvt Ltd</td>
                                                <td><p class="pstd_dte"><i class="fa fa-calendar"></i> 04/11/2016</p> <p class="pstd_tme"><i class="fa fa-clock-o"></i> 11:35 AM</p></td>
                                                <td><button class="btn btn-info">View</button> <button class="btn btn-danger confo_popup">Delete</button> <button class="btn btn-success">Applied Uers (21)</button></td>
                                           </tr>
                                           <tr>
                                                <td>4</td>
                                                <td><p class="pos_j_desg">Web Back-End Developer</p> 
                                                    <span class="pos_j_lctn"><i class="fa fa-map-marker"></i> Hyderabad, Telangana</span>
                                                </td>
                                                <td>Company Name Technologies Pvt</td>
                                                <td><p class="pstd_dte"><i class="fa fa-calendar"></i> 04/11/2016</p> <p class="pstd_tme"><i class="fa fa-clock-o"></i> 11:35 AM</p></td>
                                                <td><button class="btn btn-info">View</button> <button class="btn btn-danger confo_popup">Delete</button> <button class="btn btn-success">Applied Uers (7)</button></td>
                                           </tr>
                                           <tr>
                                                <td>5</td>
                                                <td><p class="pos_j_desg">Web UI/UX Developer</p> 
                                                    <span class="pos_j_lctn"><i class="fa fa-map-marker"></i> Hyderabad, Telangana</span>
                                                </td>
                                                <td>Company Name</td>
                                                <td><p class="pstd_dte"><i class="fa fa-calendar"></i> 04/11/2016</p> <p class="pstd_tme"><i class="fa fa-clock-o"></i> 11:35 AM</p></td>
                                                <td><button class="btn btn-info">View</button> <button class="btn btn-danger confo_popup">Delete</button> <button class="btn btn-success">Applied Uers (10)</button></td>
                                           </tr>
                                            
                                       </tbody>
                                   </table>
                               </div>
                            </div>
                        </div>
                        <!-- page end-->
                    </div>
                </section>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->

<script type="text/javascript" src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>

<script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>
<!--common script init for all pages-->
<script src="assets/js/scripts.js"></script>

<!--dynamic table initialization -->
<script src="assets/js/dynamic_table_init.js"></script>

<!--common script init for all pages-->

<!--script for this page only-->


<script type="text/javascript">
    $(document).on('click','.confo_popup',function(){
        $('.overlay').show();
        $('.popup').fadeIn();
    });
    $('.popup .btn-danger, .popup span, .overlay').on('click',function(){
        $('.overlay').fadeOut();
        $('.popup').hide(); 
    });
</script>
</body>

<!-- Mirrored from bucketadmin.themebucket.net/calendar.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 31 Aug 2017 10:56:23 GMT -->
</html>
