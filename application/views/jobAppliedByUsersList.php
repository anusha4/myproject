
 <div class="popup text-center">
    <span class="clse_btn"><i class="fa fa-times"></i></span>
    <p>Reason For Rejection</p>
    <input type="hidden" id="jobid">
    <input type="hidden" id="userid">


    <textarea placeholder="Message..." class="form-control" id="messagecount"></textarea>
    <button class="btn btn-info" id="rejectusers">Submit</button>
    <button class="btn btn-danger">Cancel</button>
</div>
<!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->
            <section class="panel">
                    <header class="panel-heading">
                        <?php echo count($candidate_jobdetails); ?> Condidate Apply On <b><?php echo base64_decode($this->uri->segment(2)); ?></b> Job
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <!-- page start-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="adv-table">
                                    <table class="display table table-bordered table-striped td_img_circle" id="dynamic-table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Profile Pic</th>
                                                <th>Name Designation</th>
                                                <th>Job Status</th>
                                                <th>Higest Qualification</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>



                                            <?php
                                            $sno=1;
                                            for($i=0;$i<count($candidate_jobdetails);$i++)
                                            {
                                            ?>
                                            <tr>
                                                <td><?php echo $sno; ?></td>

                                                <td>

                                                <img src="<?php echo base_url()?>assets/images/<?php if($candidate_jobdetails[$i]['user_image']!='') echo $candidate_jobdetails[$i]['user_image']; else echo 'defaultprofile.png'; ?>" alt="">

                                                


                                                </td>
                                                <td><p class="pos_j_desg"><?php echo $candidate_jobdetails[$i]['user_fname']; ?> &nbsp; <?php echo $candidate_jobdetails[$i]['user_lname']; ?></p> 
                                                    <span class="pos_j_lctn"><i class="fa fa-suitcase"></i> &nbsp;  <?php echo $candidate_jobdetails[$i]['user_lname']; ?></span>
                                                </td>
                                                <td><?php echo $candidate_jobdetails[$i]['user_workststus']; ?></td>
                                                <td><p class="pstd_dte"><i class="fa fa-graduation-cap"></i> <?php echo 'Mca' ?></p></td>

                                                <td>
                                                

                                                <a href="<?php echo base_url(); ?>userprofile/<?php echo base64_encode($candidate_jobdetails[$i]['joblist_id']); ?>/<?php echo base64_encode($candidate_jobdetails[$i]['jobapplicants_userid']); ?>">
                                                <button class="btn btn-info">View Profile</button>
                                                </a>

                                                <button class="btn btn-danger rejectlists" data-jobid="<?php echo $candidate_jobdetails[$i]['joblist_id'] ?>" data-userid="<?php echo $candidate_jobdetails[$i]['jobapplicants_userid'] ?>">Reject</button> 

                                                <button class="btn btn-success shortlistusers" data-jobid="<?php echo $candidate_jobdetails[$i]['joblist_id'] ?>" data-userid="<?php echo $candidate_jobdetails[$i]['jobapplicants_userid'] ?>">Shortlist</button></td>
                                            </tr>



                                            <?php
                                            $sno++;
                                             }
                                             ?>


                                         

                                       </tbody>
                                   </table>
                               </div>
                            </div>
                        </div>
                        <!-- page end-->
                    </div>
                </section>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/data-tables/DT_bootstrap.js"></script>
<!--common script init for all pages-->
<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>

<!--dynamic table initialization -->
<script src="<?php echo base_url(); ?>assets/js/dynamic_table_init.js"></script>

<!--common script init for all pages-->

<!--script for this page only-->


<script type="text/javascript">
    $(document).on('click','.confo_popup',function(){
        $('.overlay').show();
        $('.popup').fadeIn();
    });
    $('.popup .btn-danger, .popup span, .overlay').on('click',function(){
        $('.overlay').fadeOut();
        $('.popup').hide(); 
    });
</script>

<script type="text/javascript">

$('.rejectlists').on('click',function()
{

    var jobid1=$(this).data('jobid');
    var userid1=$(this).data('userid');

    $(this).addClass('confo_popup');

    $('#jobid').val(jobid1);
    $('#userid').val(userid1);


});

 
</script>


<script type="text/javascript">
    
    $('.shortlistusers').on('click',function()
    {

        alert('hi');

    var jobid=$(this).data('jobid');
    var userid=$(this).data('userid');

    // alert(jobid);
    // alert(userid);
    
    $.ajax({

              type: "post",
              url: "<?php echo base_url();  ?>api/usersupdatestatus.php",
              data: {jobid:jobid,userid:userid},

              success:function(data){
            var jsondata = JSON.parse(data);

        
                if(jsondata.status == 1)
                {
                   
                        alert("success");
                  
                   
                }

                else
                {
                  alert("Failure");
                }
             
               
             }

                
         });


    });

</script>

</body>

<!-- Mirrored from bucketadmin.themebucket.net/calendar.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 31 Aug 2017 10:56:23 GMT -->
</html>
