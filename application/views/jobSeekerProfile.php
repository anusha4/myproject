<?php
//print_r($userprofile);
?> 

<section id="main-content">
        <section class="wrapper">
        <!-- page start-->
            <section class="panel">
                    <header class="panel-heading">
                        Jobseeker Profile
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
<div class="panel-body" style="padding-top: 0">
    <!-- page start-->

    <div class="row">
  


<div class="bg_gradient">
    <div class="container_self">
        <div class="profile_div">
            <div class="col-md-1 p0">
                <img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="set_img" alt="">
            </div>
            <div class="col-md-9 up_p_b_desc">
                <h3 class="up_username"><?php echo $userprofile[0]['user_fname']; ?> <?php echo $userprofile[0]['user_lname'];?> <span>(Web and Mobile UI Developer)</span></h3>
                <p class="up_userlocation"><i class="fa fa-map-marker"></i> <?php echo $userprofile[0]['user_location']; ?>, India 500005.</p>
                <p class="up_Nationality">Nationality : <span>Indian.</span></p>
                
                <div class="social_links">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-google-plus"></i></a>
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                </div>
            </div>
            <div class="col-md-2 padding0 text-center">
                <h3 class="Jobs_appl">Applied Jobs</h3>
                <span class="jobs_app_count"><?php echo count($userprofiledata); ?></span>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<div class="profile_desc">
    <div class="container_self">
        <div class="col-md-8">
            <div class="up_div_boxes">
                
                <div class="Personal_div up_div" id="up_div1">
                    <div class="Desc_h3">
                        <div class="col-md-9 pl0"><h3 class="">Personal Information</h3></div> 
                        <div class="col-md-3 pr0 text-right"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="col-md-4"><p>Full Name</p></div> <div class="col-md-8"><p>: &nbsp; <?php echo $userprofile[0]['user_fname']; ?> <?php echo $userprofile[0]['user_lname'];?></p></div> <div class="clear"></div>
                    <div class="col-md-4"><p>Date Of Birth</p></div> <div class="col-md-8"><p>: &nbsp; <?php echo $userprofile[0]['user_dateofbirth'];?></p></div> <div class="clear"></div>
                    <div class="col-md-4"><p>Email</p></div> <div class="col-md-8"><p>: &nbsp; <?php echo $userprofile[0]['user_email'];?></p></div> <div class="clear"></div>
                    <div class="col-md-4"><p>Gender</p></div> <div class="col-md-8"><p>: &nbsp; <?php echo $userprofile[0]['user_gender'];?></p></div> <div class="clear"></div>
                    <div class="col-md-4"><p>Mobile Number</p></div> <div class="col-md-8"><p>: &nbsp; <?php echo $userprofile[0]['user_mobile'];?></p></div> <div class="clear"></div>
                    <div class="col-md-4"><p>Alternate Mobile Number</p></div> <div class="col-md-8"><p>: &nbsp; +91 9874563211</p></div> <div class="clear"></div>
                    <div class="col-md-4"><p>Resume</p></div> <div class="col-md-8"><p>: &nbsp; Resume.docx  &nbsp; <span>Last Update on: 23-7-2017</span></p></div> <div class="clear"></div>
                    <div class="col-md-12"><p class="personal_info"><?php echo $userprofile[0]['user_personalbackground'];?></p></div><div class="clear"></div>
                </div>


                <div class="Education_div up_div" id="up_div2">
                    <div class="Desc_h3">
                        <div class="col-md-9 pl0"><h3 class="">Education</h3></div> 
                        <div class="col-md-3 pr0 text-right"><!-- <p><i class="fa fa-edit"></i> Edit</p> --></div>
                        <div class="clear"></div>
                    </div>



                    <?php
                    if(count($userqualification)!=0)
                    {
                    for($i=0;$i<count($userqualification);$i++)
                    {
                    ?>
                    <div class="education_field">
                        <div class="col-md-12">
                            <div class="up_e_year"><?php echo $userqualification[$i]['userqualification_startingyear']; ?> - <?php echo $userqualification[$i]['userqualification_passingyear']; ?></div> 
                            <div class="up_e_details">
                                <h3><?php echo $userqualification[$i]['userqualification_college']; ?> <span>(<?php echo $userqualification[$i]['userqualification_specialization']; ?>)</span></h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente obcaecati, deserunt aperiam atque inventore expedita.</p>

                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
    
                    <?php
                    }
                }
                else
                {
                    ?>
                    <div class="education_field">
                        <div class="col-md-12">
                            
                            <div class="up_e_details">

                                 <p>Qualification Details Not Available</p>

                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <?php

                }
                    ?>


                    


                    <div class="skills_div">
                    <div class="Desc_h3">
                        <div class="col-md-9 pl0"><h3 class="">Skills</h3></div> 
                        <div class="col-md-3 pr0 text-right"><!-- <p><i class="fa fa-edit"></i> Edit</p> -->
                            
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="col-md-12">
                    <ul class="skills_list">


                    <?php

                    if(count($userskills)!=0)
                    {
                    for($i=0;$i<count($userskills);$i++)
                    {
                    ?>
                        <li><?php echo $userskills[$i]['userskills_skillid']; ?></li>
                        <?php
                    }
                }
                    else
                    {
                    ?>
                    <div class="education_field">
                        <div class="col-md-12">
                            
                            <div class="up_e_details">

                                 <p>Skills Details Not Available</p>

                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <?php
                    }
                  
                    ?>


                        
                    </ul>
                    </div>
                    <div class="clear"></div>
                    </div>
                </div>


                <div class="Experience_div up_div" id="up_div3">
                    <div class="Desc_h3">
                        <div class="col-md-9 pl0"><h3 class="">Experience</h3></div> 
                        <div class="col-md-3 pr0 text-right"><!-- <p><i class="fa fa-edit"></i> Edit</p> --></div>
                        <div class="clear"></div>
                    </div>


                        
                    <?php 
                    if(count($userexperience)!=0)
                    {
                    for($i=0;$i<count($userexperience);$i++)
                    {
                    ?>

                    <div class="education_field">
                        <div class="col-md-12">
                            <div class="up_e_year"><?php echo $userexperience[$i]['experience_startyear']; ?> - <?php echo $userexperience[$i]['experience_endyear']; ?></div> 

                            <div class="up_e_details">
                                <h3><?php echo $userexperience[$i]['experience_companyname']; ?> <span>(<?php echo $userexperience[$i]['experience_designation']; ?>)</span></h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente obcaecati, deserunt aperiam atque inventore expedita.</p>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <?php
                }
            }
            else
            {
                ?>
                <div class="education_field">
                        <div class="col-md-12">
                          

                            <div class="up_e_details">

                            <p>Education Details Not Available</p>
                               
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <?php

            }
                ?>

                    <!-- <div class="education_field">
                        <div class="col-md-12">
                            <div class="up_e_year">2011 - 2013</div> 
                            <div class="up_e_details">
                                <h3>IndoSoft Technologies <span>(Web Developer)</span></h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente obcaecati, deserunt aperiam atque inventore expedita.</p>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>


                    <div class="education_field">
                        <div class="col-md-12">
                            <div class="up_e_year">2009 - 2011</div> 
                            <div class="up_e_details">
                                <h3>Innervalley Technologies<span>(NodeJs Developer)</span></h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente obcaecati, deserunt aperiam atque inventore expedita.</p>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div> -->


                </div>

                
                <div class="projects_div up_div" id="up_div4">
                    <div class="Desc_h3">
                        <div class="col-md-9 pl0"><h3 class="">Projects</h3></div> 
                        <div class="col-md-3 pr0 text-right"><!-- <p><i class="fa fa-edit"></i> Edit</p> --></div>
                        <div class="clear"></div>
                    </div>
                    <div class="col-md-12 ">
                    <div class="col-md-6 pl0">
                    <div class="img_protf">
                        <img src="<?php echo base_url()?>assets/images/p1.jpg" class="set_img">
                        <div class="overlay_portfolio">
                            <div class="port_text">
                                <h3>Title</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, similique!</p>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6 pr0"><div class="img_protf"><img src="<?php echo base_url()?>assets/images/p2.jpg" class="set_img">
                    <div class="overlay_portfolio">
                            <div class="port_text">
                                <h3>Title</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, similique!</p>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="clear"></div>
                    <div class="col-md-6 pl0"><div class="img_protf"><img src="<?php echo base_url()?>assets/images/p3.jpg" class="set_img">
                    <div class="overlay_portfolio">
                            <div class="port_text">
                                <h3>Title</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, similique!</p>
                            </div>
                        </div></div></div>
                    <div class="col-md-6 pr0"><div class="img_protf"><img src="<?php echo base_url()?>assets/images/p4.jpg" class="set_img">
                    <div class="overlay_portfolio">
                            <div class="port_text">
                                <h3>Title</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, similique!</p>
                            </div>
                        </div></div></div>
                    <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>  


            </div>
        </div>
        <div class="col-md-4">
            <ul class="ref_links">
                <li class="ul_li_1 active_up">Personal</li>
                <li class="ul_li_2">Education & Skills</li>
                <li class="ul_li_3">Experience</li>
                <li class="ul_li_4">Projects</li>
                <li class="ul_li_5">Sample</li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>


    </div>
    <!-- page end-->
</div>
                </section>
        <!-- page end-->
        </section>
    </section>




    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/easypiechart/jquery.easypiechart.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-switch.js"></script>


<!--common script init for all pages-->
<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>



<script type="text/javascript">
$('#up_div1').show();
$('.ref_links li').on('click',function(){
    $(this).addClass('active_up').siblings().removeClass('active_up');
});
$('.ul_li_1').on('click',function(){
    $('#up_div1').show().siblings().hide();
});
$('.ul_li_2').on('click',function(){
    $('#up_div2').show().siblings().hide();
});
$('.ul_li_3').on('click',function(){
    $('#up_div3').show().siblings().hide();
});
$('.ul_li_4').on('click',function(){
    $('#up_div4').show().siblings().hide();
});

</script>