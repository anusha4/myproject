<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->            
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a href="<?php echo base_url(); ?>index" class="active">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
           
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-building" aria-hidden="true"></i>
                    <span>Company</span>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo base_url(); ?>addCompany">Add Company</a></li>
                    <li><a href="#">Agreement Companies</a></li>
                    
                </ul>
            </li>

             <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-suitcase"></i>
                    <span>Job</span>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo base_url(); ?>postajob">Job Post</a></li>
                    <li><a href="<?php echo base_url(); ?>joblistByRecruiter">Job Details</a></li>
                    
                </ul>
            </li>

            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span>Interview Rounds</span>
                </a>
                <ul class="sub">
                    <li><a href="#">Interview</a></li>
                    <li><a href="#">Offer Letter </a></li>
                    
                </ul>
            </li>
            
        </ul>
        </div>        
<!-- sidebar menu end-->
    </div>
</aside>