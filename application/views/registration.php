<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.html">

    <title>Login</title>

    <!--Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
  <body class="login-body">

    <div class="container">

      <div class="form-signin">
        <h2 class="form-signin-heading">registration now</h2>
        <div class="login-wrap">

            <div class="little_gap"></div>
            <input type="text" class="form-control reg_name" placeholder="FullName" id="name" >
            <div class="little_gap"></div>
            <input type="email" class="form-control reg_Email" placeholder="Email" id="emailid" autofocus>
            <div class="little_gap"></div>
            <input type="text" class="form-control reg_mobile" placeholder="Mobile" id="mobile" >
            <div class="little_gap"></div>
            <input type="password" class="form-control reg_pwd" placeholder="Password" id="password">
            <div class="little_gap"></div>

            <!-- <input type="password" class="form-control reg_cpwd" placeholder="Re-type Password" id="confirmpassword"> -->

            <label class="checkbox">
                <input type="checkbox" class="r_checkbox" value="agree this condition"> I agree to the Terms of Service and Privacy Policy
            </label>
            <span class="Error_msg"></span>
            <span class="sucess_msg"></span>
            <button class="btn btn-lg btn-login btn-block regBtn" type="submit" onclick="ValidatePassword()">Register</button>

            <div class="registration">
                Already Registered.
                <a class="" href="<?php echo base_url('login');?>">
                    Login
                </a>
            </div>

        </div>

      </div>

    </div>


    <!-- Placed js at the end of the document so the pages load faster -->

    <!--Core js-->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>


    
    <script>

     function ValidatePassword() {


      
       
                    var passwords = document.getElementById("password");
                    var email = $('#emailid').val();
                    var name = $('#name').val();
                    var mobile = $('#mobile').val();
              
                    // var confirmpassword=$('#confirmpassword').val();
                    var password=$('#password').val();
                    var atpos = email.indexOf("@");
                    var dotpos = email.lastIndexOf(".");


                    var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,10}$/;
                    // var regex1= /^[0-9-+]+$/;



                    if (email.length == 0 || name.length == 0 || mobile.length == 0|| password.length == 0 ) {
                        $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Please Fill All Fields!");
                        
                        $('.sucess_msg').hide(); 

                    }

             
                  else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) 
                          {
                            //alert("please enter valid email");

                            $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid EmailId!");
                            $('.sucess_msg').hide(); 
                                  
                          }

                  

                   else if (!regex.test(passwords.value))
                     {

                         $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Password must be 8 characters including 1 uppercase letter, 1 special character, alphanumeric characters! and spaces are not allowed");
                         $('.sucess_msg').hide(); 

                     
                        //alert("Password  Should have Uppercase lowercase with one number and one symbol");
                    }
                     else if(password.length<8)
                    {
                         $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Password Minimum Length 8 Characters!");
                         $('.sucess_msg').hide(); 
                       
                    }
                    else if(mobile.length!=10){
                       $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> required 10 digits, match requested format!");
                         $('.sucess_msg').hide(); 

                    }

                    // else if(password!=confirmpassword)
                    // {

                    //     $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Password Not matched!");
                    //     $('.sucess_msg').hide(); 
                    //     //alert('Password Not matched');
                    // }
                    else if($('.r_checkbox').prop("checked") == false)
                    {
                      $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Please Select Terms And Conditions to Proceed!");
                        $('.sucess_msg').hide(); 


                    }
                    else
                    {


                var uemail=$("#emailid").val();
                $.ajax({
                type:"post",
                url:"<?php echo base_url();?>api/checkuser.php",
                data:{email:uemail},
                success:function(data){
                if(data == 1)
                {
                
                  $('#emailid').css('border-color', 'red');

                  $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Already Registered!");

                   $('.sucess_msg').hide(); 

                   $('#emailid').focus();
          
                
                }
                else if(data != 1){

                     $('.sucess_msg').fadeIn().html("<i class='fa fa-check'></i> Available");
                     $('.Error_msg').hide(); 

            setTimeout(function(){  
                        $.ajax({
                type : "post",
                url : "<?php echo base_url();  ?>api/recruter_registration.php",
                data : {email:email,password:password,name:name,mobile:mobile},
                success:function(data)
                {
                 jsondata = JSON.parse(data);
                 if(jsondata.status == 1)
                 {
                     

                $('.Error_msg').hide(); 
                $('.sucess_msg').fadeIn().html("<i class='fa fa-check'></i> Account Created Successfully!");

                
                 }
                 else
                 {
                      $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Failed!");
                 }
                }
               });
                        }, 3000);

                }


                }
              });
               }
    }
    </script>

   <!--  <script type="text/javascript">


             $("#emailid").focusout(function(){
                var emailid=$("#emailid").val();
               //alert(emailid);

               for(var i=0; i<emailid.length; i++)
                          {

                         // alert( result[i] );

                          if(emailid[i]=='@')
                          {
                           
                            var emailidd=emailid.split('@');
                           
                          


                            if((emailidd[0].length)<3)
                            {

                                $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Pleaes enter Valid Emailid!");
                                 $('.sucess_msg').hide(); 
                             
                             
                            }
                            else
                            {


              var uemail=$("#emailid").val();
                $.ajax({
                type:"post",
                url:"<?php echo base_url();?>api/checkuser.php",
                data:{email:uemail},
                success:function(data){
                

                if(data == 1)
                {
              
                  $('#emailid').css('border-color', 'red');

                  $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Already Registered!");

                   $('.sucess_msg').hide(); 

                   $('#emailid').focus();
          
                
                }
                else if(data != 1){

                     $('.sucess_msg').fadeIn().html("<i class='fa fa-check'></i> Available");
                     $('.Error_msg').hide(); 
                     // return false;

                 
                
                }
                else
                {
             
                }

                 
                }
 
              });
              }
            }
          }
          });



            </script> -->

  </body>

<!-- Mirrored from bucketadmin.themebucket.net/registration.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 31 Aug 2017 10:57:25 GMT -->
</html>