

<!--header start-->

<!--header end-->
<!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Profile
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
<div class="panel-body">
<div id="wizard">
<section>

<div class="popup_followers folovers">
<h3>Followers  &nbsp;-  125</h3>
    <ul>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllw">follow</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllwing"><i class="fa fa-check"></i> following</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllwing"><i class="fa fa-check"></i> following</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllwing"><i class="fa fa-check"></i> following</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllw">follow</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllw">follow</button></div>
            <div class="clear"></div>
        </li>
    </ul>
    <span class="close_r_p_popup"><i class="fa fa-times"></i></span>
</div>




<div class="popup_followings folovers">
<h3>Following  &nbsp;-  125</h3>
    <ul>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllwing"><i class="fa fa-check"></i> following</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllwing"><i class="fa fa-check"></i> following</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllwing"><i class="fa fa-check"></i> following</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllwing"><i class="fa fa-check"></i> following</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllwing"><i class="fa fa-check"></i> following</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllwing"><i class="fa fa-check"></i> following</button></div>
            <div class="clear"></div>
        </li>
    </ul>
    <span class="close_r_p_popup"><i class="fa fa-times"></i></span>
</div>


<div class="popup_U_blockd folovers">
<h3>Blocked  &nbsp;-  125</h3>
    <ul>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllw"> Un-block</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllw"> Un-block</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllw"> Un-block</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllw"> Un-block</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllw"> Un-block</button></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="col-md-2"><img src="<?php echo base_url()?>assets/images/avatar1.jpg" class="img100" alt=""></div>
            <div class="col-md-6 padding0">
                <h4>UseraNAme</h4>
                <p>Web developer at SomeTechCompany</p>
            </div>
            <div class="col-md-4 text-right"><button class="Flwr_fllw"> Un-block</button></div>
            <div class="clear"></div>
        </li>
    </ul>
    <span class="close_r_p_popup"><i class="fa fa-times"></i></span>
</div>





    <div class="main-wrapper">
        <div class="main">
            
        
            <div class="">
    <div class="row">
    <div class="col-md-12">
      <div class="job_Desc_div1">
          
          <div class="col-md-8 pl0">
            <div class="col-md-2 pl0"><img src="<?php echo base_url() ?>assets/images/avatar1.jpg" class="img100"></div>
            <div class="col-md-10 pl0">
                <h4 class="cp_compname">Recruiter Name</h4>
                <p class="cp_compof">Senior Web Developer</p>
                <p class="cp_estb">Since Aug 4, 2001</p>
                <p class="cp_complocat">UAE, Address</p>
            </div>
          </div>
          <div class="col-md-4 pl0">
              <ul class="cp_com_details">
                  <li><i class="fa fa-phone"></i> &nbsp; : +91 9874654130</li>
                  <li><i class="fa fa-envelope"></i> &nbsp; : <a href="mailto:info@company.com">info@company.com</a></li>
                  <li><i class="fa fa-globe"></i> &nbsp; : <a href="https://companynae.com" target="_blank">https://companynae.com</a></li>
              </ul>
              <ul class="cp_social">
                  <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                  <li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
              </ul>
          </div>
          <div class="clear"></div>
          <hr>
          <div class="col-md-8 pl0">
          <span class="recruiter_ffb U_Fr_list">Followers (125)</span> &nbsp; &nbsp; 
          <span class="recruiter_ffb U_flling_list">Following (34)</span> &nbsp;  &nbsp;
          <span class="recruiter_ffb U_blckd_list">Blocked Users(4)</span>
          </div>
          <div class="col-md-4 pr0 text-right">
            <button class="edit_btn">Edit profile</button>
          </div>
          <div class="clear"></div>
      </div>  
    </div>
    
    <div class="col-md-8">
        <div class="job_Desc_div2">
            <h3>About Me</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, necessitatibus nihil voluptatem, aliquid incidunt sint quasi libero obcaecati molestias illo ut repudiandae, optio deserunt modi nam. Autem dolorem officia distinctio! Aspernatur repellat asperiores quas eveniet itaque voluptas blanditiis corrupti recusandae. Tempora quis dignissimos tempore dicta quo doloribus sapiente iste repellendus, commodi fuga non quia ratione debitis excepturi asperiores, porro sit voluptatibus aut incidunt suscipit hic reprehenderit possimus? Architecto ullam rerum tenetur, quod unde ipsum labore excepturi fugiat ipsa incidunt assumenda asperiores quam suscipit culpa doloribus debitis accusantium corporis quasi pariatur, iste sit. Natus fugiat atque laborum debitis earum totam dicta?</p>
           
            <h3>Skills</h3>
            <ul>
                <li><i class="fa fa-check"></i> &nbsp; Html5 &nbsp; - &nbsp; 45% </li>
                <li><i class="fa fa-check"></i> &nbsp; Html5 &nbsp; - &nbsp; 45% </li>
                <li><i class="fa fa-check"></i> &nbsp; Html5 &nbsp; - &nbsp; 45% </li>
                <li><i class="fa fa-check"></i> &nbsp; Html5 &nbsp; - &nbsp; 45% </li>
                <li><i class="fa fa-check"></i> &nbsp; Html5 &nbsp; - &nbsp; 45% </li>
                <li><i class="fa fa-check"></i> &nbsp; Html5 &nbsp; - &nbsp; 45% </li>
            </ul>
        </div>

        <div class="job_Desc_div2">
            <h3>Qualification</h3>
            <div class="qualif_div">
            <div class="col-md-2 pl0"><div class="Q_circle">2011 <br> - <br> 2014</div></div>
            <div class="col-md-10 pl0">
                <h4>Malla Reddy Engineering College</h4>
                <p>Engineer's Degree, Computer Science, B</p>
            </div>
            <div class="clear"></div>
            </div>
            <div class="qualif_div">
            <div class="col-md-2 pl0"><div class="Q_circle">2011 <br> - <br> 2014</div></div>
            <div class="col-md-10 pl0">
                <h4>Malla Reddy Engineering College</h4>
                <p>Engineer's Degree, Computer Science, B</p>
            </div>
            <div class="clear"></div>
            </div>

        </div>


         <div class="job_Desc_div2">
            <h3>Work Experience</h3>
            <div class="experien_div">
            <div class="col-md-2 pl0"><div class="Q_circle">2011 <br> - <br> 2014</div></div>
            <div class="col-md-10 pl0">
                <h4>Comapany Name</h4>
                <p><a href="#">WWW.CompanyName.com</a></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius voluptatem ipsam nam, odit natus.</p>
            </div>
            <div class="clear"></div>
            </div>
            <div class="experien_div">
            <div class="col-md-2 pl0"><div class="Q_circle">2011 <br> - <br> 2014</div></div>
            <div class="col-md-10 pr0">
                <h4>Comapany Name</h4>
                <p><a href="#">WWW.CompanyName.com</a></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius voluptatem ipsam nam, odit natus.</p>
            </div>
            <div class="clear"></div>
            </div>
        </div>

        <div class="job_Desc_div2 Portfolio_divsss">
            <h3>Portfolio</h3>
           <div class="col-md-6 pl0">
                <div class="img_protf">
                    <img src="<?php echo base_url()?>assets/images/p1.jpg" class="img_100">
                    <div class="overlay_portfolio">
                        <div class="port_text">
                            <h3>Title</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, similique!</p>
                        </div>
                    </div>
                </div>
           </div>
           <div class="col-md-6 pr0">
                <div class="img_protf">
                    <img src="<?php echo base_url()?>assets/images/p2.jpg" class="img_100">
                <div class="overlay_portfolio">
                        <div class="port_text">
                            <h3>Title</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, similique!</p>
                        </div>
                    </div>
           </div>
           </div>
           <div class="clear"></div>
           <div class="col-md-6 pl0">
                <div class="img_protf">
                    <img src="<?php echo base_url()?>assets/images/p3.jpg" class="img_100">
           <div class="overlay_portfolio">
                        <div class="port_text">
                            <h3>Title</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, similique!</p>
                        </div>
                    </div></div></div>
           <div class="col-md-6 pr0">
                <div class="img_protf">
                    <img src="<?php echo base_url()?>assets/images/p4.jpg" class="img_100">
           <div class="overlay_portfolio">
                        <div class="port_text">
                            <h3>Title</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, similique!</p>
                        </div>
                    </div></div></div>
           <div class="clear"></div>
        </div>

    </div>


    <div class="col-md-4">
        <div class="job_Desc_div3">
            <h3>Company Detail</h3>
            <div class="col-md-6 pl0"><p class="job_d_title">Total Employees</p></div> <div class="col-md-6 text-right"><p class="job_d_ans">10-50</p></div><div class="clear"></div>
            <div class="col-md-6 pl0"><p class="job_d_title">Established In</p></div> <div class="col-md-6 text-right"><p class="job_d_ans">2008</p></div><div class="clear"></div>
            <div class="col-md-6 pl0"><p class="job_d_title">Current Jobs</p></div> <div class="col-md-6 text-right"><p class="job_d_ans">10</p></div><div class="clear"></div>
        </div>
        
        <div class="job_Desc_div3 cp_c text-center">
            <h3 class="text-left">Contact Recruiter</h3>
            <input type="text" placeholder="Your Name" name="">
            <input type="text" placeholder="Your Email" name="">
            <input type="text" placeholder="Phone" name="">
            <input type="text" placeholder="Subject" name="">
            <textarea placeholder="Message"></textarea>        
            <button class="cp_submit">Submit</button>
        </div>

        <div class="job_Desc_div3">
            <h3>Map Location</h3>
            <div id="googleMap" style="width:100%;height:260px;"></div>
        </div>
    </div>
    
    

</div><!-- /.row -->

</div><!-- /.container -->
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->


<script>
function myMap() {
var mapProp= {
    center:new google.maps.LatLng(51.508742,-0.120850),
    zoom:5,
};
var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY&callback=myMap"></script>



<script type="text/javascript">
    $('.U_Fr_list').on('click',function(){
        $('.overlay').show();
        $('.popup_followers').fadeIn();
    });
    $('.overlay, .close_r_p_popup').on('click',function(){
        $('.overlay').fadeOut();
        $('.popup_followers').hide();
        $('.popup_followings').hide();
        $('.popup_U_blockd').hide();
    });

    $('.U_flling_list').on('click',function(){
        $('.overlay').show();
        $('.popup_followings').fadeIn();
    });

    $('.U_blckd_list').on('click',function(){
        $('.overlay').show();
        $('.popup_U_blockd').fadeIn();
    });
</script>


</section>
</div>
</div>
                </section>
                
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->
<!--right sidebar start-->
<div class="right-sidebar">


            
                
                
                    <div class="target-sell">
                    </div>
                
            
            
         
            
            
        

</div>
<!--right sidebar end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->


<script src="assets/js/jquery.js"></script>
<script src="assets/js/jquery-1.10.2.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/easypiechart/jquery.easypiechart.js"></script>
<script src="assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/bootstrap-switch.js"></script>


<!--common script init for all pages-->
<script src="assets/js/scripts.js"></script>

<script>
    $(function ()
    {
        $("#wizard").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft"
        });

        $("#wizard-vertical").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            stepsOrientation: "vertical"
        });
    });


</script>

</body>

</html>
