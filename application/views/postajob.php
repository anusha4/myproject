    
<!--header start-->

<!--header end-->
<!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->


        

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Post A Job
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">

                        <div id="wizard">
                            

                            <section>
                            <div class="little_gap"></div>


                         <!--   <?php
                           if(count($getcompanycount)>0)
                           {
                           ?>
                            <div class="">
                                <div class="col-md-2"></div>
                                <div class="col-md-3 text-right">
                                    <label class="control-label">Select Job Post for Company</label>
                                </div>
                                <div class="col-md-5 text-right">
                                    <select name="" id="myagreementcompany" class="form-control pa_j_select_company" id="">
                                        <option value="0">Select Company</option>

                                        <?php
                                        for($i=0;$i<count($getcompaniesbelongsrecruiter);$i++)
                                        {
                                        ?>
                                        <option value="<?php echo $getcompaniesbelongsrecruiter[$i]['companydetails_companyid']; ?>"><?php echo $getcompaniesbelongsrecruiter[$i]['companydetails_name']; ?></option>
                                        <?php
                                         }
                                        ?>
                                        
                                    </select>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="clear"></div>
                            </div>
                            <?php
                        }
                        else
                        {
                            ?>


                            <div class="">
                                <div class="col-md-2"></div>
                                <div class="col-md-3 text-right">
                                    <label class="control-label">Select Job Post for Company</label>
                                </div>
                                <div class="col-md-5 text-right">
                                    <select name="" class="form-control pa_j_select_company1" id="">
                                        <option value="0">Select Company</option>
                                        <option value="-1">Add New Company</option>
                                    </select>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="clear"></div>
                            </div>
                            <?php
                        }
                            ?>
 -->


                            <div class="large_gap"></div>

                            <div class="full_field_fiv">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Job Title</label>
                                    <input type="text" class="form-control" placeholder="Enter Job Title..." id="jobtitle">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Designation</label>
                                    <input type="text" class="form-control" placeholder="Enter Designation" id="designation">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Department</label>
                                    <input type="text" class="form-control" placeholder="Enter Department" id="department">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">No of Vacancies</label>
                                    <input type="text" min="1" class="form-control" placeholder="Enter no of Vacancies" onkeypress="return isNumberKey(event)" id="noof_requirement">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Job Type</label>
                                    <select class="form-control" id="jobtype">
                                        <option value="Select Job Type">Select Job Type</option>
                                        <option value="temperory">temperory</option>
                                        <option value="permanent">permanent</option>
                                      
                                    </select>
                                </div>
                                
                                <div class="form-group">

                                    <label class="control-label">Skills</label>
                                    <!-- <input id="tags_1" type="text" class="tags form-control" value="<?php for($i=0;$i<count($master_skils);$i++){echo $master_skils[$i]['masterskills_skillname'].',';}?>" id="skills"/> -->
                                    <input id="tags_1" name="skills" type="text" class="tags form-control" value="" id="skills"/>

                                </div>

                                <div class="form-group">
                                    <label class="control-label" style="display: block;">Work Experience</label>
                                    <div class="col-md-6 pl0">
                                        <select class="tags form-control" id="work_experience_min">
                                            <option value="0">Select Min</option>
                                            <option value="11">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 pr0">
                                        <select class="tags form-control" id="work_experience_max">
                                            <option value="0">Select Max</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label">Location</label>
                                    <div class="">
                                        <select multiple name="location" id="e9" class="populate" style="width: 100%;">


                                         

                                            <?php
                                                 for($i=0;$i<count($masterlocation);$i++)
                                                 {
                                                 ?>
                                                <option value="<?php echo $masterlocation[$i]['location_id']; ?>"><?php echo $masterlocation[$i]['location_name']; ?></option>
                                                <?php
                                                 }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Qualification</label>
                                    <div class="field">
                                        <select multiple name="qualification" id="e1" class="populate" style="width: 100%;">

                                                 <option value="AK">Select Qualification</option>
                                                 <?php
                                                 for($i=0;$i<count($masterqualification);$i++)
                                                 {
                                                 ?>
                                                <option value="<?php echo $masterqualification[$i]['masterqualification_id']; ?>"><?php echo $masterqualification[$i]['masterqualification_name']; ?></option>
                                                <?php
                                                 }
                                                ?>
                                               

                                        </select>
                                    </div>
                                </div>
                               
                                
                            </div>

                            <div class="col-md-6">
                                
                                <div class="form-group">
                                    <label class="control-label">CTC</label>
                                    <input type="text" onkeypress="return isNumberKey(event)" class="form-control" id="ctc" placeholder="Enter CTC">
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label">Industry</label>
                                    <select class="form-control" id="industry">
                                        <option value="0">Select Industry Type</option>

                                        
                                        <?php
                                        for($i=0;$i<count($masterjobcategory);$i++)
                                        {
                                        ?>
                                        <option value="<?php echo $masterjobcategory[$i]['masterjobcategory_id']; ?>"><?php echo $masterjobcategory[$i]['jobcategory_name']; ?></option>
                                        <?php
                                    }
                                        ?>


                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">No of Interviews Round</label>
                                   
                                    <select class="form-control" id="noof_interview_round">
                                        <option value="-1">Select Interview Rounds</option>
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                         <option value="4">4</option>
                                          <option value="5">5</option>
                                           <option value="6">6</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Notice Period</label>
                                    <input type="text" class="form-control" placeholder="Enter Noticeperiod" id="noticepriod">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Job Expire Date</label>
                                    <input type="datepicker" class="form-control" id="job_exp_date" placeholder="Enter Job Expire Date">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Shift Timings</label>
                                    <input type="text" class="form-control" placeholder="Enter Shifttimings" id="shifttimings">
                                </div>
                                 <div class="form-group">
                                    <label class="control-label">Contact Email</label>
                                    <input type="text" class="form-control" placeholder="Enter Contact Email" id="contactemail">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Contact Mobile</label>
                                    <input type="text" class="form-control" placeholder="Enter Contact Mobile" id="contactmobile" onkeypress="return isNumberKey(event)">
                                </div>
                            </div>
                            <div class="col-md-12">
                            <div class="form-group">
                                    <label class="control-label">Job Description</label>
                                    <textarea class="wysihtml5 form-control" rows="9" name="job_desc" class="job_desc"></textarea>
                                </div>
                            </div>
                            <div class="clear"></div>
                            

                            <div class="text-center message_div">
                                <span class="Error_msg1"></span>
                              <span class="sucess_msg1"></span>
                              </div>

                                <div class="little_gap"></div>
                                <div class="text-center">
                                    <button type="button" id="jobposting" class="btn btn-primary btn-lg">Submit Job Post</button>
                                </div>
                                <div class="large_gap"></div>
                            </div>
                            </section>

                            

                           
                        </div>
                    </div>
                </section>
                
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->
<!--right sidebar start-->
<div class="right-sidebar">


            
                
                
                    <div class="target-sell">
                    </div>
                
            
            
         
            
            
        

</div>
<!--right sidebar end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->


<script src="assets/js/jquery.js"></script>
<script src="assets/js/jquery-1.10.2.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/easypiechart/jquery.easypiechart.js"></script>
<script src="assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/bootstrap-switch.js"></script>

<script type="text/javascript" src="assets/js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="assets/js/jquery-multi-select/js/jquery.multi-select.js"></script>


<script src="assets/js/jquery-tags-input/jquery.tagsinput.js"></script>
<script src="assets/js/select2/select2.js"></script>
<script src="assets/js/select-init.js"></script>


<!--common script init for all pages-->
<script src="assets/js/scripts.js"></script>
<script src="assets/js/toggle-init.js"></script>
<script src="assets/js/advanced-form.js"></script>

<script>
    $(function ()
    {
        $("#wizard").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft"
        });

        $("#wizard-vertical").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            stepsOrientation: "vertical"
        });
    });
</script>

<script type="text/javascript">

    $('.pa_j_select_company').on('change',function(){

    //     <script type="text/javascript">
    // $(function() {
    $( "#job_exp_date" ).datepicker({ 
     maxDate: new Date() 
 });
  // });


        var get_company_Value = $(this).val();


        // if (get_company_Value == 0) {
        //     $('.full_field_fiv').slideUp();
        // }
        // else
        // {
        //     $('.full_field_fiv').slideDown(); 

            // $( "#job_exp_date" ).datepicker({ 
            //      maxDate: 0
          //    $( "#job_exp_date" ).datepicker({ 
          //     maxDate: new Date() 
          // });
            
           // changeYear: true,
           //  minDate: '-3M',
           //  maxDate: '+28D',
        // }); 

        // }


    });



    $('.pa_j_select_company1').on('change',function(){
            // var get_company_Value = $(this).val();
            // if (get_company_Value == -1) {
            //    window.location = 'addCompany';
            // }
        
    });

</script>





<script type="text/javascript">
   
    $('#jobposting').on('click',function()
    // function jobposting()
    {



            // var companyid=$('#myagreementcompany').val();
            var jobtitle = $('#jobtitle').val();
            var designation = $('#designation').val();
            var department = $('#department').val();
            var noof_requirement = $('#noof_requirement').val();
            var jobtype = $('#jobtype').val();
            var skills = $('input[name="skills"]').val();
            var work_experience_min = $('#work_experience_min').val();
            var work_experience_max = $('#work_experience_max').val();
            var job_desc = $('textarea[name="job_desc"]').val();
            var ctc = $('#ctc').val();
            var location = $('select[name="location"]').val();
            // alert(location);
            var industry = $('#industry').val();
            var noticepriod = $('#noticepriod').val();
            var shifttimings = $('#shifttimings').val();
            var contactemail = $('#contactemail').val();
            var contactmobile = $('#contactmobile').val();
            // var functional_area = $('#functional_area').val();
            var qualification = $('select[name="qualification"]').val();
            var noof_interview_round = $('#noof_interview_round').val();
            //var interview_round_name = $('#interview_round_name').val();
            var job_exp_date = $('#job_exp_date').val();
           // alert(job_exp_date);
            var recruiter_id = '<?php echo $this->session->userdata('recruiter_id'); ?>';

            var atpos = contactemail.indexOf("@");
            var dotpos = contactemail.lastIndexOf(".");
             var re = /^[ A-Za-z-_/']*$/;
             var re1 = /^[ A-Za-z]*$/;

            if(jobtitle.length==0 || department.length == 0 || noticepriod.length == 0 || shifttimings.length == 0 || contactemail.length == 0 || contactmobile.length == 0 || designation.length==0 || noof_requirement.length==0 || jobtype.length==0 || skills.length==0 || work_experience_min.length==0 || work_experience_max.length==0 || job_desc.length==0 || ctc.length==0 || location.lenght==0 || industry.length==0 || qualification.length==0 || noof_interview_round.length==0 || job_exp_date.length==0)
            {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i>Please Fill Above Fields");

            }
             else if(jobtitle.length<5 || jobtitle.length>200)  
           
            {
             // alert('Jobtitle Maximum Of 5 To 100 Characters Allowed!');
              $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i>Jobtitle Maximum Of 5 To 100 Characters Allowed!");
               


            }
            else if(!re.test(jobtitle))
            {

                 $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i>Jobtitle Accepts No Special Symbols Except -_/' Are Allowed!");
               

            }
             else if(designation.length<5 || designation.length>200)  
            {
                
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i>Designation Maximum Of 5 To 100 Characters Allowed!");


            }
            else if(!re.test(designation))
            {
                
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Designation Accepts No Special Symbols Except -_/' Are Allowed!");

            }
            else if(noof_requirement.length>4)  
            {
                
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i>No Of Requirements Maximum Of 4 Digits Allowed!");


            }

          else if(contactmobile.length!=10){
                       $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> required 10 digits, match requested format!");
                         $('.sucess_msg').hide(); 

                    }
          else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=contactemail.length) 
                          {
                            //alert("please enter valid email");

                            $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid EmailId!");
                            $('.sucess_msg').hide(); 
                                  
                          }

            // else if(location.length<3 || location.length>100)  
            // {
            //     $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i>Location  Maximum 3 To 100 Characters Allowed!");
               

            // }
            // else if(!re1.test(location))
            // {
                
            //     $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Location Accepts No Special Symbols Except -_/' Are Allowed!");

            // }

             else if(qualification.length>50)  
            {

                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Qualification Maximum Of 50 Characters Allowed!");
              


            }
            else
            {

           $('.Error_msg1').hide();

            // alert(companyid+''+jobtitle+''+designation+''+noof_requirement+''+jobtype+''+skills+''+work_experience_min+''+work_experience_max+''+job_desc+''+ctc+''+location+''+industry+''+functional_area+''+qualification+''+noof_interview_round+''+recruiter_id+''+job_exp_date);

             $.ajax({

              type: "post",
              url: "<?php echo base_url();  ?>api/jobposting_api.php",
              data: {jobtitle:jobtitle,department:department,designation:designation,noof_requirement:noof_requirement,jobtype:jobtype,skills:skills,work_experience_min:work_experience_min,work_experience_max:work_experience_max,job_desc:job_desc,ctc:ctc,location:location,industry:industry,qualification:qualification,noof_interview_round:noof_interview_round,job_exp_date:job_exp_date,noticepriod:noticepriod,shifttimings:shifttimings,contactemail:contactemail,contactmobile:contactmobile,},

              success:function(data){
            var jsondata = JSON.parse(data);

        
                if(jsondata.status == 1)
                {
                   var lastjob_slno = jsondata.lastid; 
                   localStorage.setItem("lastjob_slno",lastjob_slno); 

                       joblogoimage(lastjob_slno);
                        alert("success");
                  
                   
                }

                else
                {
                  alert("Failure");
                }
             
               
             }

                
         });
         }




    });
 
</script>
 <script type="text/javascript">
      
      function isNumberKey(evt){
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && charCode != 43 && charCode != 45 && (charCode < 48 || charCode > 57))
            return false;
          return true;
      }

    </script>







</body>

</html>
