<div class="popup text-center">
    <span class="clse_btn"><i class="fa fa-times"></i></span>
    <p>Reason For Rejection</p>
    <textarea placeholder="Message..." class="form-control"></textarea>
    <button class="btn btn-info">Submit</button>
    <button class="btn btn-danger">Cancel</button>
</div>

<!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->
            <section class="panel">
                    <header class="panel-heading">
                        List Of Users Applied this Job Post
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <!-- page start-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="adv-table">
                                    <table class="display table table-bordered table-striped td_img_circle" id="dynamic-table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Job Title</th>
                                                <th>Company</th>
                                                <th>Posted on</th>
                                                
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                             <?php 
                                             $sno=1;

                                    for ($i=0; $i <count($joblist_companydetails);$i++) {
                                 ?>   
                                                <td><?php echo $sno; ?></td>
                                                
                                                <td><p class="pos_j_desg"><?php echo $joblist_companydetails[$i]['joblist_jobtitle'] ?></p> 
                                                    <span class="pos_j_lctn"><i class="fa fa-suitcase"></i> &nbsp;  <?php echo $joblist_companydetails[$i]['joblist_skills'] ?></span>
                                                </td>
                                                <td><?php echo $joblist_companydetails[$i]['companydetails_name'] ?></td>
                                                <td><p class="pstd_dte"><i class="fa fa-calendar"> 29-08-2017<br><br><i class="fa fa-clock-o"></i> 10:55:12</i></p></td>
                                                <!-- <td><p class="pstd_dte"><i class="fa fa-graduation-cap"></i> BTech</p></td> -->
                                                <td><button class="btn btn-info">View Profile</button> 

                                                <button class="btn btn-danger deletejobs" data-id="<?php echo $joblist_companydetails[$i]['joblist_id'] ?>">Delete</button>

                                                <a href="<?php echo base_url(); ?>jobAppliedByUsersList/<?php echo base64_encode($joblist_companydetails[$i]['joblist_id']); ?>">
                                                <button class="btn btn-success">Applied Users</button>
                                                </a>
                                                </td>
                                            </tr>


                                            <?php
                                            $sno++;
                                        }
                                            ?>
                                          
                                            

                                       </tbody>
                                   </table>
                               </div>
                            </div>
                        </div>
                        <!-- page end-->
                    </div>
                </section>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->

<script type="text/javascript" src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>

<script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>
<!--common script init for all pages-->
<script src="assets/js/scripts.js"></script>

<!--dynamic table initialization -->
<script src="assets/js/dynamic_table_init.js"></script>

<!--common script init for all pages-->

<!--script for this page only-->


<script type="text/javascript">
    $(document).on('click','.confo_popup',function(){
        $('.overlay').show();
        $('.popup').fadeIn();
    });
    $('.popup .btn-danger, .popup span, .overlay').on('click',function(){
        $('.overlay').fadeOut();
        $('.popup').hide(); 
    });
</script>

<script type="text/javascript">
    
    $('.deletejobs').on('click',function()
    {
       // alert('hi');

        var jobid=$(this).data('id');
         var recruiter_id = '<?php echo $this->session->userdata('recruiter_id'); ?>';

        $.ajax({

              type: "post",
              url: "<?php echo base_url();  ?>api/delete_jobdetails.php",
              data: {recruiter_id:recruiter_id,jobid:jobid},

              success:function(data){
            var jsondata = JSON.parse(data);

        
                if(jsondata.status == 1)
                {
                  
                    
                        alert("success");
                  
                   
                }

                else
                {
                  alert("Failure");
                }
             
               
             }

                
         });


    });
</script>
</body>

<!-- Mirrored from bucketadmin.themebucket.net/calendar.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 31 Aug 2017 10:56:23 GMT -->
</html>
