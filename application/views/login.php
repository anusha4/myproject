
 <script type = "text/javascript" >
        function preventBack(){window.history.forward();}
        setTimeout("preventBack()", 0);
        window.onunload=function(){null};
</script>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">

    <title>Login</title>

    <!--Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/bootstrap-reset.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/main.css">

</head>


  <body class="login-body">

    <div class="container">

      <div class="form-signin">
        <h2 class="form-signin-heading">sign in now</h2>
        <div class="login-wrap">
            <div class="user-login-info">
            
                <input type="text" class="form-control userId" placeholder="User ID" name="userId" autofocus>
                <div class="little_gap"></div>
                <input type="password" class="form-control password" placeholder="Password" name="password" id="password">
            </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me" class="r_checkbox"> Remember me
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>

                </span>
            </label>
            <span class="Error_msg"></span>
            <span class="sucess_msg"></span>
            <button class="btn btn-lg btn-login btn-block signIn_Btn" onclick="ValidateLogin()" type="submit">Sign in</button>

            <div class="registration">
                Don't have an account yet?
                <a class="" href="<?php echo base_url('registration'); ?>">
                    Create an account
                </a>
            </div>

        </div>
  </div>
          <!-- Modal -->
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>
                      <div class="modal-body">
                          <p>Enter your e-mail address below to reset your password.</p>
                          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix email_forgot_pwd">
                          <span class="Error_msg1"></span>
                          <span class="sucess_msg1"></span>
                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button class="btn btn-success forgot_send_btn" type="button">Submit</button>
                      </div>
                  </div>
              </div>
          </div>
          <!-- modal -->

     

    </div>



    <!-- Placed js at the end of the document so the pages load faster -->

    <!--Core js-->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
      // $('.signIn_Btn').on('click',function(){

        function ValidateLogin()
        {

        var login_UserID = $('.login-wrap .userId').val();
        var login_pwd = $('.login-wrap .password').val();
        var passwords = document.getElementById("password");

        var atpos = login_UserID.indexOf("@");
        var dotpos = login_UserID.lastIndexOf(".");

         var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,10}$/;


        if (login_UserID.length == 0 || login_pwd.length == 0 || $('.r_checkbox').prop("checked") == false) 
        {
          $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Please Fill All Fields!");
        }

          else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=login_UserID.length) 
          {
            //alert("please enter valid email");

           $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid EmailId!");
           $('.sucess_msg').hide(); 
                                  
         }
         else if(login_pwd.length<8)
              {
           $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Password Minimum Length 8 Characters!");
           $('.sucess_msg').hide(); 
                       
           }
          else if (!regex.test(passwords.value))
              {

             $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Password must be 8 characters including 1 uppercase letter, 1 special character, alphanumeric characters! and spaces are not allowed");
              $('.sucess_msg').hide(); 

          
           }

        else
        {

          $.ajax({
                type : "post",
                url : "<?php echo base_url();  ?>api/recruiter_login.php",
                data : {email:login_UserID,password:login_pwd},
                success:function(data)
                {
                 jsondata = JSON.parse(data);
                 if(jsondata.status == 1)
                 {
                     
               
                  $('.Error_msg').hide(); 
                $('.sucess_msg').fadeIn().html("<i class='fa fa-check'></i> Login  Successful Redirecting!");

                 setInterval(function(){
               window.location="<?php echo base_url('checklogin'); ?>" + '/' + login_UserID;
                    
                  }, 2000);
             }
             else
             {
                  $('.Error_msg').fadeIn().html("<i class='fa fa-times'></i> Please Enter valid Username And Password!");
             }
            }
          });
        
        
        }
      }

      // });











      $('.forgot_send_btn').on('click',function(){
        var email_forgot_pwd = $('.email_forgot_pwd').val();
        if (email_forgot_pwd.length == 0) {
        $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Email");
        }
        else
        {
          $('.Error_msg1').hide();
         $('.sucess_msg1').fadeIn().html("<i class='fa fa-check'></i> Forgot Password link send to your registered Email **********mca@gmail.com "); 
        }
      }); 
    </script>

  </body>


</html>
