
<!--header end-->

<!--sidebar end-->




<script>

    $(document).ready(function() {
        
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: '2017-09-12',
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectHelper: true,
            select: function(start, end) {
                var title = prompt('Event Title:');
                var eventData;
                if (title) {
                    eventData = {
                        title: title,
                        start: start,
                        end: end
                    };
                    $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                }
                $('#calendar').fullCalendar('unselect');
            },
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [
                {
                    title: 'All Day Event',
                    start: '2017-09-01'
                },
                {
                    title: 'Long Event',
                    start: '2017-09-07',
                    end: '2017-09-10'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2017-09-09T16:00:00'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2017-09-16T16:00:00'
                },
                {
                    title: 'Conference',
                    start: '2017-09-11',
                    end: '2017-09-13'
                },
                {
                    title: 'Meeting',
                    start: '2017-09-12T10:30:00',
                    end: '2017-09-12T12:30:00'
                },
                {
                    title: 'Lunch',
                    start: '2017-09-12T12:00:00'
                },
                {
                    title: 'Meeting',
                    start: '2017-09-12T14:30:00'
                },
                {
                    title: 'Happy Hour',
                    start: '2017-09-12T17:30:00'
                },
                {
                    title: 'Dinner',
                    start: '2017-09-12T20:00:00'
                },
                {
                    title: 'Birthday Party',
                    start: '2017-09-13T07:00:00'
                },
                {
                    title: 'Click for Google',
                    url: 'http://google.com/',
                    start: '2017-09-28'
                }
            ]
        });

        $('#calendar tbody tr td').on('click',function()
        {

         var data = $(this).data("date");
          //var classname=$(this).attr('class');
         alert(data);

        });

//         $(document).ready(function () {
//     $('.outerDiv').click(function () {
//         alert($(this).attr('class'));
//     });
// });

        
        
    });



</script>


    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">ToDo List</header>
                    <div class="panel-body">
                        <div id='calendar'></div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>









    <!--main content end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->

<script src="assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="assets/js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="assets/js/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<script src="assets/js/flot-chart/jquery.flot.js"></script>
<script src="assets/js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="assets/js/flot-chart/jquery.flot.resize.js"></script>
<script src="assets/js/flot-chart/jquery.flot.pie.resize.js"></script>


<script src="assets/js/scripts.js"></script>
<!--common script init for all pages-->



</body>

<!-- Mirrored from bucketadmin.themebucket.net/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 31 Aug 2017 10:57:09 GMT -->
</html>
