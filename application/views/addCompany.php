

<!--header start-->

<!--header end-->
<!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Add Company
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">

                        <div id="wizard">
                            

                            <section>
                            
                            

                        <div class="personal_details">
                            <h2>Company Details</h2>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Company Name</label>
                                    <input type="text" class="form-control" placeholder="Enter Company Name" id="companyname">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Company Category</label>
                                    <select class="form-control" id="companycategory">

                                        <option value="0">Select Company Category</option>

                                       
                                        <?php
                                        for($i=0;$i<count($getmastercategory);$i++)
                                        {

                                        ?>

                                        <option value="<?php echo $getmastercategory[$i]['mastercompanycategory_name']; ?>"><?php echo $getmastercategory[$i]['mastercompanycategory_name']; ?></option>
                                        <?php
                                            }
                                            ?>
                                            <option value="Other">Other</option>
                                             </select>
                                             <div class="form-group" id="other">

                                             <label class="control-label">Enter Company Category</label><input type="text"  id="othercategory" class="form-control" placeholder="Enter Company Category" min="1">
                                   
                                                   </div>


                                        <!-- <option value="Categoy 0021">Categoy 0021</option>
                                        <option value="Categoy 0031">Categoy 0031</option>
                                        <option value="Categoy 0041">Categoy 0041</option> -->
                                    </select>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">No of Employees</label>
                                    <input type="text"  id="companynoofemployees" class="form-control" placeholder="Set No of Employees" min="1" onkeypress="return isNumberKey(event)">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Branch Name</label>
                                    <input type="text" id="companybranchname" class="form-control" placeholder="Enter Branch Name">
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Technologies</label>
                                     <div class="">
                                        <select multiple name="abc" id="e1" class="populate abc" style="width: 100%;">


                                           <?php
                                            for($i=0;$i<count($mastertechnolgy);$i++)
                                            {
                                            ?>

                                                <option value="<?php echo $mastertechnolgy[$i]['mastertechnolgy_name']; ?>"><?php echo $mastertechnolgy[$i]['mastertechnolgy_name']; ?></option>
                                                <?php
                                            }
                                            ?> 

                                                <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Description</label>
                                    <textarea class="form-control" id="editor" placeholder="Enter Description"> </textarea>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Rectuiter Type</label>
                                    <select class="form-control" id="companyrecruitertype">
                                    <option value="0">Select recruiter Type</option>
                                    <option value="Self">Employee</option>
                                    <option value="Other">Consultant</option>
                                    <!-- <option value="Recruiter Type 031">Recruiter Type 031</option>
                                    <option value="Recruiter Type 041">Recruiter Type 041</option> -->
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Company Contact Person</label>
                                    <input type="text" class="form-control" placeholder="Enter Contact Person Name" id="companycontactperson">
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Company Mobile</label>
                                    <input type="text" class="form-control" placeholder="Enter Company Mobile" id="companymobile" onkeypress="return isNumberKey(event)">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Company H.No</label>
                                    <input type="text" class="form-control" placeholder="Enter Company H.No:" id="companyhouseno">
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Street Name</label>
                                    <input type="text" class="form-control" placeholder="Enter Street Name" id="companystreet">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Locality</label>
                                    <input type="text" class="form-control" placeholder="Enter no of Vacancies" id="companylocality">
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">City</label>
                                    <input type="text" class="form-control" placeholder="Enter City" id="companycity">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">State</label>
                                    <input type="text" class="form-control" placeholder="Enter State" id="companystate">
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Country</label>
                                    <input type="text" class="form-control" placeholder="Enter Country" id="companycountry">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Started on</label>

                                    <select class="form-control" id="companystartedon">
                                    <option value="0">Select Year</option>

                                    <?php for($i=2017; $i>1979;$i--)
                                 echo '<option value="'.$i.'">'.$i.'</option>';
                                 ?>
                                 </select>

                                   <!--  <input type="text" id="companystartedon" class="form-control default-date-picker" placeholder="Enter Started on"> -->

                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Pin Code</label>
                                    <input type="text" class="form-control" placeholder="Enter Pin Code" id="companypincode" onkeypress="return isNumberKey(event)">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Company Phone</label>
                                    <input type="text" class="form-control" placeholder="Enter Company Phone" id="companymobile" onkeypress="return isNumberKey(event)">
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Company website</label>
                                    <input type="text" class="form-control" placeholder="Enter Company website" id="companywebsite">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Company Email</label>
                                    <input type="text" class="form-control" placeholder="Enter Company Email Company" id="companyemailid">
                                </div>
                            </div>


                            <div class="clear"></div>
                           

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Company Logo</label>
                                    <input type="file" class="form-control" placeholder="" id="form-register-photo">
                                </div>
                            </div>
                            
                        </div>
                        <div class="clear"></div>

                        
                            </div>
    
                    <div class="clear"></div>
                        <div class="large_gap"></div>

                        <div class="text-center message_div">
                                <span class="Error_msg1"></span>
                              <span class="sucess_msg1"></span>
                              </div>

                              <div class="large_gap"></div>

                        <div class="text-center">
                            <button type="button" class="btn btn-primary btn-lg" id="addcompany">Save Company</button>
                        </div>
                        <div class="large_gap"></div>
                    </section>

                            

                           
                        </div>
                    </div>
                </section>
                
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->
<!--right sidebar start-->
<div class="right-sidebar">


            
                
                
                    <div class="target-sell">
                    </div>
                
            
            
         
            
            
        

</div>
<!--right sidebar end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->


<script src="assets/js/jquery.js"></script>
<script src="assets/js/jquery-1.10.2.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/easypiechart/jquery.easypiechart.js"></script>
<script src="assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/bootstrap-switch.js"></script>

<script type="text/javascript" src="assets/js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="assets/js/jquery-multi-select/js/jquery.multi-select.js"></script>


<script src="assets/js/jquery-tags-input/jquery.tagsinput.js"></script>
<script src="assets/js/select2/select2.js"></script>
<script src="assets/js/select-init.js"></script>


<!--common script init for all pages-->
<script src="assets/js/scripts.js"></script>
<script src="assets/js/toggle-init.js"></script>
<script src="assets/js/advanced-form.js"></script>

<script>
    $(function ()
    {
        $("#wizard").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft"
        });

        $("#wizard-vertical").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            stepsOrientation: "vertical"
        });
    });
</script>

<script type="text/javascript">
    $(document).on('click','.add_degree',function(){
        var degree_div = '<div class="form-group"><label class="control-label">College Name</label><input type="text" class="form-control" placeholder="Enter Name of College"><div class="little_gap"></div><label class="control-label">Degree</label><input type="text" class="form-control" placeholder="Enter Degree"><div class="little_gap"></div><label class="control-label" style="display: block;">Duration</label><div class="col-md-6 pl0"><input type="text" class="form-control default-date-picker" placeholder="From"></div><div class="col-md-6 pr0"><input type="text" class="form-control default-date-picker" placeholder="to"></div><div class="clear"></div><button class="btn btn-danger btn-xs btn_delte_div disabled_btn"><i><span class="fa fa-trash-o"></span></i></button></div><div class="large_gap"></div>';
        $('.degree_full_div').append(degree_div);
        $('.default-date-picker').datepicker();
        var degree_div_length =$('.degree_full_div .form-group').length;
        if (degree_div_length == 1) {
            $('.degree_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.degree_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        }
    });  
    $(document).on('click','.degree_full_div .form-group .btn_delte_div',function(){
        $(this).parent().next('.large_gap').remove();
        $(this).parent().remove().next('.large_gap').remove();
        var degree_div_length =$('.degree_full_div .form-group').length;
        if (degree_div_length == 1) {
            $('.degree_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.degree_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        }
    });


    $(document).on('click','.add_Skills',function(){
        var skills_div = '<div class="form-group"><label class="control-label">Skill Name</label><input type="text" class="form-control" placeholder="Enter no of Vacancies"><div class="little_gap"></div><label class="control-label">Level</label><select class="form-control"><option value="0">Select Level</option><option value="Low" selected>Low</option><option value="Moderate">Moderate</option><option value="High">High</option></select><button class="btn btn-danger btn-xs btn_delte_div disabled_btn"><i><span class="fa fa-trash-o"></span></i></button></div><div class="large_gap"></div>';
        $('.skill_full_div').append(skills_div);
        var skills_div_length = $('.skill_full_div .form-group').length;
        if (skills_div_length == 1) {
            $('.skill_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.skill_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        }
    });
    $(document).on('click','.skill_full_div .form-group .btn_delte_div',function(){
        $(this).parent().next('.large_gap').remove();
        $(this).parent().remove().next('.large_gap').remove();
        var skills_div_length =$('.skill_full_div .form-group').length;
        if (skills_div_length == 1) {
            $('.skill_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.skill_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        }
    });   

    $(document).on('click','.add_experience',function(){
        var experience_div = '<div class="form-group"><div class="col-md-6"><label class="control-label">Company Name</label><input type="text" class="form-control" placeholder="Enter Name of College"></div><div class="col-md-6"><label class="control-label">Degree</label><input type="text" class="form-control" placeholder="Enter Degree"></div><div class="clear"></div><div class="little_gap"></div><div class="col-md-6"><label class="control-label" style="display: block;">Duration</label><div class="col-md-6 pl0"><input type="text" class="form-control default-date-picker" placeholder="From"></div><div class="col-md-6 pr0"><input type="text" class="form-control default-date-picker" placeholder="to"></div></div><div class="col-md-6"><label class="control-label">Location</label><input type="text" class="form-control" placeholder="Enter Location"></div><div class="clear"></div><button class="btn btn-danger btn-xs btn_delte_div disabled_btn"><i><span class="fa fa-trash-o"></span></i></button></div><div class="large_gap"></div>';
        $('.experience_full_div').append(experience_div);
        $('.default-date-picker').datepicker();
        var experience_div_length = $('.experience_full_div .form-group').length;
        if (experience_div_length == 1) {
            $('.experience_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.experience_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        }
    });
    $(document).on('click','.experience_full_div .form-group .btn_delte_div',function(){
        $(this).parent().next('.large_gap').remove();
        $(this).parent().remove().next('.large_gap').remove();
        var experience_div_length =$('.experience_full_div .form-group').length;
        if (experience_div_length == 1) {
            $('.experience_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.experience_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        }
    });

    $(document).on('click','.add_portfolio',function(){
        var portfolio_div = '<div class="form-group"><label class="control-label">Project Name</label><input type="text" class="form-control" placeholder="Enter Project Name"><div class="little_gap"></div><label class="control-label">Project Short Description</label><textarea class="form-control" placeholder="Project Short Description"></textarea><div class="little_gap"></div><label class="control-label" style="display: block;">Duration</label><div class="col-md-6 pl0"><input type="text" class="form-control default-date-picker" placeholder="From"></div><div class="col-md-6 pr0"><input type="text" class="form-control default-date-picker" placeholder="to"></div><div class="clear"></div><div class="little_gap"></div><label class="control-label">Project Name</label><input type="file" class="form-control"><button class="btn btn-danger btn-xs btn_delte_div disabled_btn"><i><span class="fa fa-trash-o"></span></i></button></div><div class="large_gap"></div>';
        $('.portfolio_full_div').append(portfolio_div);
        $('.default-date-picker').datepicker();
        var portfolio_div_length = $('.portfolio_full_div .form-group').length;
        if (portfolio_div_length == 1) {
            $('.portfolio_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.portfolio_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        } 
    });
    $(document).on('click','.portfolio_full_div .form-group .btn_delte_div',function(){
        $(this).parent().next('.large_gap').remove();
        $(this).parent().remove().next('.large_gap').remove();
        var portfolio_div_length =$('.portfolio_full_div .form-group').length;
        if (portfolio_div_length == 1) {
            $('.portfolio_full_div .form-group .btn_delte_div').addClass('disabled_btn');
        }
        else
        {
            $('.portfolio_full_div .form-group .btn_delte_div').removeClass('disabled_btn');   
        } 
    });
</script>

 <script type="text/javascript">

    // $(document).ready(function()
    // {
        $('#other').hide();
        $('#companycategory').on('change',function()
        {

            var a=$(this).val();
            if(a=='Other')
            {
                 $('#other').slideDown();
                
                var othercategory=$('#othercategory').val();

                localStorage.setItem("othercategory",othercategory);

                



         
            }
            else
            {
                $('#other').slideUp();

            }

        });
        // });
    </script>






<script type="text/javascript">

  $('#addcompany').on('click',function(){

            var recruiterid='<?php echo $this->session->userdata('recruiter_id')?>';
            
           var companyname = $('#companyname').val();
           var companycategory = $('#companycategory').val();
           var companynoofemployees = $('#companynoofemployees').val();

            var companybranchname = $('#companybranchname').val();
            var companytechnologies = $('select[name="abc"]').val();

            var companyhouseno = $('#companyhouseno').val();
            var companystreet = $('#companystreet').val();
            var company_desc = $('#editor').val();

            var companylocality = $('#companylocality').val();
            var companycity = $('#companycity').val();
            var companystate = $('#companystate').val();
            var companycountry = $('#companycountry').val();
            var companystartedon = $('#companystartedon').val();
            var companypincode = $('#companypincode').val();
            var companymobile = $('#companymobile').val();
            var companyrecruitertype = $('#companyrecruitertype').val();
             var companycontactperson = $('#companycontactperson').val();
             
             var companywebsite = $('#companywebsite').val();
          
            var companyemailid = $('#companyemailid').val();

            //alert(companytechnologies);
            
              var re = /^[ A-Za-z-']*$/;
              var re1 = /^[ A-Za-z]*$/;
              var re2 = /^[ A-Za-z,'.,/]*$/;
              var re3 = /^[ A-Za-z,'.-:/]*$/;
              var re4 = /^[ A-Za-z0-9.@]*$/;
              var re5 = /^[ A-Za-z0-9]*$/;

             if(companyname.length==0 || companycategory.length==0 || companynoofemployees.length==0 || companybranchname.length==0 || companytechnologies.length==0 || companyhouseno.length==0 || companystreet.length==0 || company_desc.length==0 || companycity.length==0 || companystate.length==0 || companycountry.length==0 || companystartedon.length==0 || companypincode.length==0 || companymobile.length==0 || ompanyrecruitertype.length==0 || companycontactperson.length==0 || companymobile.length==0 || companywebsite.length==0 || companyemailid.length==0 || companylocality.length==0)
                 {
                    $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Fill All Details!");
                    // alert('Please Fill All Details');
                 }
              else if (!re.test(companyname)) {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company Name Accepts Only Hiphen And Single Quotes!");

              //alert('Company Name Accepts only hiphen and single quotes');

              }
              else if(companyname.length<5 || companyname.length>100)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company Name Minimum of 5 Characters And Maximum Of 100 Characters!");
                //alert('Company Name Minimum of 5 characters and maximum of 100 characters');

              }

              
              else if(companybranchname.length<5 || companybranchname.length>10)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Companybranchname Minimum Of 5 Characters And Maximum Of 10 Characters!");
               // alert('companybranchname Minimum of 5 characters and maximum of 10 characters');

              }
              else if(company_desc.length>2000)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company Description Accepts Maximum of 2000 Characters!");
                //alert('Company Description Accepts Maximum of 2000 characters');

              }
              else if(companycontactperson.length<3 || companycontactperson.length>100)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company Contactperson Minimum of 3 Characters and Maximum of 100 Characters, No Special Symbols Allowed!");
                //alert('companycontactperson Minimum of 3 Characters and Maximum of 100 Characters, no special symbols allowed ');

              }
             
              else if (!re5.test(companycontactperson)) 
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company Contactperson Minimum of 3 Characters and Maximum of 100 Characters, No Special Symbols Allowed!");
              //alert('companycontactperson Minimum of 3 Characters and Maximum of 100 Characters, no special symbols allowed ');
              }

             
              
               else if (!re2.test(companyhouseno)) 
               {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Address of Company Alpha Numeric And Only Symbols .,/- Allowed!");

              //alert('Address of company  alpha numeric and only symbols .,/- allowed');
              }

              else if(companyhouseno.length>50)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company houseno Accepts Maximum Of 50 Characters!");

                //alert("companyhouseno accepts maximum of 50 characters");

              }
              else if (!re2.test(companystreet)) 
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company street Of company Accepts Only Alphanumeric And Only Symbols .,/- allowed!");

              //alert('companystreet of company  alpha numeric and only symbols .,/- allowed');
              }

              else if(companystreet.length>50)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company street Accepts Maximum of 50 Characters!");

                //alert("companystreet accepts maximum of 50 characters");

              }

              else if (!re2.test(companylocality)) 
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company locality Of Company Accepts Alphanumeric And Only Symbols .,/- Allowed!");

              //alert('companylocality of company  alpha numeric and only symbols .,/- allowed');
              }

              else if(companylocality.length>50)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company Locality Accepts Maximum Of 50 Characters!");

                //alert("companylocality accepts maximum of 50 characters");

              }

              else if (!re2.test(companycity)) 
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company City Of Company  Alphanumeric And Only Symbols .,/- Allowed!");

              //alert('companycity of company  alpha numeric and only symbols .,/- allowed');
              }

              else if(companycity.length>50)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company City Accepts Maximum Of 50 Characters!");

                //alert("companycity accepts maximum of 50 characters");

              }

              else if (!re2.test(companystate))
               {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company State Of Company Alphanumeric And Only Symbols .,/- Allowed!");

              //alert('companystate of company  alpha numeric and only symbols .,/- allowed');
              }

              else if(companystate.length>100)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company State Accepts Maximum Of 100 Characters!");

                //alert("companystate accepts maximum of 100 characters");

              }

              else if (!re2.test(companycountry)) 
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company Country Of Company  Alphanumeric And Only Symbols .,/- Allowed!");

              //alert('companycountry of company  alpha numeric and only symbols .,/- allowed');
              }

              else if(companycountry.length>100)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company Country Accepts Maximum Of 100 Characters!");

                //alert("companycountry accepts maximum of 100 characters");

              }
               else if(companypincode.length>6 || companypincode.length<6)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company Pincode Accepts Maximum Of 50 Characters!");

                //alert("companypincode accepts maximum of 50 characters");

              }
               else if(companymobile.length>14 || companymobile.length<14)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid Mobile Number!");

                //alert("Please enter valid mobilenumber");

              }

              else if (!re3.test(companywebsite)) 
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company Website Accepts Maximum Of 50 Characters!");
                 //alert("companywebsite accepts maximum of 50 characters");
             
              }
              else if(companywebsite.length>150)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Company Website Accepts Maximum Of 150 Characters!");

                //alert("please enter companywebsite accepts maximum of 150 characters");

              }

              else if (!re4.test(companyemailid)) 
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Please Enter Valid Emaiid!");

                // alert("please enter valid emaiid");
              }
               //alert($('#form-register-photo')[0].files[0].size);

              else if($('#form-register-photo')[0].files[0].size<1048576)
              {
                $('.Error_msg1').fadeIn().html("<i class='fa fa-times'></i> Company logo with max of 1 MB size!");
                  //alert('Company logo with max of 1 MB size');
            }
            else
            {
              

              
             


              // else {
            //   alert('Company Name Must Be Includes Hyphen or Single Quotes');
            //   return false;
            //   }

            $.ajax({

              type: "post",
              url: "<?php echo base_url();  ?>api/addcompany_api.php",
              data: {recruiterid:recruiterid,companyname:companyname,companycategory:companycategory,companynoofemployees:companynoofemployees,companybranchname:companybranchname,companytechnologies:companytechnologies,companyhouseno:companyhouseno,companystreet:companystreet,company_desc:company_desc,companylocality:companylocality,companycity:companycity,companystate:companystate,companycountry:companycountry,companystartedon:companystartedon,companypincode:companypincode,companymobile:companymobile,companyrecruitertype:companyrecruitertype,companycontactperson:companycontactperson,companymobile:companymobile,companywebsite:companywebsite,companyemailid:companyemailid},

              success:function(data){
                var jsondata = JSON.parse(data);

                //alert(jsondata.status);

                if(jsondata.status == 1)
                {
                   var lastjob_slno = jsondata.lastid; 
                   localStorage.setItem("lastjob_slno",lastjob_slno);

                   var divlength = localStorage.getItem("divlength"); 

                        joblogoimage(lastjob_slno);
                        var othercategory = localStorage.getItem("othercategory");

                        if($('#othercategory').val()!='')
                        {
                        addothercategory();
                        }
                         alert("success");
                  
                   
                }

                else
                {
                  alert("Failure");
                }
              }

          });
         }
        
        });
  </script>



  <script type="text/javascript">
      function addothercategory()
      {

        var othercategory=$('#othercategory').val();
        alert(othercategory);

           var recruiterid='<?php echo $this->session->userdata('recruiter_id')?>';

         $.ajax({

              type: "post",
              url: "<?php echo base_url();  ?>api/addothercategory.php",
              data: {recruiterid:recruiterid,othercategory:othercategory},

              success:function(data){
                var jsondata = JSON.parse(data);

                //alert(jsondata.status);

                if(jsondata.status == 1)
                {
                   
                         alert("success");
                  
                   
                }

                else
                {
                  alert("Failure");
                }
              }

          });

      }
  </script>

  <script type="text/javascript">

    $('#form-register-photo').on('change', function(e) {
   if((this.files[0].size/1000) > 5120)
   {
       $('.err').text('File size limit exceeded');

       alert('File size limit exceeded');
      // e.preventDefault();
   }
    });
  </script>




  <script type="text/javascript">

   function joblogoimage(lastjob_slno)
    {
       
    var form_data = new FormData();  
    var file_data = $("#form-register-photo").prop("files")[0]; 
    for(var loopvar=0;loopvar<$("#form-register-photo").prop("files").length;loopvar++)
    {
       var file_data = $("#form-register-photo").prop("files")[loopvar]; 
      form_data.append("file[]", file_data);
    }
    if($("#form-register-photo").prop("files").length==0)
      form_data.append("file[]", "");

  
      form_data.append("lastjob_slno", lastjob_slno);
      
      $.ajax({
            url: "api/companylogoimage.php",
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(php_script_response){
              var jsondata=JSON.parse(php_script_response);
                if(jsondata.status==1)
                {
                    //alert("success");
                }
                else
                {
                   // alert("failed");
                }
        }
      });

    }

</script>

<script type="text/javascript">
      
      function isNumberKey(evt){
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && charCode != 43 && charCode != 45 && (charCode < 48 || charCode > 57))
            return false;
          return true;
      }

    </script>














</body>

</html>
