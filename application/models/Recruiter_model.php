<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recruiter_model extends CI_Model
{
     
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
          //
     }

     //get the emailid from recruiter
    


      public function recruiter_id($rec_id_session)
     {
          $sql="SELECT * from recruiter WHERE recruiter_emailid='$rec_id_session'";

          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }

     }

     //get the recruiter details from recruiter

     public function recruiterProfile($rec_id)
     {
          $sql="SELECT * from recruiter WHERE recruiter_id='$rec_id'";

          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }

     }


     //get the recruiter experience from recruiter

     
     
       public function recruiter_workexperience($rec_id)
      {
          $sql="SELECT * from recruiterexperience WHERE recruiterexperience_recruiterid='$rec_id'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }

     }

     //get the recruiter skills from recruiter

      public function recruiter_skills($rec_id)
      {
          $sql="SELECT * from recruiterskills WHERE recruiterskills_recruiterid='$rec_id'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }

     }


       //get the recruiter qualification from recruiter
 
     public function recruiter_qualification($rec_id)
      {
          $sql="SELECT * from recruiterqualification WHERE recruiterqualification_recruiterid='$rec_id'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }

     }


     public function recruiter_portfolio($rec_id)
      {
          $sql="SELECT * from  recruiterportfolio WHERE recruiterportfolio_recruiterid='$rec_id'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }

     }

     public function recruiter_companydata($rec_id)
      {
          $sql="SELECT * from companydetails WHERE companydetails_recruiterid='$rec_id' AND companydetails_companyrecruitertype='Self'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }

     }

     public function recruiter_personaldata($rec_id)
      {
          $sql="SELECT * from recruiter WHERE recruiter_id='$rec_id'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }

     }

     public function recruiter_details($rec_id)
      {
          $sql="SELECT * from recruiter WHERE recruiter_id='$rec_id'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }

     }

     public function location()
      {
          $sql="SELECT * from location";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }

     }


     public function master_skils()
      {
          $sql="SELECT * from masterskills";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }

     }

     public function selfcompany_details($recruiter_id)
     {
          $sql="SELECT * from companydetails WHERE companydetails_companyrecruitertype='Self' AND companydetails_recruiterid='$recruiter_id' ";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function joblist_companydetails($rec_id)
     {
          $sql="SELECT * from joblist_company WHERE jobist_recuiterid='$rec_id' and   joblist_status='ACTIVE'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function recruiter_name($rec_id)
     {
          $sql="SELECT * from recruiter WHERE recruiter_id='$rec_id'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function joblist_companydetails_single($rec_id,$jobid)
     {


          $sql="SELECT * from joblist_company WHERE jobist_recuiterid='$rec_id' and joblist_id='$jobid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

      public function joblist_companydetails_userid($userid)
     {
          $sql="SELECT jobapplicants_jobpostingid from jobapplicants WHERE jobapplicants_userid='$userid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

   
     public function condidatelist_onjob($jobid)
     {

          // print_r($jobid);
          

          $sql="SELECT jobapplicants_userid from jobapplicants WHERE jobapplicants_jobpostingid='$jobid'";
          // print_r($sql);
          // exit();
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function single_jobtitle($jobid)
     {
          $sql="SELECT * from joblist WHERE joblist_id='$jobid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }



      public function condidate_details($userid)
     {
          
          $sql="SELECT * from users WHERE user_userid='$userid'";
          
       $query=$this->db->query($sql);
          
          if($query->num_rows>=0)
          {
               return $query-> row_array();
          }
          else
          {
               return array();
          }
          // return $response;
     }

       public function condidate_qualification($userid)
     {
          
          
           $sql="SELECT * from userqualification WHERE userqualification_userid='$userid'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
          // return $response;
     }

      public function condidate_shortliststatus($userid,$jobid)
     {
          
          
           $sql="SELECT * from jobapplicants WHERE jobapplicants_userid='$userid' and jobapplicants_jobpostingid='$jobid'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
          // return $response;
     }

      public function company_profiledata($companyid)
     {
          
          
           $sql="SELECT * from companydetails WHERE companydetails_companyid='$companyid'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }

     public function joblist_bycompany($companyid)
     {
          
          
           $sql="SELECT * from joblist_company WHERE companydetails_companyid='$companyid'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }


     public function jobapplicants_userview($userid)
     {
          
          
           $sql="SELECT * from users WHERE  user_userid='$userid'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }

     public function userqualification($companyid)
     {
       
         $sql="SELECT * from userqualification WHERE userqualification_userid='$companyid'";
         
          $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }

     public function jobappliedusersshortlist($rec_id)
     {
          
          
           $sql="SELECT * from condidate_jobdetails WHERE jobapplicants_status='shortlisted' and jobapplicants_recruiterid='$rec_id'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }

     public function jobappliedusersselected($rec_id)
     {
          
          
           $sql="SELECT * from condidate_jobdetails where jobapplicants_recruiterid='$rec_id'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }

     
     public function company_profile_companyid($recruiter_id)
     {
          
          
           $sql="SELECT * from companydetails where companydetails_recruiterid='$recruiter_id'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }



     public function interviewstatus($rec_id)
     {
          
          
           $sql="SELECT  joblist_interviewmode from joblist where jobist_recuiterid='$rec_id'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }

      public function interviewmodecandidates()
     {
          
          
           $sql="SELECT  interviewstatus_round from interviewstatus group by interviewstatus_round";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }
      public function recruiterportfolio($rec_id)
     {
          
          
           $sql="SELECT  * from recruiterportfolio where recruiterportfolio_recruiterid='$rec_id'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }

     public function get_agreementcompany($rec_id)
     {
          
          
           $sql="SELECT  * from companydetails where   companydetails_recruiterid='$rec_id'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }

     public function checkselfcompanydetails($rec_id)
     {
          
          
           $sql="SELECT  * from companydetails where   companydetails_recruiterid='$rec_id' and     companydetails_companyrecruitertype='Self'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }

      public function joblistcompanydetailssingle($company_id)
     {

          // print_r($company_id);
          // exit();
          
          
           $sql="SELECT  * from companydetails where   companydetails_companyid='$company_id'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }

     public function recruitercompanydetails($recruiterid)
     {
          
           $sql="SELECT  * from companydetails where   companydetails_recruiterid='$recruiterid'";

            $query=$this->db->query($sql);
      
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
         
     }

     public function joblist_companydetails_singlebasedonid($rec_id,$companyid)
     {


          $sql="SELECT * from companydetails WHERE companydetails_recruiterid='$rec_id' and companydetails_companyid='$companyid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function jobapplicants($jobid,$rec_id)
     {


          $sql="SELECT * from  candidate_jobdetails WHERE  jobist_recuiterid='$rec_id' and    joblist_id='$jobid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function allcompaniesbasedonrecid($rec_id)
     {


          $sql="SELECT * from  companydetails WHERE companydetails_recruiterid='$rec_id'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function allusers($jobid)
     {


          $sql="SELECT jobapplicants_userid from  jobapplicants WHERE jobapplicants_jobpostingid='$jobid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function getmastercategory()
     {


          $sql="SELECT * from  mastercompanycategory";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }
     public function mastertechnolgy()
     {


          $sql="SELECT * from  mastertechnolgy";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

      public function getcompanycount($rec_id)
     {


          $sql="SELECT * from companydetails where companydetails_recruiterid='$rec_id'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

      public function companydetails($refid)
     {


          $sql="SELECT * from companydetails where companydetails_companyid='$refid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }


     public function userprofile($rec_id,$jobid,$userid)
     {


          $sql="SELECT  * from  candidate_jobdetails where jobist_recuiterid='$rec_id' and  joblist_id='$jobid' and jobapplicants_userid='$userid'";
          $query=$this->db->query($sql);

         

          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function userprofiledata($userid)
     {


          $sql="SELECT * from  candidate_jobdetails where  jobapplicants_userid='$userid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }


       public function userqualifications($userid)
     {


          $sql="SELECT * from  userqualification where userqualification_userid='$userid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

      public function userqualificationforusersapplicant($userid)
     {

          $sql="SELECT * from  userqualification where userqualification_userid='$userid' order by  userqualification_passingyear DESC limit 0,1";

          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

       public function userskills($userid)
     {


          $sql="SELECT * from userskills where  userskills_username='$userid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function userexperience($userid)
     {


          $sql="SELECT * from  experience where  experience_userid='$userid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }
     


   public function getcompaniesbelongsrecruiter($rec_id)
     {


          $sql="SELECT * from companydetails where companydetails_recruiterid='$rec_id'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function checkjob($jobid,$rec_id)
     {


          $sql="SELECT * from joblist where jobist_recuiterid='$rec_id' and joblist_id='$jobid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function checkuserprofile($jobid,$userid)
     {


          $sql="SELECT * from  jobapplicants where  jobapplicants_jobpostingid='$jobid' and jobapplicants_userid='$userid'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

      public function getrecruiteremail($rec_id_session)
     {


          $sql="SELECT recruiter_emailid from  recruiter where  recruiter_id='$rec_id_session'";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function mastercompanycategory()
     {


          $sql="SELECT * from  mastercompanycategory";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     public function masterjobcategory()
     {


          $sql="SELECT * from  masterjobcategory";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

      public function masterqualification()
     {


          $sql="SELECT * from  masterqualification";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

      public function masterlocation()
     {


          $sql="SELECT * from location";
          $query=$this->db->query($sql);
          if($query->num_rows>=0)
          {
               return $query->result_array();
          }
          else
          {
               return array();
          }
     }

     

     
     

     

     



     
}
?>