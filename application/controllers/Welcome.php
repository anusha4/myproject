<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct()
     {
          parent::__construct();
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          $this->load->helper('array');
          //$this->form_validation->set_rules();
          $this->load->model('Recruiter_model','GetRecruiterData');
     }
	public function index()
	{

		if($this->session->userdata('recruiter_id')!=NULL)
		{
		$this->load->view('addons/header.php');
		$this->load->view('addons/leftmenu.php');
		$this->load->view('index');
	    }
	    else
	    {
	    	redirect(base_url('login'));

	    }
	}

	public function login()
	{
		
		$this->load->view('login');

	}
	public function registration()
	{
		$this->load->view('registration');
	}
	public function setNewPassword()
	{
		$this->load->view('setNewPassword');
	}
	public function addProfileDetails()
	{


		
		 $rec_id_session =  $this->session->userdata('recruiter_id');
		  $data['getrecruiteremail']=$this->GetRecruiterData->getrecruiteremail($rec_id_session);
		  $data['mastercompanycategory']=$this->GetRecruiterData->mastercompanycategory();

	if($this->session->userdata('recruiter_id')!=NULL)
		{
		$this->load->view('addons/header.php');
		$this->load->view('addons/leftmenu.php');
		$this->load->view('addProfileDetails',$data);
	    }
	    else
	    {
	    	redirect(base_url('login'));

	    }
		
		
	}

	
	
	public function postajob()
	{

		$rec_id=$this->session->userdata('recruiter_id');
		$data['getcompanycount']=$this->GetRecruiterData->getcompanycount($rec_id);
		$data['getcompaniesbelongsrecruiter']=$this->GetRecruiterData->getcompaniesbelongsrecruiter($rec_id);
		$data['master_skils']=$this->GetRecruiterData->master_skils();

		 $data['masterjobcategory']=$this->GetRecruiterData->masterjobcategory();

		  $data['masterqualification']=$this->GetRecruiterData->masterqualification();

		   $data['masterlocation']=$this->GetRecruiterData->masterlocation();

		// print_r($data['getcompaniesbelongsrecruiter']);
		// exit();

		   if($this->session->userdata('recruiter_id')!=NULL)
		{
		$this->load->view('addons/header.php');
		$this->load->view('addons/leftmenu.php');
		$this->load->view('postajob',$data);
	    }
	    else
	    {
	    	redirect(base_url('login'));

	    }

		


	}
	public function myprofile()
	{

		 $rec_id=$this->session->userdata('recruiter_id');
		 $data['recruiterProfile']=$this->GetRecruiterData->recruiterProfile($rec_id);
		 $data['recruiter_qualification']=$this->GetRecruiterData->recruiter_qualification($rec_id);
		 $data['recruiter_experience']=$this->GetRecruiterData->recruiter_workexperience($rec_id);
		  $data['recruiter_skills']=$this->GetRecruiterData->recruiter_skills($rec_id);
		   $data['recruiter_portfolio']=$this->GetRecruiterData->recruiter_portfolio($rec_id);


		      if($this->session->userdata('recruiter_id')!=NULL)
		{


			if($data['recruiter_qualification']!=NULL)
			{
		$this->load->view('addons/header.php');
		$this->load->view('addons/leftmenu.php');
		$this->load->view('recruiterProfile',$data);
		}
		else
		{
			redirect(base_url('addProfileDetails'));

		}

	    }
	    else
	    {
	    	redirect(base_url('login'));

	    }


		
	}
	


	
	 public function checklogin($rec_email)
     {

     	

         	  $this->session->set_userdata('recruiter_email', $rec_email);
         	  $rec_id_session =  $this->session->userdata('recruiter_email');

         	$data['recruiter_email']=$this->GetRecruiterData->recruiter_id($rec_id_session);


			$recruiter_id = $data['recruiter_id']=$data['recruiter_email'][0]['recruiter_id'];

			
			 $rec_id=$this->session->set_userdata('recruiter_id',$recruiter_id);
			  $rec_id=$this->session->userdata('recruiter_id');
			  
		      redirect(base_url('index'));

			}


	public function logout()
	{
	    $this->session->unset_userdata('recruiter_email');
	    $this->session->unset_userdata('recruiter_id');

        redirect(base_url('login'));
	}


	public function addCompany()
	{

		 $rec_id=$this->session->userdata('recruiter_id');
		 $data['getmastercategory']=$this->GetRecruiterData->getmastercategory();
		 $data['mastertechnolgy']=$this->GetRecruiterData->mastertechnolgy();
		 

		  if($this->session->userdata('recruiter_id')!=NULL)
		{
		$this->load->view('addons/header.php');
		$this->load->view('addons/leftmenu.php');
		$this->load->view('addCompany',$data);
	    }
	    else
	    {
	    	redirect(base_url('login'));

	    }
		

	}

	

	public function addCompanyEdit($refid)
	{


		 $rec_id=$this->session->userdata('recruiter_id');

		 $data['companydetails']=$this->GetRecruiterData->companydetails($refid);

		  $data['getmastercategory']=$this->GetRecruiterData->getmastercategory();
		 $data['mastertechnolgy']=$this->GetRecruiterData->mastertechnolgy();
		 


       
		  if($this->session->userdata('recruiter_id')!=NULL)
		{
		$this->load->view('addons/header.php');
		$this->load->view('addons/leftmenu.php');
		$this->load->view('addCompanyEdit',$data);
	    }
	    else
	    {
	    	redirect(base_url('login'));

	    }
		

	}

	public function userprofile($jobid,$userid)
	{

		$jobid=base64_decode($jobid);
		$userid=base64_decode($userid);

		
		 $rec_id=$this->session->userdata('recruiter_id');

		 $data['userprofile']=$this->GetRecruiterData->userprofile($rec_id,$jobid,$userid);

		 $data['userprofiledata']=$this->GetRecruiterData->userprofiledata($userid);

		 $data['userqualification']=$this->GetRecruiterData->userqualifications($userid);

		 $data['userskills']=$this->GetRecruiterData->userskills($userid);

		  $data['userexperience']=$this->GetRecruiterData->userexperience($userid);

		    $data['checkuserprofile']=$this->GetRecruiterData->checkuserprofile($jobid,$userid);

		    

		  if($data['checkuserprofile']!=NULL)
		  { 
		$this->load->view('addons/header.php');
		$this->load->view('addons/leftmenu.php');
		$this->load->view('jobSeekerProfile',$data);
		}
		else
		{
			redirect(base_url('logout'));

		}

	}

	public function jobAppliedByUsersList($jobid)
	{

		$jobid=base64_decode($jobid);



		$data['recruiter_email'] = $this->session->userdata('recruiter_email');
	 	$rec_id = $this->session->userdata('recruiter_id');

	 	$a=$data['checkjob']=$this->GetRecruiterData->checkjob($jobid,$rec_id);

        if($a!=NULL)
        {
          $jobidfromdb=$data['checkjob'][0]['joblist_id'];


        

          $data['candidate_jobdetails']=$this->GetRecruiterData->jobapplicants($jobid,$rec_id);
        
          for($i=0;$i<count($data['candidate_jobdetails']);$i++)
          {

          $userid=$data['candidate_jobdetails'][$i]['user_userid'];
           $data['userqualification'][$i]=$this->GetRecruiterData->userqualificationforusersapplicant($userid);
 

          }

     
		$this->load->view('addons/header.php');
		$this->load->view('addons/leftmenu.php');
		$this->load->view('jobAppliedByUsersList',$data);
	}
	else
	{

		redirect(base_url('logout'));
	}
	
	}

	public function joblistByRecruiter()
	{
		$data['recruiter_email'] = $this->session->userdata('recruiter_email');
	 	$rec_id = $this->session->userdata('recruiter_id');

        $data['joblist_companydetails']=$this->GetRecruiterData->joblist_companydetails($rec_id);

          // print_r($data['joblist_companydetails']);
          // exit();

         if($this->session->userdata('recruiter_id')!=NULL)
		{
		$this->load->view('addons/header.php');
		$this->load->view('addons/leftmenu.php');
		$this->load->view('joblistByRecruiter',$data);
	    }
	    else
	    {
	    	redirect(base_url('login'));

	    }

		
		
	}

	


	


	

}
