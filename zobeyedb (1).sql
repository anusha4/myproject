-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2017 at 08:20 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zobeyedb`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `candidate_jobdetails`
-- (See below for the actual view)
--
CREATE TABLE `candidate_jobdetails` (
`joblist_slno` double
,`joblist_id` varchar(20)
,`joblist_jobtitle` varchar(100)
,`joblist_companyid` varchar(200)
,`joblist_employementjobtype` enum('temperory','permanent','','')
,`joblist_category` varchar(100)
,`joblist_interviewmode` varchar(100)
,`joblist_noofround` int(100)
,`joblist_jobdescription` varchar(1000)
,`joblist_jobqualification` varchar(100)
,`joblist_additionalinf` varchar(1000)
,`joblist_noofrequirements` varchar(100)
,`joblist_experience` varchar(30)
,`joblist_salary` varchar(100)
,`joblist_createdon` datetime
,`joblist_skills` varchar(1000)
,`jobist_recuiterid` varchar(100)
,`jobist_designation` varchar(500)
,`joblist_functionalarea` varchar(500)
,`jobist_jobpostingenddate` date
,`joblist_logo` varchar(200)
,`jobapplicants_slno` double
,`jobapplicants_jobpostingid` varchar(100)
,`jobapplicants_userid` varchar(100)
,`jobapplicants_status` enum('applied','shortlisted','rejected')
,`jobapplicants_rejectedmessage` varchar(500)
,`jobapplicants_appliedon` datetime
,`user_slno` double
,`user_userid` varchar(100)
,`user_fname` varchar(100)
,`user_lname` varchar(100)
,`user_email` varchar(100)
,`user_password` varchar(100)
,`user_mobile` varchar(12)
,`user_location` varchar(100)
,`user_image` varchar(100)
,`user_addedon` date
,`user_personalbackground` varchar(100)
,`user_preferedlocation` varchar(100)
,`user_jobtype` enum('permanent','temporary','','')
,`user_employementtype` enum('fulltime','parttime','','')
,`user_dateofbirth` datetime
,`user_gender` enum('male','female','other','')
,`user_meritalstatus` enum('married','unmarried','','')
,`user_mailingaddress` varchar(500)
,`user_fathername` varchar(100)
,`user_mothername` varchar(100)
,`user_securityquention` varchar(500)
,`user_securityanswer` varchar(500)
,`user_hashkey` varchar(200)
,`user_status` enum('active','inactive','','')
,`user_workststus` enum('Fresher','Experience')
,`user_blocked` enum('blocked','unblocked','','')
);

-- --------------------------------------------------------

--
-- Table structure for table `companycontact`
--

CREATE TABLE `companycontact` (
  `companycontact_slno` double NOT NULL,
  `companycontact_comanyid` varchar(100) NOT NULL,
  `companycontact_recruiterid` varchar(100) NOT NULL,
  `companycontact_personname` varchar(100) NOT NULL,
  `companycontact_email` varchar(100) NOT NULL,
  `companycontact_mobile` varchar(100) NOT NULL,
  `companycontact_subject` varchar(100) NOT NULL,
  `companycontact_message` varchar(500) NOT NULL,
  `companycontact_addedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companycontact`
--

INSERT INTO `companycontact` (`companycontact_slno`, `companycontact_comanyid`, `companycontact_recruiterid`, `companycontact_personname`, `companycontact_email`, `companycontact_mobile`, `companycontact_subject`, `companycontact_message`, `companycontact_addedon`) VALUES
(1, 'COMP0002', 'ZOBEYER0023', 'pandey', 'kmpandey94@gmail.com', '943141496', 'Job Details', 'not good', '2017-08-05 11:35:17'),
(2, 'COMP0002', 'ZOBEYER0023', '', '', '', '', '', '2017-08-09 12:21:15'),
(3, 'COMP0002', 'ZOBEYER0023', '', '', '', '', '', '2017-08-09 12:21:48'),
(4, 'COMP0002', 'ZOBEYER0021', '', '', '', '', '', '2017-08-18 18:49:47'),
(5, 'COMP0002', 'ZOBEYER0021', 'koty', 'kotymca199@gmail.com', '7799679638', 'Sample', 'sample message', '2017-08-18 18:50:39'),
(7, 'COMP0040', 'ZOBEYER0052', 'Koty', 'kotymca199@gmail.com', '7799679638', 'Msc', 'Sample Description', '2017-08-23 13:15:04'),
(8, 'COMP0043', 'ZOBEYER0056', '', '', '', '', '', '2017-09-04 19:07:08');

-- --------------------------------------------------------

--
-- Table structure for table `companydetails`
--

CREATE TABLE `companydetails` (
  `companydetails_slno` double NOT NULL,
  `companydetails_companyid` varchar(24) NOT NULL,
  `companydetails_name` varchar(100) NOT NULL,
  `companydetails_descritption` varchar(10000) NOT NULL,
  `companydetails_houseno` varchar(100) NOT NULL,
  `companydetails_street` varchar(100) NOT NULL,
  `companydetails_locality` varchar(100) NOT NULL,
  `companydetails_city` varchar(100) NOT NULL,
  `companydetails_state` varchar(100) NOT NULL,
  `companydetails_country` varchar(100) NOT NULL,
  `companydetails_pincode` varchar(100) NOT NULL,
  `companydetails_logo` varchar(100) NOT NULL,
  `companydetails_startedon` varchar(11) NOT NULL,
  `companydetails_createdon` datetime NOT NULL,
  `companydetails_category` varchar(500) NOT NULL,
  `companydetails_noofemp` varchar(100) NOT NULL,
  `companydetails_branchname` varchar(100) NOT NULL,
  `companydetails_technologies` varchar(1000) NOT NULL,
  `companydetails_mobile` varchar(100) NOT NULL,
  `companydetails_website` varchar(100) NOT NULL,
  `companydetails_recruiterid` varchar(100) NOT NULL,
  `companydetails_companyrecruitertype` enum('Self','Consultant') NOT NULL,
  `companydetails_contactperson` varchar(100) NOT NULL,
  `companydetails_contactmobile` varchar(100) NOT NULL,
  `companydetails_contactemail` varchar(100) NOT NULL,
  `companydetails_status` enum('active','inactive','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companydetails`
--

INSERT INTO `companydetails` (`companydetails_slno`, `companydetails_companyid`, `companydetails_name`, `companydetails_descritption`, `companydetails_houseno`, `companydetails_street`, `companydetails_locality`, `companydetails_city`, `companydetails_state`, `companydetails_country`, `companydetails_pincode`, `companydetails_logo`, `companydetails_startedon`, `companydetails_createdon`, `companydetails_category`, `companydetails_noofemp`, `companydetails_branchname`, `companydetails_technologies`, `companydetails_mobile`, `companydetails_website`, `companydetails_recruiterid`, `companydetails_companyrecruitertype`, `companydetails_contactperson`, `companydetails_contactmobile`, `companydetails_contactemail`, `companydetails_status`) VALUES
(42, 'COMP0043', 'Logicprog', '<p>Sample Description</p>', '300-02', 'street1', 'Police station', 'Madhapur', 'Ts', 'india', '500007', 'COMP_22_26072017_011713ebc0b2.jpg', '1997', '2017-08-24 12:10:59', 'Web application Deveplopment', '500', 'Madhapur', 'Php, Node Js And java', '9989064364', 'www.tcs.com', 'ZOBEYER0066', 'Consultant', 'Koty', '7799679638', 'kotymca199@gmail.com', 'inactive'),
(43, 'COMP0044', 'Tcs', '<p>Sample</p>', '201-23', 'street2', 'It park', 'Hi-tech', 'Ts', 'india', '500009', 'COMP_89_24082017_1030194a8d81.jpg', '2000', '2017-08-24 13:15:23', 'Mobile application Deveplopment', '1000', 'Hitech-city', 'java,.net', '7799679638', 'www.techmahindra.com', 'ZOBEYER0066', 'Self', 'koty', '757456754745', 'kotymca199@gmail.com', 'inactive'),
(45, 'COMP0046', 'Techraq', '', '301-12', 'Ngri', 'Tarnaka', 'Hyderabad', 'ts', 'India', '524413', 'COMP_45_31082017_010218c0e989.png', '12', '0000-00-00 00:00:00', 'It', '400', '5655', '', '7799679638', 'www.techraq.com', 'ZOBEYER0056', 'Consultant', 'Koty', '7799679638', 'techraq@gmail.com', 'active');

-- --------------------------------------------------------

--
-- Stand-in structure for view `condidate_jobandusersdetails`
-- (See below for the actual view)
--
CREATE TABLE `condidate_jobandusersdetails` (
`jobapplicants_slno` double
,`jobapplicants_jobpostingid` varchar(100)
,`jobapplicants_userid` varchar(100)
,`jobapplicants_status` enum('applied','shortlisted','rejected')
,`jobapplicants_rejectedmessage` varchar(500)
,`jobapplicants_appliedon` datetime
,`user_slno` double
,`user_userid` varchar(100)
,`user_fname` varchar(100)
,`user_lname` varchar(100)
,`user_email` varchar(100)
,`user_password` varchar(100)
,`user_mobile` varchar(12)
,`user_location` varchar(100)
,`user_image` varchar(100)
,`user_addedon` date
,`user_personalbackground` varchar(100)
,`user_preferedlocation` varchar(100)
,`user_jobtype` enum('permanent','temporary','','')
,`user_employementtype` enum('fulltime','parttime','','')
,`user_dateofbirth` datetime
,`user_gender` enum('male','female','other','')
,`user_meritalstatus` enum('married','unmarried','','')
,`user_mailingaddress` varchar(500)
,`user_fathername` varchar(100)
,`user_mothername` varchar(100)
,`user_securityquention` varchar(500)
,`user_securityanswer` varchar(500)
,`user_hashkey` varchar(200)
,`user_status` enum('active','inactive','','')
,`user_workststus` enum('Fresher','Experience')
,`user_blocked` enum('blocked','unblocked','','')
,`userqualification_slno` double
,`userqualification_userid` varchar(100)
,`userqualification_coursename` varchar(100)
,`userqualification_specialization` varchar(100)
,`userqualification_college` varchar(100)
,`userqualification_resume` varchar(100)
,`userqualification_coursetype` enum('regular','distance','','')
,`userqualification_startingmonth` varchar(20)
,`userqualification_startingyear` varchar(20)
,`userqualification_passingmonth` varchar(20)
,`userqualification_passingyear` varchar(20)
,`userqualification_industry` varchar(100)
,`userqualification_grade` varchar(100)
,`userqualification_addedon` date
);

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `experience_slno` double NOT NULL,
  `experience_userid` varchar(100) NOT NULL,
  `experience_companyname` varchar(100) NOT NULL,
  `experience_designation` varchar(100) NOT NULL,
  `experience_startmonth` int(11) NOT NULL,
  `experience_startyear` int(11) NOT NULL,
  `experience_endmonth` int(11) NOT NULL,
  `experience_endyear` int(11) NOT NULL,
  `experience_addedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`experience_slno`, `experience_userid`, `experience_companyname`, `experience_designation`, `experience_startmonth`, `experience_startyear`, `experience_endmonth`, `experience_endyear`, `experience_addedon`) VALUES
(1, 'USER0006', 'Rolyal Technologies Pvt.LTD', 'WEbUIDeveloper', 5, 2007, 4, 2010, '2017-09-14 00:00:00'),
(2, 'USER0006', 'Innervalley Technologies', 'NodeJs Developer', 3, 2011, 4, 2015, '2017-09-14 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `googlecalender`
--

CREATE TABLE `googlecalender` (
  `googlecalender_sno` int(11) NOT NULL,
  `googlecalender_title` varchar(100) NOT NULL,
  `googlecalender_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `googlecalender`
--

INSERT INTO `googlecalender` (`googlecalender_sno`, `googlecalender_title`, `googlecalender_date`) VALUES
(1, 'koty', '2017-09-04 00:00:00'),
(12, '', '0000-00-00 00:00:00'),
(13, '', '0000-00-00 00:00:00'),
(14, '', '2017-09-05 15:40:30'),
(15, '', '2017-09-05 15:40:55'),
(16, '', '2017-09-05 15:43:16'),
(17, 'hgjhg', '2017-09-05 15:46:55'),
(18, 'tyt', '2017-09-05 15:47:37'),
(19, '', '2017-09-05 15:48:37'),
(20, 'jhgjhg', '2017-09-05 15:49:35'),
(21, '', '2017-09-05 15:56:56'),
(22, '', '2017-09-05 15:57:26'),
(23, '', '2017-09-05 15:57:33'),
(24, '', '2017-09-05 15:59:24'),
(25, '', '2017-09-05 16:00:15'),
(26, '', '2017-09-05 16:00:21'),
(27, '', '2017-09-05 16:00:47'),
(28, '', '2017-09-05 16:01:09'),
(29, '', '2017-09-05 16:01:10'),
(30, '', '2017-09-05 16:01:11'),
(31, '', '2017-09-05 16:01:11'),
(32, '', '2017-09-05 16:01:11'),
(33, '', '2017-09-05 16:01:41'),
(34, '', '2017-09-05 16:01:43'),
(35, '', '2017-09-05 16:02:07'),
(36, '', '2017-09-05 16:02:17'),
(37, '', '2017-09-05 16:02:22'),
(38, '', '2017-09-05 16:02:36'),
(39, '', '2017-09-05 16:02:54'),
(40, '', '2017-09-05 16:03:54');

-- --------------------------------------------------------

--
-- Table structure for table `interviewstatus`
--

CREATE TABLE `interviewstatus` (
  `interviewstatus_slno` double NOT NULL,
  `interviewstatus_id` varchar(100) NOT NULL,
  `interviewstatus_jobid` varchar(100) NOT NULL,
  `interviewstatus_userid` varchar(100) NOT NULL,
  `interviewstatus_interviewername` varchar(100) NOT NULL,
  `interviewstatus_round` varchar(100) NOT NULL,
  `interviewstatus_score` varchar(100) NOT NULL,
  `interviewstatus_status` enum('Not Selected','Selected') NOT NULL DEFAULT 'Not Selected',
  `interviewstatus_addedon` datetime NOT NULL,
  `interviewstatus_interviewstatus` enum('Process','qualified','disqualified') NOT NULL DEFAULT 'Process'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `interviewstatus`
--

INSERT INTO `interviewstatus` (`interviewstatus_slno`, `interviewstatus_id`, `interviewstatus_jobid`, `interviewstatus_userid`, `interviewstatus_interviewername`, `interviewstatus_round`, `interviewstatus_score`, `interviewstatus_status`, `interviewstatus_addedon`, `interviewstatus_interviewstatus`) VALUES
(1, 'IMODE0002', 'JOBID0092', 'USER0001', 'Koti malli', 'Technical Round', '10/5', 'Not Selected', '2017-07-03 00:00:00', 'Process'),
(2, 'IMODE0003', 'JOBID0002', 'USER0005', 'ram kumasr', 'Communication round', '10/8', 'Selected', '2017-07-19 00:00:00', 'disqualified'),
(3, 'IMODE0004', 'JOBID0002', 'USER0005', 'mohan', 'Technical Round', '10/4', 'Not Selected', '2017-07-13 18:12:52', 'disqualified'),
(4, 'IMODE0005', 'JOBID0003', 'USER0006', 'ashok', 'Group discussion', '10/3', 'Not Selected', '2017-07-13 18:13:07', 'disqualified'),
(5, 'IMODE0006', 'JOBID0092', 'USER0006', 'Rakesh kumar', 'Jam Round', '10/7', 'Not Selected', '2017-07-13 12:08:08', 'disqualified'),
(6, 'IMODE0007', 'JOBID0002', 'USER0005', 'Mukesh raja', 'aptitude test', '10/6', 'Selected', '2017-07-13 12:08:58', 'disqualified'),
(7, 'IMODE0008', 'JOBID0002', 'USER0006', 'P Chitambram', 'Group discussion', '10/5', 'Selected', '2017-07-15 12:33:13', 'disqualified'),
(8, 'IMODE0009', 'JOBID0092', 'USER0005', '', 'written', '8', 'Selected', '2017-09-18 16:36:58', 'Process'),
(9, 'IMODE0010', 'JOBID0092', 'USER0007', 'Malli', 'Face to Face', '7', 'Selected', '2017-09-18 17:26:13', 'Process');

-- --------------------------------------------------------

--
-- Table structure for table `jobapplicants`
--

CREATE TABLE `jobapplicants` (
  `jobapplicants_slno` double NOT NULL,
  `jobapplicants_jobpostingid` varchar(100) NOT NULL,
  `jobapplicants_userid` varchar(100) NOT NULL,
  `jobapplicants_status` enum('applied','shortlisted','rejected') NOT NULL,
  `jobapplicants_rejectedmessage` varchar(500) NOT NULL,
  `jobapplicants_appliedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobapplicants`
--

INSERT INTO `jobapplicants` (`jobapplicants_slno`, `jobapplicants_jobpostingid`, `jobapplicants_userid`, `jobapplicants_status`, `jobapplicants_rejectedmessage`, `jobapplicants_appliedon`) VALUES
(4, 'JOBID0085', 'USER0005', 'applied', 'sorry', '2017-07-11 18:59:18'),
(5, 'JOBID0091', 'USER0006', 'applied', 'Sorry Babu Your Rejected', '2017-07-11 18:59:34'),
(6, 'JOBID0092', 'USER0005', 'applied', 'dsfsd', '2017-07-11 19:00:42'),
(7, 'JOBID0089', 'USER0006', 'applied', 'NOt good', '2017-07-11 19:04:10'),
(8, 'JOBID0091', 'USER0007', 'shortlisted', 'Sorry Babu Your Rejected', '2017-07-17 10:48:00'),
(9, 'JOBID0090', 'USER0007', 'applied', 'Bhanu prakash have no any experience so that ', '2017-08-04 02:04:14'),
(10, 'JOBID0005', 'USER0005', 'applied', '', '2017-08-04 02:04:14'),
(11, 'JOBID0056', 'USER0006', 'applied', '', '2017-08-04 02:04:14'),
(12, 'JOBID0082', 'USER0007', 'applied', 'sorry babu we need node js', '2017-08-04 02:04:14'),
(15, 'JOBID0092', 'USER0007', 'applied', 'dsfsd', '2017-08-21 15:55:49'),
(16, 'JOBID0093', 'USER00013', 'applied', '', '2017-08-21 16:10:07'),
(17, 'JOBID0091', 'USER00013', 'applied', 'Sorry Babu Your Rejected', '2017-08-21 18:24:14'),
(19, 'JOBID0092', 'USER0006', 'applied', 'Rejected banu', '2017-08-22 12:13:04');

-- --------------------------------------------------------

--
-- Stand-in structure for view `jobdetails_view`
-- (See below for the actual view)
--
CREATE TABLE `jobdetails_view` (
`joblist_slno` double
,`joblist_id` varchar(20)
,`joblist_jobtitle` varchar(100)
,`joblist_companyid` varchar(200)
,`joblist_employementjobtype` enum('temperory','permanent','','')
,`joblist_category` varchar(100)
,`joblist_interviewmode` varchar(100)
,`joblist_noofround` int(100)
,`joblist_jobdescription` varchar(1000)
,`joblist_jobqualification` varchar(100)
,`joblist_additionalinf` varchar(1000)
,`joblist_noofrequirements` varchar(100)
,`joblist_experience` varchar(30)
,`joblist_salary` varchar(100)
,`joblist_createdon` datetime
,`joblist_skills` varchar(1000)
,`jobist_recuiterid` varchar(100)
,`jobist_designation` varchar(500)
,`joblist_functionalarea` varchar(500)
,`jobist_jobpostingenddate` date
,`joblist_logo` varchar(200)
,`companydetails_slno` double
,`companydetails_companyid` varchar(24)
,`companydetails_name` varchar(100)
,`companydetails_descritption` varchar(10000)
,`companydetails_houseno` varchar(100)
,`companydetails_street` varchar(100)
,`companydetails_locality` varchar(100)
,`companydetails_city` varchar(100)
,`companydetails_state` varchar(100)
,`companydetails_country` varchar(100)
,`companydetails_pincode` varchar(100)
,`companydetails_logo` varchar(100)
,`companydetails_startedon` varchar(11)
,`companydetails_createdon` datetime
,`companydetails_category` varchar(500)
,`companydetails_noofemp` varchar(100)
,`companydetails_branchname` varchar(100)
,`companydetails_technologies` varchar(1000)
,`companydetails_mobile` varchar(100)
,`companydetails_website` varchar(100)
,`companydetails_recruiterid` varchar(100)
,`companydetails_companyrecruitertype` enum('Self','Consultant')
,`companydetails_contactperson` varchar(100)
,`companydetails_contactmobile` varchar(100)
,`companydetails_contactemail` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `joblist`
--

CREATE TABLE `joblist` (
  `joblist_slno` double NOT NULL,
  `joblist_id` varchar(20) NOT NULL,
  `joblist_jobtitle` varchar(100) NOT NULL,
  `joblist_companyid` varchar(200) NOT NULL,
  `joblist_employementjobtype` enum('temperory','permanent','','') NOT NULL,
  `joblist_category` varchar(100) NOT NULL,
  `joblist_interviewmode` varchar(100) NOT NULL,
  `joblist_noofround` int(100) NOT NULL,
  `joblist_jobdescription` varchar(1000) NOT NULL,
  `joblist_jobqualification` varchar(100) NOT NULL,
  `joblist_additionalinf` varchar(1000) NOT NULL,
  `joblist_noofrequirements` varchar(100) NOT NULL,
  `joblist_experience` varchar(30) NOT NULL,
  `joblist_salary` varchar(100) NOT NULL,
  `joblist_createdon` datetime NOT NULL,
  `joblist_skills` varchar(1000) NOT NULL,
  `jobist_recuiterid` varchar(100) NOT NULL,
  `jobist_designation` varchar(500) NOT NULL,
  `joblist_functionalarea` varchar(500) NOT NULL,
  `jobist_jobpostingenddate` date NOT NULL,
  `joblist_logo` varchar(200) NOT NULL,
  `joblist_status` enum('ACTIVE','INACTIVE','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `joblist`
--

INSERT INTO `joblist` (`joblist_slno`, `joblist_id`, `joblist_jobtitle`, `joblist_companyid`, `joblist_employementjobtype`, `joblist_category`, `joblist_interviewmode`, `joblist_noofround`, `joblist_jobdescription`, `joblist_jobqualification`, `joblist_additionalinf`, `joblist_noofrequirements`, `joblist_experience`, `joblist_salary`, `joblist_createdon`, `joblist_skills`, `jobist_recuiterid`, `jobist_designation`, `joblist_functionalarea`, `jobist_jobpostingenddate`, `joblist_logo`, `joblist_status`) VALUES
(90, 'JOBID0091', 'software developer', 'COMP0043', 'temperory', 'IT', 'Java', 2, '<p>Sample Description</p>', 'AK', '', '4', '1-2years', '3.0', '2017-08-29 10:55:12', 'C++', 'ZOBEYER0066', 'Developer', 'AK', '2017-08-21', 'COMP_90_29082017_072512f907d9.jpg', 'ACTIVE'),
(91, 'JOBID0092', 'Java Developer', 'COMP0043', 'permanent', 'IT', 'Written,Face to Face,Hr,manager', 3, '<p>Sample Description</p>', 'HI', '', '5', '0-3years', '3.0', '2017-08-29 18:13:23', 'Java,C,Javascript', 'ZOBEYER0066', 'Associate Software Developer', 'AK', '2017-08-31', 'COMP_91_29082017_024323fe90db.jpg', 'ACTIVE'),
(97, 'JOBID0093', 'Tester', 'COMP0045', 'permanent', 'IT', 'Jam,Technical,Hr', 2, '<p>Sample Description</p>', 'AK', '', '4', '3-3years', '3.0', '2017-08-30 16:42:27', 'Java,C++', 'ZOBEYER0056', 'Developer', 'AK', '2017-08-21', 'COMP_97_30082017_011227e730e3.jpg', 'ACTIVE'),
(112, 'JOBID0085', 'klkjl', 'COMP0043', 'temperory', 'IT', 'Technical,Hr,manager', 2, '', 'AK', '', '6', '0-1years', '6', '2017-08-31 13:03:44', 'Java', 'ZOBEYER0056', 'hjh', 'AK', '2017-07-31', 'COMP_112_31082017_0933445bd511.jpg', 'ACTIVE'),
(113, 'JOBID0082', '', 'Self', '', 'IT', '', 0, '', 'AK', '', '', 'sele-seleyears', '', '2017-09-01 12:12:19', '', 'ZOBEYER0056', '', 'AK', '1970-01-01', '', 'ACTIVE'),
(114, 'JOBID0005', '', 'Self', '', 'IT', '', 0, '', 'AK', '', '', 'sele-seleyears', '', '2017-09-01 12:12:34', '', 'ZOBEYER0056', '', 'AK', '1970-01-01', '', 'ACTIVE');

-- --------------------------------------------------------

--
-- Stand-in structure for view `joblist_company`
-- (See below for the actual view)
--
CREATE TABLE `joblist_company` (
`joblist_slno` double
,`joblist_id` varchar(20)
,`joblist_jobtitle` varchar(100)
,`joblist_companyid` varchar(200)
,`joblist_employementjobtype` enum('temperory','permanent','','')
,`joblist_category` varchar(100)
,`joblist_interviewmode` varchar(100)
,`joblist_noofround` int(100)
,`joblist_jobdescription` varchar(1000)
,`joblist_jobqualification` varchar(100)
,`joblist_additionalinf` varchar(1000)
,`joblist_noofrequirements` varchar(100)
,`joblist_experience` varchar(30)
,`joblist_salary` varchar(100)
,`joblist_createdon` datetime
,`joblist_skills` varchar(1000)
,`jobist_recuiterid` varchar(100)
,`jobist_designation` varchar(500)
,`joblist_functionalarea` varchar(500)
,`jobist_jobpostingenddate` date
,`joblist_logo` varchar(200)
,`joblist_status` enum('ACTIVE','INACTIVE','','')
,`companydetails_slno` double
,`companydetails_companyid` varchar(24)
,`companydetails_name` varchar(100)
,`companydetails_descritption` varchar(10000)
,`companydetails_houseno` varchar(100)
,`companydetails_street` varchar(100)
,`companydetails_locality` varchar(100)
,`companydetails_city` varchar(100)
,`companydetails_state` varchar(100)
,`companydetails_country` varchar(100)
,`companydetails_pincode` varchar(100)
,`companydetails_logo` varchar(100)
,`companydetails_startedon` varchar(11)
,`companydetails_createdon` datetime
,`companydetails_category` varchar(500)
,`companydetails_noofemp` varchar(100)
,`companydetails_branchname` varchar(100)
,`companydetails_technologies` varchar(1000)
,`companydetails_mobile` varchar(100)
,`companydetails_website` varchar(100)
,`companydetails_recruiterid` varchar(100)
,`companydetails_companyrecruitertype` enum('Self','Consultant')
,`companydetails_contactperson` varchar(100)
,`companydetails_contactmobile` varchar(100)
,`companydetails_contactemail` varchar(100)
,`companydetails_status` enum('active','inactive','','')
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `joblist_jobapplicants`
-- (See below for the actual view)
--
CREATE TABLE `joblist_jobapplicants` (
`joblist_slno` double
,`joblist_id` varchar(20)
,`joblist_jobtitle` varchar(100)
,`joblist_companyid` varchar(200)
,`joblist_employementjobtype` enum('temperory','permanent','','')
,`joblist_category` varchar(100)
,`joblist_interviewmode` varchar(100)
,`joblist_noofround` int(100)
,`joblist_jobdescription` varchar(1000)
,`joblist_jobqualification` varchar(100)
,`joblist_additionalinf` varchar(1000)
,`joblist_noofrequirements` varchar(100)
,`joblist_experience` varchar(30)
,`joblist_salary` varchar(100)
,`joblist_createdon` datetime
,`joblist_skills` varchar(1000)
,`jobist_recuiterid` varchar(100)
,`jobist_designation` varchar(500)
,`joblist_functionalarea` varchar(500)
,`jobist_jobpostingenddate` date
,`joblist_logo` varchar(200)
,`jobapplicants_slno` double
,`jobapplicants_jobpostingid` varchar(100)
,`jobapplicants_userid` varchar(100)
,`jobapplicants_status` enum('applied','shortlisted','rejected')
,`jobapplicants_rejectedmessage` varchar(500)
,`jobapplicants_appliedon` datetime
);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `location_slno` int(10) NOT NULL,
  `location_id` varchar(100) NOT NULL,
  `location_name` varchar(100) NOT NULL,
  `location_doorno` varchar(1000) NOT NULL,
  `location_streetno` varchar(100) NOT NULL,
  `location_city` varchar(100) NOT NULL,
  `location_state` varchar(100) NOT NULL,
  `location_country` varchar(100) NOT NULL DEFAULT 'INDIA',
  `location_pincode` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location_slno`, `location_id`, `location_name`, `location_doorno`, `location_streetno`, `location_city`, `location_state`, `location_country`, `location_pincode`) VALUES
(1, 'mumbai123', 'Mumbai', '', '', 'Mumbai', 'Maharastra', 'INDIA', ''),
(2, 'Delhi1', 'Delhi', '', '', 'Delhi', 'Delhi', 'INDIA', ''),
(3, 'Kolkata1', 'Kolkata', '', '', 'Kolkata', 'WB', 'INDIA', ''),
(4, 'bengaluru1', 'Bengaluru', '', '', 'Bengaluru', 'Karnatka', 'INDIA', ''),
(5, 'channai1', 'Channai', '', '', 'Channai', 'Tamil naidu', 'INDIA', ''),
(6, 'hyderbad11', 'Hyderabad', '', '', 'Hyderabad', 'Hyderabad', 'INDIA', ''),
(7, 'ahmedabad1', 'Ahmedabad', '', '', 'Ahmedabad', 'Ahmedabad', 'INDIA', ''),
(8, 'pune1', 'Pune', '', '', 'Pune', 'Pune', 'INDIA', ''),
(9, 'surat2', 'Surat', '', '', 'Surat', 'Surat', 'INDIA', ''),
(10, 'visakhapatnam1', 'Visakhapatnam', '', '', 'Visakhapatnam', 'AP', 'INDIA', '');

-- --------------------------------------------------------

--
-- Table structure for table `mastercompanycategory`
--

CREATE TABLE `mastercompanycategory` (
  `mastercompanycategory_slno` double NOT NULL,
  `mastercompanycategory_id` varchar(100) NOT NULL,
  `mastercompanycategory_name` varchar(100) NOT NULL,
  `mastercompanycategory_code` varchar(100) NOT NULL,
  `mastercompanycategory_desc` varchar(500) NOT NULL,
  `mastercompanycategory_addedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mastercompanycategory`
--

INSERT INTO `mastercompanycategory` (`mastercompanycategory_slno`, `mastercompanycategory_id`, `mastercompanycategory_name`, `mastercompanycategory_code`, `mastercompanycategory_desc`, `mastercompanycategory_addedon`) VALUES
(1, 'MCC0001', 'IT', '', '', '2017-09-04 06:15:15'),
(2, 'MCC0002', 'NON-IT', '', '', '2017-09-04 17:35:41'),
(3, 'MCC0003', 'Salse', '', '', '2017-09-04 06:15:15'),
(4, 'MCC0004', 'Marketting', '', '', '2017-09-04 17:35:41'),
(5, 'MCC0005', 'Business', '', '', '2017-09-04 06:15:15'),
(6, 'MCC0006', 'Profitable', '', '', '2017-09-04 17:35:41'),
(7, 'MCC0007', 'Third Party', '', '', '2017-09-04 06:15:15'),
(8, 'MCC0008', 'Web Develipment', '', '', '2017-09-04 17:35:41'),
(9, 'MCC0009', 'Client support', '', '', '2017-09-04 06:15:15'),
(10, 'MCC0010', 'Indian', '', '', '2017-09-04 17:35:41'),
(11, 'MCC0012', 'Business', '', '', '2017-09-06 11:40:30'),
(12, 'MCC0013', 'Third party ', '', 'Which company are running for third party company', '2017-09-06 11:41:45'),
(14, 'MCC0014', 'murari', '', '', '2017-09-21 16:22:22'),
(15, 'MCC0016', 'koti', '', '', '2017-09-21 16:35:59'),
(16, 'MCC0017', 'Mani', '', '', '2017-09-21 16:37:45'),
(17, 'MCC0018', 'Mani', '', '', '2017-09-21 16:38:15'),
(18, 'MCC0019', 'Mani', '', '', '2017-09-21 16:38:35'),
(19, 'MCC0020', 'Mani', '', '', '2017-09-21 16:39:56'),
(20, 'MCC0021', 'Manikyam', '', '', '2017-09-21 16:57:57');

-- --------------------------------------------------------

--
-- Table structure for table `masterjobcategory`
--

CREATE TABLE `masterjobcategory` (
  `masterjobcategory_slno` double NOT NULL,
  `masterjobcategory_id` varchar(100) NOT NULL,
  `jobcategory_parentid` varchar(100) NOT NULL,
  `jobcategory_name` varchar(100) NOT NULL,
  `jobcategory_code` varchar(1000) NOT NULL,
  `jobcategory_description` varchar(100) NOT NULL,
  `jobcategory_createdon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masterjobcategory`
--

INSERT INTO `masterjobcategory` (`masterjobcategory_slno`, `masterjobcategory_id`, `jobcategory_parentid`, `jobcategory_name`, `jobcategory_code`, `jobcategory_description`, `jobcategory_createdon`) VALUES
(1, 'administration', '', 'administration', 'admin123', 'administration', '2017-07-06 00:00:00'),
(2, 'agriculture', '', 'agriculture', 'agri123', 'agriculture', '2017-07-06 00:00:00'),
(3, 'bpojobs', '', 'bpojobs', 'bpo123', 'bpojobs', '2017-07-06 00:00:00'),
(4, 'engineering', '', 'engineering', 'engineering123', 'engineering', '2017-07-06 00:00:00'),
(5, 'finance', '', 'finance', 'finance123', 'finance', '2017-07-06 00:00:00'),
(6, 'government', '', 'government', 'government123', 'government', '2017-07-06 00:00:00'),
(7, 'health', '', 'health', 'health123', 'health', '2017-07-06 00:00:00'),
(8, 'it', '', 'it', 'it123', 'it', '2017-07-06 00:00:00'),
(9, 'other', '', 'other', 'other', 'other123', '2017-07-06 00:00:00'),
(10, 'pharmacy', '', 'pharmacy', 'pharmacy123', 'pharmacy', '2017-07-06 00:00:00'),
(11, 'reearch&development', '', 'reearch&development', 'reearch123', 'reearch&development', '2017-07-06 00:00:00'),
(12, 'sales&marketing', '', 'sales&marketing', 'sales123', 'sales&marketing', '2017-07-06 00:00:00'),
(16, 'JOBCAT0017', '', 'Farmacy', '', '', '2017-09-06 11:46:21'),
(17, 'JOBCAT0018', '', 'Mgt', '', '', '2017-09-06 11:58:26'),
(0, 'JOBCAT0019', '', 'wsqewe', '', 'ewqewq', '2017-09-26 17:02:17'),
(0, 'JOBCAT0019', '', 'rtret', '', 'retre', '2017-09-26 17:03:03'),
(0, 'JOBCAT0019', '', 'reyty', '', 'trytr', '2017-09-26 17:03:20'),
(0, 'JOBCAT0019', '', 'reter', '', 'retre', '2017-09-26 17:03:38');

-- --------------------------------------------------------

--
-- Table structure for table `masterqualification`
--

CREATE TABLE `masterqualification` (
  `masterqualification_slno` double NOT NULL,
  `masterqualification_id` varchar(50) NOT NULL,
  `masterqualification_name` varchar(100) NOT NULL,
  `masterqualification_desc` varchar(500) NOT NULL,
  `masterqualification_addedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masterqualification`
--

INSERT INTO `masterqualification` (`masterqualification_slno`, `masterqualification_id`, `masterqualification_name`, `masterqualification_desc`, `masterqualification_addedon`) VALUES
(1, 'QUAL0002', 'MCA', 'master degree', '2017-09-06 11:22:09'),
(2, 'QUAL0003', 'BCA ', '', '2017-09-06 11:26:10'),
(3, 'QUAL0004', '10+2 (intermediate)', '', '2017-09-06 11:26:28'),
(4, 'QUAL0005', 'LLB', '', '2017-09-06 11:26:34'),
(5, 'QUAL0006', 'Msc', '', '2017-09-06 11:26:39'),
(6, 'QUAL0007', 'B tech', '', '2017-09-06 11:27:00'),
(7, 'QUAL0008', 'M Tech', '', '2017-09-06 11:27:07'),
(8, 'QUAL0009', 'Matriculation', '', '2017-09-06 11:27:16'),
(9, 'QUAL0010', 'Bsc (computer)', '', '2017-09-06 11:27:54'),
(10, 'QUAL0011', 'Bsc (Accounting)', '', '2017-09-06 11:28:05'),
(11, 'QUAL0012', 'MA ', '', '2017-09-06 11:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `masterskills`
--

CREATE TABLE `masterskills` (
  `masterskills_slno` double NOT NULL,
  `masterskills_id` varchar(50) NOT NULL,
  `masterskills_skillname` varchar(100) NOT NULL,
  `masterskills_desc` varchar(500) NOT NULL,
  `masterskills_addedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masterskills`
--

INSERT INTO `masterskills` (`masterskills_slno`, `masterskills_id`, `masterskills_skillname`, `masterskills_desc`, `masterskills_addedon`) VALUES
(1, 'PHP', 'PHP', '', '0000-00-00 00:00:00'),
(2, 'Java', 'Java', '', '0000-00-00 00:00:00'),
(4, 'C', 'C', '', '0000-00-00 00:00:00'),
(5, 'C++', 'C++', '', '0000-00-00 00:00:00'),
(6, 'Javascript', 'Javascript', '', '0000-00-00 00:00:00'),
(7, 'Oracle', 'Oracle', '', '0000-00-00 00:00:00'),
(8, 'Wordpress', 'Wordpress', '', '0000-00-00 00:00:00'),
(9, 'HTML', 'HTML', '', '0000-00-00 00:00:00'),
(10, 'XML', 'XML', '', '0000-00-00 00:00:00'),
(11, 'CSS', 'CSS', '', '0000-00-00 00:00:00'),
(12, 'Phyton', 'Phyton', '', '0000-00-00 00:00:00'),
(13, 'Anglar JS', 'Anglar JS', '', '0000-00-00 00:00:00'),
(14, 'JQuery', 'JQuery', '', '0000-00-00 00:00:00'),
(15, 'ASP', 'ASP', '', '0000-00-00 00:00:00'),
(16, 'SKILL0017', 'Visual Basic', 'Visual Basic for offline application', '2017-09-06 11:59:35'),
(17, 'SKILL0018', 'Python', 'Python', '2017-09-06 12:00:16');

-- --------------------------------------------------------

--
-- Table structure for table `mastertechnolgy`
--

CREATE TABLE `mastertechnolgy` (
  `mastertechnolgy_slno` double NOT NULL,
  `mastertechnolgy_id` varchar(100) NOT NULL,
  `mastertechnolgy_name` varchar(100) NOT NULL,
  `mastertechnolgy_code` varchar(100) NOT NULL,
  `mastertechnolgy_desc` varchar(500) NOT NULL,
  `mastertechnolgy_addedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mastertechnolgy`
--

INSERT INTO `mastertechnolgy` (`mastertechnolgy_slno`, `mastertechnolgy_id`, `mastertechnolgy_name`, `mastertechnolgy_code`, `mastertechnolgy_desc`, `mastertechnolgy_addedon`) VALUES
(1, 'MT0001', 'Java', '', '', '2017-09-04 04:00:19'),
(2, 'MT0001', 'PHP', '', '', '2017-09-04 02:11:14'),
(3, 'MT0003', '.NET Tecnology', '', '', '2017-09-04 04:00:19'),
(4, 'MT0004', 'Short hanad', '', '', '2017-09-04 02:11:14'),
(5, 'MT0005', 'Android', '', '', '2017-09-04 04:00:19'),
(6, 'MT0006', 'Python', '', '', '2017-09-04 02:11:14');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notification_slno` double NOT NULL,
  `notification_id` varchar(100) NOT NULL,
  `notification_recruiterid` varchar(100) NOT NULL,
  `notification_message` varchar(300) NOT NULL,
  `notification_recruitername` varchar(100) NOT NULL,
  `notification_addedtype` varchar(100) NOT NULL,
  `notification_addedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notification_slno`, `notification_id`, `notification_recruiterid`, `notification_message`, `notification_recruitername`, `notification_addedtype`, `notification_addedon`) VALUES
(2, 'MCC0003', 'ZOBEYER0066', 'this is koty added category', '', '', '2017-09-21 16:39:56'),
(3, 'MCC0004', 'ZOBEYER0066', 'this is koty added category', '', '', '2017-09-21 16:57:57');

-- --------------------------------------------------------

--
-- Table structure for table `offerletter`
--

CREATE TABLE `offerletter` (
  `offerletter_slno` double NOT NULL,
  `offerletter_id` varchar(50) NOT NULL,
  `offerletter_candidateusername` varchar(50) NOT NULL,
  `offerletter_jobsid` varchar(100) NOT NULL,
  `offerletter_candidatename` varchar(100) NOT NULL,
  `offerletter_email` varchar(100) NOT NULL,
  `offerletter_mobileno` varchar(100) NOT NULL,
  `offerletter_message` varchar(2000) NOT NULL,
  `offerletter_designation` varchar(100) NOT NULL,
  `offerletter_refname` varchar(100) NOT NULL,
  `offerletter_acceptedby` varchar(100) NOT NULL,
  `offerletter_generatedate` varchar(100) NOT NULL,
  `offerletter_createdon` varchar(100) NOT NULL,
  `offerletter_joiningdate` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `offerletter_view`
-- (See below for the actual view)
--
CREATE TABLE `offerletter_view` (
`interviewstatus_slno` double
,`interviewstatus_id` varchar(100)
,`interviewstatus_jobid` varchar(100)
,`interviewstatus_userid` varchar(100)
,`interviewstatus_interviewername` varchar(100)
,`interviewstatus_round` varchar(100)
,`interviewstatus_score` varchar(100)
,`interviewstatus_status` enum('Not Selected','Selected')
,`interviewstatus_addedon` datetime
,`interviewstatus_interviewstatus` enum('Process','qualified','disqualified')
,`user_slno` double
,`user_userid` varchar(100)
,`user_fname` varchar(100)
,`user_lname` varchar(100)
,`user_email` varchar(100)
,`user_password` varchar(100)
,`user_mobile` varchar(12)
,`user_location` varchar(100)
,`user_image` varchar(100)
,`user_addedon` date
,`user_personalbackground` varchar(100)
,`user_preferedlocation` varchar(100)
,`user_jobtype` enum('permanent','temporary','','')
,`user_employementtype` enum('fulltime','parttime','','')
,`user_dateofbirth` datetime
,`user_gender` enum('male','female','other','')
,`user_meritalstatus` enum('married','unmarried','','')
,`user_mailingaddress` varchar(500)
,`user_fathername` varchar(100)
,`user_mothername` varchar(100)
,`user_securityquention` varchar(500)
,`user_securityanswer` varchar(500)
,`user_hashkey` varchar(200)
,`user_status` enum('active','inactive','','')
,`user_workststus` enum('Fresher','Experience')
,`user_blocked` enum('blocked','unblocked','','')
);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `projects_slno` double NOT NULL,
  `projects_id` varchar(100) NOT NULL,
  `projects_title` varchar(100) NOT NULL,
  `projects_technlogies` varchar(100) NOT NULL,
  `projects_description` varchar(100) NOT NULL,
  `projects_clientname` varchar(100) NOT NULL,
  `projects_userid` varchar(100) NOT NULL,
  `projects_teamsize` varchar(20) NOT NULL,
  `project_duration` varchar(20) NOT NULL,
  `projects_createdon` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recruiter`
--

CREATE TABLE `recruiter` (
  `recruiter_slno` double NOT NULL,
  `recruiter_id` varchar(100) NOT NULL,
  `recruiter_firstname` varchar(50) NOT NULL,
  `recruiter_lastname` varchar(50) NOT NULL,
  `recruiter_designation` varchar(200) NOT NULL,
  `recruiter_mobileno` varchar(100) NOT NULL,
  `recruiter_emailid` varchar(100) NOT NULL,
  `recruiter_dob` date NOT NULL,
  `recruiter_username` varchar(100) NOT NULL,
  `recruiter_password` varchar(100) NOT NULL,
  `recruiter_industrytype` varchar(100) NOT NULL,
  `recruiter_companyname` varchar(200) NOT NULL,
  `recruiter_companyid` varchar(100) NOT NULL,
  `recruiter_specalityareas` varchar(100) NOT NULL,
  `recruiter_telephoneno` varchar(100) NOT NULL,
  `recruiter_aboutme` varchar(5000) NOT NULL,
  `recruiter_experience` varchar(100) NOT NULL,
  `recruiter_hashcode` varchar(100) NOT NULL,
  `recruiter_status` enum('unblocked','blocked') NOT NULL,
  `recruiter_blocked` varchar(100) NOT NULL,
  `recruiter_addedon` datetime NOT NULL,
  `recruiter_securityquestion` varchar(500) NOT NULL,
  `recruiter_securityanswer` varchar(100) NOT NULL,
  `recruiter_profilepic` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recruiter`
--

INSERT INTO `recruiter` (`recruiter_slno`, `recruiter_id`, `recruiter_firstname`, `recruiter_lastname`, `recruiter_designation`, `recruiter_mobileno`, `recruiter_emailid`, `recruiter_dob`, `recruiter_username`, `recruiter_password`, `recruiter_industrytype`, `recruiter_companyname`, `recruiter_companyid`, `recruiter_specalityareas`, `recruiter_telephoneno`, `recruiter_aboutme`, `recruiter_experience`, `recruiter_hashcode`, `recruiter_status`, `recruiter_blocked`, `recruiter_addedon`, `recruiter_securityquestion`, `recruiter_securityanswer`, `recruiter_profilepic`) VALUES
(64, 'ZOBEYER0056', 'mahesh', 'malli', 'engineer', '9000085879', 'mahendra@gmail.com', '1970-01-01', '', 'Koti123@', 'Industrytype0', 'Logicprog', 'COMP0044', 'Specality', '07799679638777', 'Sample Description1', '', '', 'unblocked', '', '2017-09-04 16:05:17', '', '', ''),
(65, 'ZOBEYER0066', 'koti', 'malli', 'Developer', '7799679638', 'kotymca199@gmail.com', '2017-10-09', '', 'Koty@123', 'Industrytype2', 'Logicprog', 'COMP0044', 'Non it', '07799679638777', 'Sample Description2', '', '', 'unblocked', '', '2017-09-04 17:07:00', '', '', 'COMP_ZOBEYER0066_20092017_1250358edb7b.png'),
(68, 'ZOBEYER0067', 'malli', 'mahi', 'software developer', '9989065324', 'koteswar@gmail.com', '2017-09-15', '', 'Koty@123', 'Industrytype1', 'Logicprog', 'COMP0044', 'It', '07799679638777', 'Sample Descriptions0', '', '', 'unblocked', '', '2017-09-19 11:43:52', '', '', ''),
(71, 'ZOBEYER0070', 'purushotham', 'manikanta', 'Designer', '8008120931', 'koty@gmail.com', '0000-00-00', '', 'Koty@123', 'Industrytype3', 'Logicprog', 'COMP0044', 'Skin', '07799679638777', 'Sample Description3', '', '', 'unblocked', '', '2017-09-19 12:04:47', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `recruiterexperience`
--

CREATE TABLE `recruiterexperience` (
  `recruiterexperience_slno` double NOT NULL,
  `recruiterexperience_recruiterid` varchar(100) NOT NULL,
  `recruiterexperience_companyname` varchar(100) NOT NULL,
  `recruiterexperience_companywebsite` varchar(100) NOT NULL,
  `recruiterexperience_from` varchar(100) NOT NULL,
  `recruiterexperience_todate` varchar(100) NOT NULL,
  `recruiterexperience_location` varchar(100) NOT NULL,
  `recruiterexperience_addedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recruiterexperience`
--

INSERT INTO `recruiterexperience` (`recruiterexperience_slno`, `recruiterexperience_recruiterid`, `recruiterexperience_companyname`, `recruiterexperience_companywebsite`, `recruiterexperience_from`, `recruiterexperience_todate`, `recruiterexperience_location`, `recruiterexperience_addedon`) VALUES
(61, 'ZOBEYER0066', 'Logicprog Technologies pvt Lmt', 'java developer', '09-05-2017', '09-05-2017', 'Hyderabad', '0000-00-00 00:00:00'),
(62, 'ZOBEYER0066', 'Wipro Technologies pvt Lmt', '.net developer', '09/21/2017', '09/21/2017', 'Secenderabad', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `recruiterportfolio`
--

CREATE TABLE `recruiterportfolio` (
  `recruiterportfolio_slno` double NOT NULL,
  `recruiterportfolio_recruiterid` varchar(100) NOT NULL,
  `recruiterportfolio_projectname` varchar(100) NOT NULL,
  `recruiterportfolio_projectdesc` varchar(100) NOT NULL,
  `recruiterportfolio_startdate` varchar(100) NOT NULL,
  `recruiterportfolio_enddate` varchar(100) NOT NULL,
  `recruiterportfolio_image` varchar(100) NOT NULL,
  `recruiterportfolio_facebook` varchar(100) NOT NULL,
  `recruiterportfolio_twitter` varchar(100) NOT NULL,
  `recruiterportfolio_linkedin` varchar(100) NOT NULL,
  `recruiterportfolio_googleplus` varchar(100) NOT NULL,
  `recruiterportfolio_addedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recruiterportfolio`
--

INSERT INTO `recruiterportfolio` (`recruiterportfolio_slno`, `recruiterportfolio_recruiterid`, `recruiterportfolio_projectname`, `recruiterportfolio_projectdesc`, `recruiterportfolio_startdate`, `recruiterportfolio_enddate`, `recruiterportfolio_image`, `recruiterportfolio_facebook`, `recruiterportfolio_twitter`, `recruiterportfolio_linkedin`, `recruiterportfolio_googleplus`, `recruiterportfolio_addedon`) VALUES
(39, 'ZOBEYER0066', 'artnweaves', 'Sample Description', '09-04-2017', '09-15-2017', '', 'facebook', 'twitter', 'linkedin', 'googleplus', '2017-09-14 12:54:14'),
(40, 'ZOBEYER0066', 'zobeye', 'description', '09/04/2017', '09/14/2017', '', 'facebook', 'twitter', 'linkedin', 'googleplus', '2017-09-14 12:54:14');

-- --------------------------------------------------------

--
-- Table structure for table `recruiterqualification`
--

CREATE TABLE `recruiterqualification` (
  `recruiterqualification_slno` double NOT NULL,
  `recruiterqualification_recruiterid` varchar(100) NOT NULL,
  `recruiterqualification_degreename` varchar(100) NOT NULL,
  `recruiterqualification_collegename` varchar(100) NOT NULL,
  `recruiterqualification_startdate` varchar(100) NOT NULL,
  `recruiterqualification_enddate` varchar(100) NOT NULL,
  `recruiterqualification_location` varchar(100) NOT NULL,
  `recruiterqualification_addedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recruiterqualification`
--

INSERT INTO `recruiterqualification` (`recruiterqualification_slno`, `recruiterqualification_recruiterid`, `recruiterqualification_degreename`, `recruiterqualification_collegename`, `recruiterqualification_startdate`, `recruiterqualification_enddate`, `recruiterqualification_location`, `recruiterqualification_addedon`) VALUES
(255, 'ZOBEYER0066', 'Msc', 'audisankara college', '09-12-2017', '09-22-2017', '', '0000-00-00 00:00:00'),
(256, 'ZOBEYER0066', 'mca', 'narayana college', '09/12/2017', '09/15/2017', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `recruiterskills`
--

CREATE TABLE `recruiterskills` (
  `recruiterskills_slno` double NOT NULL,
  `recruiterskills_recruiterid` varchar(100) NOT NULL,
  `recruiterskills_skillsname` varchar(100) NOT NULL,
  `recruiterskills_skilsllevel` varchar(100) NOT NULL,
  `recruiterskills_addedno` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recruiterskills`
--

INSERT INTO `recruiterskills` (`recruiterskills_slno`, `recruiterskills_recruiterid`, `recruiterskills_skillsname`, `recruiterskills_skilsllevel`, `recruiterskills_addedno`) VALUES
(95, 'ZOBEYER0066', 'dfsdsf', 'Low', '0000-00-00 00:00:00'),
(96, 'ZOBEYER0066', 'netdsfsd', 'Moderate', '0000-00-00 00:00:00'),
(160, 'ZOBEYER0066', 'dfssdsa', 'Low', '0000-00-00 00:00:00'),
(161, 'ZOBEYER0066', 'dffgfdg', 'Low', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `shortisting`
--

CREATE TABLE `shortisting` (
  `shortisting_slno` double NOT NULL,
  `shortisting_jobid` varchar(100) NOT NULL,
  `shortisting_userid` varchar(100) NOT NULL,
  `shortisting_addedon` datetime NOT NULL,
  `shortisting_recruiterid` varchar(100) NOT NULL,
  `shortisting_interviewername` varchar(100) NOT NULL,
  `shortisting_interviewmode` varchar(100) NOT NULL,
  `shortisting_sheduledate` date NOT NULL,
  `shortisting_interviewdate` date NOT NULL,
  `shortisting_interviewtime` varchar(100) NOT NULL,
  `shortlisting_status` enum('Pending','Process','Completed') NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shortisting`
--

INSERT INTO `shortisting` (`shortisting_slno`, `shortisting_jobid`, `shortisting_userid`, `shortisting_addedon`, `shortisting_recruiterid`, `shortisting_interviewername`, `shortisting_interviewmode`, `shortisting_sheduledate`, `shortisting_interviewdate`, `shortisting_interviewtime`, `shortlisting_status`) VALUES
(18, 'JOBID0023', 'USER0005', '2017-08-10 23:14:58', '', 'dsds', 'Stress Interviews', '2017-08-09', '2017-08-17', '03:02 PM', '');

-- --------------------------------------------------------

--
-- Stand-in structure for view `shortlisting_view`
-- (See below for the actual view)
--
CREATE TABLE `shortlisting_view` (
`user_slno` double
,`user_userid` varchar(100)
,`user_fname` varchar(100)
,`user_lname` varchar(100)
,`user_email` varchar(100)
,`user_password` varchar(100)
,`user_mobile` varchar(12)
,`user_location` varchar(100)
,`user_image` varchar(100)
,`user_addedon` date
,`user_personalbackground` varchar(100)
,`user_preferedlocation` varchar(100)
,`user_jobtype` enum('permanent','temporary','','')
,`user_employementtype` enum('fulltime','parttime','','')
,`user_dateofbirth` datetime
,`user_gender` enum('male','female','other','')
,`user_meritalstatus` enum('married','unmarried','','')
,`user_mailingaddress` varchar(500)
,`user_fathername` varchar(100)
,`user_mothername` varchar(100)
,`user_securityquention` varchar(500)
,`user_securityanswer` varchar(500)
,`user_hashkey` varchar(200)
,`user_status` enum('active','inactive','','')
,`user_workststus` enum('Fresher','Experience')
,`user_blocked` enum('blocked','unblocked','','')
,`jobapplicants_slno` double
,`jobapplicants_jobpostingid` varchar(100)
,`jobapplicants_userid` varchar(100)
,`jobapplicants_status` enum('applied','shortlisted','rejected')
,`jobapplicants_rejectedmessage` varchar(500)
,`jobapplicants_appliedon` datetime
,`userqualification_slno` double
,`userqualification_userid` varchar(100)
,`userqualification_coursename` varchar(100)
,`userqualification_specialization` varchar(100)
,`userqualification_college` varchar(100)
,`userqualification_resume` varchar(100)
,`userqualification_coursetype` enum('regular','distance','','')
,`userqualification_startingmonth` varchar(20)
,`userqualification_startingyear` varchar(20)
,`userqualification_passingmonth` varchar(20)
,`userqualification_passingyear` varchar(20)
,`userqualification_industry` varchar(100)
,`userqualification_grade` varchar(100)
,`userqualification_addedon` date
,`userlanguages_slno` double
,`userlanguages_id` varchar(100)
,`userlanguages_name` varchar(100)
,`userlanguages_userid` varchar(100)
,`userlanguages_addedon` varchar(100)
,`userlanguages_read` varchar(100)
,`userlanguages_write` varchar(100)
,`userlanguages_speak` varchar(100)
,`usercourse_slno` double
,`usercourse_userid` varchar(100)
,`usercourse_coursename` varchar(100)
,`usercourse_statrtmonth` int(11)
,`usercourse_satrtyear` int(11)
,`usercourse_endmonth` int(11)
,`usercourse_endyear` int(11)
,`usercourse_desc` varchar(500)
,`usercourse_institute` varchar(100)
,`usercourse_addedon` datetime
,`usercoursecertificate_slno` double
,`usercoursecertificate_userid` varchar(100)
,`usercoursecertificate_courseid` varchar(100)
,`usercoursecertificate_certificatename` varchar(100)
,`usercoursecertificate_date` date
,`usercoursecertificate_institute` varchar(100)
,`usercoursecertificate_addedon` datetime
,`experience_slno` double
,`experience_userid` varchar(100)
,`experience_companyname` varchar(100)
,`experience_designation` varchar(100)
,`experience_startmonth` int(11)
,`experience_startyear` int(11)
,`experience_endmonth` int(11)
,`experience_endyear` int(11)
,`experience_addedon` datetime
);

-- --------------------------------------------------------

--
-- Table structure for table `usercourse`
--

CREATE TABLE `usercourse` (
  `usercourse_slno` double NOT NULL,
  `usercourse_userid` varchar(100) NOT NULL,
  `usercourse_coursename` varchar(100) NOT NULL,
  `usercourse_statrtmonth` int(11) NOT NULL,
  `usercourse_satrtyear` int(11) NOT NULL,
  `usercourse_endmonth` int(11) NOT NULL,
  `usercourse_endyear` int(11) NOT NULL,
  `usercourse_desc` varchar(500) NOT NULL,
  `usercourse_institute` varchar(100) NOT NULL,
  `usercourse_addedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `usercoursecertificate`
--

CREATE TABLE `usercoursecertificate` (
  `usercoursecertificate_slno` double NOT NULL,
  `usercoursecertificate_userid` varchar(100) NOT NULL,
  `usercoursecertificate_courseid` varchar(100) NOT NULL,
  `usercoursecertificate_certificatename` varchar(100) NOT NULL,
  `usercoursecertificate_date` date NOT NULL,
  `usercoursecertificate_institute` varchar(100) NOT NULL,
  `usercoursecertificate_addedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userlanguages`
--

CREATE TABLE `userlanguages` (
  `userlanguages_slno` double NOT NULL,
  `userlanguages_id` varchar(100) NOT NULL,
  `userlanguages_name` varchar(100) NOT NULL,
  `userlanguages_userid` varchar(100) NOT NULL,
  `userlanguages_addedon` varchar(100) NOT NULL,
  `userlanguages_read` varchar(100) NOT NULL,
  `userlanguages_write` varchar(100) NOT NULL,
  `userlanguages_speak` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userqualification`
--

CREATE TABLE `userqualification` (
  `userqualification_slno` double NOT NULL,
  `userqualification_userid` varchar(100) NOT NULL,
  `userqualification_coursename` varchar(100) NOT NULL,
  `userqualification_specialization` varchar(100) NOT NULL,
  `userqualification_college` varchar(100) NOT NULL,
  `userqualification_resume` varchar(100) NOT NULL,
  `userqualification_coursetype` enum('regular','distance','','') NOT NULL,
  `userqualification_startingmonth` varchar(20) NOT NULL,
  `userqualification_startingyear` varchar(20) NOT NULL,
  `userqualification_passingmonth` varchar(20) NOT NULL,
  `userqualification_passingyear` varchar(20) NOT NULL,
  `userqualification_industry` varchar(100) NOT NULL,
  `userqualification_grade` varchar(100) NOT NULL,
  `userqualification_addedon` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userqualification`
--

INSERT INTO `userqualification` (`userqualification_slno`, `userqualification_userid`, `userqualification_coursename`, `userqualification_specialization`, `userqualification_college`, `userqualification_resume`, `userqualification_coursetype`, `userqualification_startingmonth`, `userqualification_startingyear`, `userqualification_passingmonth`, `userqualification_passingyear`, `userqualification_industry`, `userqualification_grade`, `userqualification_addedon`) VALUES
(22, 'USER0006', 'Bsc', 'Programmer', 'Avanthi PG College', 'Resume_23_14072017_021521e8ece9.doc', 'regular', '4', '2009', '6', '2012', 'It', 'A', '2017-07-14'),
(23, 'USER0006', 'Mca', 'Computers', 'Audisankara College Of Engineering', 'Resume_22_14072017_011407743859.doc', 'regular', '3', '2012', '7', '2015', 'It', 'C', '2017-07-14'),
(24, 'USER0006', 'Inter', 'Digital Marketting', 'Rolyal Engineering College and Science', 'KOTYRESUME.doc', 'distance', '3', '2007', '4', '2009', 'It', 'B', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_slno` double NOT NULL,
  `user_userid` varchar(100) NOT NULL,
  `user_fname` varchar(100) NOT NULL,
  `user_lname` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_mobile` varchar(12) NOT NULL,
  `user_location` varchar(100) NOT NULL,
  `user_image` varchar(100) NOT NULL,
  `user_addedon` date NOT NULL,
  `user_personalbackground` varchar(100) NOT NULL,
  `user_preferedlocation` varchar(100) NOT NULL,
  `user_jobtype` enum('permanent','temporary','','') NOT NULL,
  `user_employementtype` enum('fulltime','parttime','','') NOT NULL,
  `user_dateofbirth` datetime NOT NULL,
  `user_gender` enum('male','female','other','') NOT NULL,
  `user_meritalstatus` enum('married','unmarried','','') NOT NULL,
  `user_mailingaddress` varchar(500) NOT NULL,
  `user_fathername` varchar(100) NOT NULL,
  `user_mothername` varchar(100) NOT NULL,
  `user_securityquention` varchar(500) NOT NULL,
  `user_securityanswer` varchar(500) NOT NULL,
  `user_hashkey` varchar(200) NOT NULL,
  `user_status` enum('active','inactive','','') NOT NULL,
  `user_workststus` enum('Fresher','Experience') NOT NULL,
  `user_blocked` enum('blocked','unblocked','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_slno`, `user_userid`, `user_fname`, `user_lname`, `user_email`, `user_password`, `user_mobile`, `user_location`, `user_image`, `user_addedon`, `user_personalbackground`, `user_preferedlocation`, `user_jobtype`, `user_employementtype`, `user_dateofbirth`, `user_gender`, `user_meritalstatus`, `user_mailingaddress`, `user_fathername`, `user_mothername`, `user_securityquention`, `user_securityanswer`, `user_hashkey`, `user_status`, `user_workststus`, `user_blocked`) VALUES
(5, 'USER0005', 'induru', 'mahesh', 'mahesh@yopmail.com', 'ajaykumar', '9640303023', 'Hyderabad', 'lock_thumb.jpg', '2017-07-14', '', '', 'permanent', 'fulltime', '2017-07-20 00:00:00', 'male', 'married', '', 'aaa', 'bbb', '', '', '', 'active', 'Experience', 'blocked'),
(6, 'USER0006', 'madhu', 'babu', 'mahesh123@yopmail.com', 'ajaykumar', '9640303023', 'Hyderabad', '', '2017-07-14', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis laboriosam, nesciunt minus exce', '', 'permanent', 'fulltime', '2017-07-14 00:00:00', 'male', 'married', '', 'aaa', 'bbb', '', '', '', 'active', 'Fresher', 'blocked'),
(7, 'USER0007', 'Bhanu ', 'Prakash', 'bhanuprakash@gmail.com', 'bhanu', '9651235612', 'Hydearbad', 'lock_thumb.jpg', '2017-08-15', 'Have Three Wife and Seven Children ', 'Mumbai', 'temporary', 'parttime', '1982-08-12 00:00:00', 'male', 'married', '1-02-05-7B Hyderabade telangan India', 'Bhanu Prakash', 'aaa bkjfhes ', '', '', '', 'active', 'Experience', 'unblocked'),
(13, 'USER00013', 'Koty', 'Malli', 'kotymca199@gmail.com', 'kotimca', '7799679638', 'Hyderabad', 'User_5_14072017_02115886be07.jpg', '2017-08-21', '', '', 'permanent', 'fulltime', '2016-09-12 00:00:00', 'male', 'married', '', 'M ramaiah', 'Radha', '', '', '', 'active', 'Experience', 'blocked'),
(14, 'USER00014', 'Nani', 'Holi', 'nanu@gmail.com', 'koti', '7799679638', 'Chennai', 'User_5_14072017_02115886be07.jpg', '2017-08-22', '', '', 'permanent', 'fulltime', '2017-08-14 00:00:00', 'male', 'married', '', 'babu', 'kotimca', '', '', '', 'active', 'Fresher', 'blocked');

-- --------------------------------------------------------

--
-- Table structure for table `userskills`
--

CREATE TABLE `userskills` (
  `userskills_slno` double NOT NULL,
  `userskills_skillid` varchar(50) NOT NULL,
  `userskills_username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userskills`
--

INSERT INTO `userskills` (`userskills_slno`, `userskills_skillid`, `userskills_username`) VALUES
(1, 'PHP', 'USER0006'),
(2, 'Java', 'USER0006'),
(3, 'Javascript', 'USER0006'),
(4, 'Oracle', 'USER0006');

-- --------------------------------------------------------

--
-- Stand-in structure for view `users_alldetails`
-- (See below for the actual view)
--
CREATE TABLE `users_alldetails` (
`user_slno` double
,`user_userid` varchar(100)
,`user_fname` varchar(100)
,`user_lname` varchar(100)
,`user_email` varchar(100)
,`user_password` varchar(100)
,`user_mobile` varchar(12)
,`user_location` varchar(100)
,`user_image` varchar(100)
,`user_addedon` date
,`user_personalbackground` varchar(100)
,`user_preferedlocation` varchar(100)
,`user_jobtype` enum('permanent','temporary','','')
,`user_employementtype` enum('fulltime','parttime','','')
,`user_dateofbirth` datetime
,`user_gender` enum('male','female','other','')
,`user_meritalstatus` enum('married','unmarried','','')
,`user_mailingaddress` varchar(500)
,`user_fathername` varchar(100)
,`user_mothername` varchar(100)
,`user_securityquention` varchar(500)
,`user_securityanswer` varchar(500)
,`user_hashkey` varchar(200)
,`user_status` enum('active','inactive','','')
,`user_workststus` enum('Fresher','Experience')
,`user_blocked` enum('blocked','unblocked','','')
,`userqualification_slno` double
,`userqualification_userid` varchar(100)
,`userqualification_coursename` varchar(100)
,`userqualification_specialization` varchar(100)
,`userqualification_college` varchar(100)
,`userqualification_resume` varchar(100)
,`userqualification_coursetype` enum('regular','distance','','')
,`userqualification_startingmonth` varchar(20)
,`userqualification_startingyear` varchar(20)
,`userqualification_passingmonth` varchar(20)
,`userqualification_passingyear` varchar(20)
,`userqualification_industry` varchar(100)
,`userqualification_grade` varchar(100)
,`userqualification_addedon` date
,`jobapplicants_slno` double
,`jobapplicants_jobpostingid` varchar(100)
,`jobapplicants_userid` varchar(100)
,`jobapplicants_status` enum('applied','shortlisted','rejected')
,`jobapplicants_rejectedmessage` varchar(500)
,`jobapplicants_appliedon` datetime
);

-- --------------------------------------------------------

--
-- Structure for view `candidate_jobdetails`
--
DROP TABLE IF EXISTS `candidate_jobdetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `candidate_jobdetails`  AS  select `joblist`.`joblist_slno` AS `joblist_slno`,`joblist`.`joblist_id` AS `joblist_id`,`joblist`.`joblist_jobtitle` AS `joblist_jobtitle`,`joblist`.`joblist_companyid` AS `joblist_companyid`,`joblist`.`joblist_employementjobtype` AS `joblist_employementjobtype`,`joblist`.`joblist_category` AS `joblist_category`,`joblist`.`joblist_interviewmode` AS `joblist_interviewmode`,`joblist`.`joblist_noofround` AS `joblist_noofround`,`joblist`.`joblist_jobdescription` AS `joblist_jobdescription`,`joblist`.`joblist_jobqualification` AS `joblist_jobqualification`,`joblist`.`joblist_additionalinf` AS `joblist_additionalinf`,`joblist`.`joblist_noofrequirements` AS `joblist_noofrequirements`,`joblist`.`joblist_experience` AS `joblist_experience`,`joblist`.`joblist_salary` AS `joblist_salary`,`joblist`.`joblist_createdon` AS `joblist_createdon`,`joblist`.`joblist_skills` AS `joblist_skills`,`joblist`.`jobist_recuiterid` AS `jobist_recuiterid`,`joblist`.`jobist_designation` AS `jobist_designation`,`joblist`.`joblist_functionalarea` AS `joblist_functionalarea`,`joblist`.`jobist_jobpostingenddate` AS `jobist_jobpostingenddate`,`joblist`.`joblist_logo` AS `joblist_logo`,`jobapplicants`.`jobapplicants_slno` AS `jobapplicants_slno`,`jobapplicants`.`jobapplicants_jobpostingid` AS `jobapplicants_jobpostingid`,`jobapplicants`.`jobapplicants_userid` AS `jobapplicants_userid`,`jobapplicants`.`jobapplicants_status` AS `jobapplicants_status`,`jobapplicants`.`jobapplicants_rejectedmessage` AS `jobapplicants_rejectedmessage`,`jobapplicants`.`jobapplicants_appliedon` AS `jobapplicants_appliedon`,`users`.`user_slno` AS `user_slno`,`users`.`user_userid` AS `user_userid`,`users`.`user_fname` AS `user_fname`,`users`.`user_lname` AS `user_lname`,`users`.`user_email` AS `user_email`,`users`.`user_password` AS `user_password`,`users`.`user_mobile` AS `user_mobile`,`users`.`user_location` AS `user_location`,`users`.`user_image` AS `user_image`,`users`.`user_addedon` AS `user_addedon`,`users`.`user_personalbackground` AS `user_personalbackground`,`users`.`user_preferedlocation` AS `user_preferedlocation`,`users`.`user_jobtype` AS `user_jobtype`,`users`.`user_employementtype` AS `user_employementtype`,`users`.`user_dateofbirth` AS `user_dateofbirth`,`users`.`user_gender` AS `user_gender`,`users`.`user_meritalstatus` AS `user_meritalstatus`,`users`.`user_mailingaddress` AS `user_mailingaddress`,`users`.`user_fathername` AS `user_fathername`,`users`.`user_mothername` AS `user_mothername`,`users`.`user_securityquention` AS `user_securityquention`,`users`.`user_securityanswer` AS `user_securityanswer`,`users`.`user_hashkey` AS `user_hashkey`,`users`.`user_status` AS `user_status`,`users`.`user_workststus` AS `user_workststus`,`users`.`user_blocked` AS `user_blocked` from ((`joblist` join `jobapplicants` on((`joblist`.`joblist_id` = `jobapplicants`.`jobapplicants_jobpostingid`))) join `users` on((convert(`users`.`user_userid` using utf8) = `jobapplicants`.`jobapplicants_userid`))) ;

-- --------------------------------------------------------

--
-- Structure for view `condidate_jobandusersdetails`
--
DROP TABLE IF EXISTS `condidate_jobandusersdetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `condidate_jobandusersdetails`  AS  select `jobapplicants`.`jobapplicants_slno` AS `jobapplicants_slno`,`jobapplicants`.`jobapplicants_jobpostingid` AS `jobapplicants_jobpostingid`,`jobapplicants`.`jobapplicants_userid` AS `jobapplicants_userid`,`jobapplicants`.`jobapplicants_status` AS `jobapplicants_status`,`jobapplicants`.`jobapplicants_rejectedmessage` AS `jobapplicants_rejectedmessage`,`jobapplicants`.`jobapplicants_appliedon` AS `jobapplicants_appliedon`,`users`.`user_slno` AS `user_slno`,`users`.`user_userid` AS `user_userid`,`users`.`user_fname` AS `user_fname`,`users`.`user_lname` AS `user_lname`,`users`.`user_email` AS `user_email`,`users`.`user_password` AS `user_password`,`users`.`user_mobile` AS `user_mobile`,`users`.`user_location` AS `user_location`,`users`.`user_image` AS `user_image`,`users`.`user_addedon` AS `user_addedon`,`users`.`user_personalbackground` AS `user_personalbackground`,`users`.`user_preferedlocation` AS `user_preferedlocation`,`users`.`user_jobtype` AS `user_jobtype`,`users`.`user_employementtype` AS `user_employementtype`,`users`.`user_dateofbirth` AS `user_dateofbirth`,`users`.`user_gender` AS `user_gender`,`users`.`user_meritalstatus` AS `user_meritalstatus`,`users`.`user_mailingaddress` AS `user_mailingaddress`,`users`.`user_fathername` AS `user_fathername`,`users`.`user_mothername` AS `user_mothername`,`users`.`user_securityquention` AS `user_securityquention`,`users`.`user_securityanswer` AS `user_securityanswer`,`users`.`user_hashkey` AS `user_hashkey`,`users`.`user_status` AS `user_status`,`users`.`user_workststus` AS `user_workststus`,`users`.`user_blocked` AS `user_blocked`,`userqualification`.`userqualification_slno` AS `userqualification_slno`,`userqualification`.`userqualification_userid` AS `userqualification_userid`,`userqualification`.`userqualification_coursename` AS `userqualification_coursename`,`userqualification`.`userqualification_specialization` AS `userqualification_specialization`,`userqualification`.`userqualification_college` AS `userqualification_college`,`userqualification`.`userqualification_resume` AS `userqualification_resume`,`userqualification`.`userqualification_coursetype` AS `userqualification_coursetype`,`userqualification`.`userqualification_startingmonth` AS `userqualification_startingmonth`,`userqualification`.`userqualification_startingyear` AS `userqualification_startingyear`,`userqualification`.`userqualification_passingmonth` AS `userqualification_passingmonth`,`userqualification`.`userqualification_passingyear` AS `userqualification_passingyear`,`userqualification`.`userqualification_industry` AS `userqualification_industry`,`userqualification`.`userqualification_grade` AS `userqualification_grade`,`userqualification`.`userqualification_addedon` AS `userqualification_addedon` from ((`jobapplicants` join `users` on((`jobapplicants`.`jobapplicants_userid` = convert(`users`.`user_userid` using utf8)))) join `userqualification` on((`jobapplicants`.`jobapplicants_userid` = convert(`userqualification`.`userqualification_userid` using utf8)))) ;

-- --------------------------------------------------------

--
-- Structure for view `jobdetails_view`
--
DROP TABLE IF EXISTS `jobdetails_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jobdetails_view`  AS  select `joblist`.`joblist_slno` AS `joblist_slno`,`joblist`.`joblist_id` AS `joblist_id`,`joblist`.`joblist_jobtitle` AS `joblist_jobtitle`,`joblist`.`joblist_companyid` AS `joblist_companyid`,`joblist`.`joblist_employementjobtype` AS `joblist_employementjobtype`,`joblist`.`joblist_category` AS `joblist_category`,`joblist`.`joblist_interviewmode` AS `joblist_interviewmode`,`joblist`.`joblist_noofround` AS `joblist_noofround`,`joblist`.`joblist_jobdescription` AS `joblist_jobdescription`,`joblist`.`joblist_jobqualification` AS `joblist_jobqualification`,`joblist`.`joblist_additionalinf` AS `joblist_additionalinf`,`joblist`.`joblist_noofrequirements` AS `joblist_noofrequirements`,`joblist`.`joblist_experience` AS `joblist_experience`,`joblist`.`joblist_salary` AS `joblist_salary`,`joblist`.`joblist_createdon` AS `joblist_createdon`,`joblist`.`joblist_skills` AS `joblist_skills`,`joblist`.`jobist_recuiterid` AS `jobist_recuiterid`,`joblist`.`jobist_designation` AS `jobist_designation`,`joblist`.`joblist_functionalarea` AS `joblist_functionalarea`,`joblist`.`jobist_jobpostingenddate` AS `jobist_jobpostingenddate`,`joblist`.`joblist_logo` AS `joblist_logo`,`companydetails`.`companydetails_slno` AS `companydetails_slno`,`companydetails`.`companydetails_companyid` AS `companydetails_companyid`,`companydetails`.`companydetails_name` AS `companydetails_name`,`companydetails`.`companydetails_descritption` AS `companydetails_descritption`,`companydetails`.`companydetails_houseno` AS `companydetails_houseno`,`companydetails`.`companydetails_street` AS `companydetails_street`,`companydetails`.`companydetails_locality` AS `companydetails_locality`,`companydetails`.`companydetails_city` AS `companydetails_city`,`companydetails`.`companydetails_state` AS `companydetails_state`,`companydetails`.`companydetails_country` AS `companydetails_country`,`companydetails`.`companydetails_pincode` AS `companydetails_pincode`,`companydetails`.`companydetails_logo` AS `companydetails_logo`,`companydetails`.`companydetails_startedon` AS `companydetails_startedon`,`companydetails`.`companydetails_createdon` AS `companydetails_createdon`,`companydetails`.`companydetails_category` AS `companydetails_category`,`companydetails`.`companydetails_noofemp` AS `companydetails_noofemp`,`companydetails`.`companydetails_branchname` AS `companydetails_branchname`,`companydetails`.`companydetails_technologies` AS `companydetails_technologies`,`companydetails`.`companydetails_mobile` AS `companydetails_mobile`,`companydetails`.`companydetails_website` AS `companydetails_website`,`companydetails`.`companydetails_recruiterid` AS `companydetails_recruiterid`,`companydetails`.`companydetails_companyrecruitertype` AS `companydetails_companyrecruitertype`,`companydetails`.`companydetails_contactperson` AS `companydetails_contactperson`,`companydetails`.`companydetails_contactmobile` AS `companydetails_contactmobile`,`companydetails`.`companydetails_contactemail` AS `companydetails_contactemail` from (`joblist` join `companydetails` on((`joblist`.`joblist_companyid` = convert(`companydetails`.`companydetails_companyid` using utf8)))) ;

-- --------------------------------------------------------

--
-- Structure for view `joblist_company`
--
DROP TABLE IF EXISTS `joblist_company`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `joblist_company`  AS  select `joblist`.`joblist_slno` AS `joblist_slno`,`joblist`.`joblist_id` AS `joblist_id`,`joblist`.`joblist_jobtitle` AS `joblist_jobtitle`,`joblist`.`joblist_companyid` AS `joblist_companyid`,`joblist`.`joblist_employementjobtype` AS `joblist_employementjobtype`,`joblist`.`joblist_category` AS `joblist_category`,`joblist`.`joblist_interviewmode` AS `joblist_interviewmode`,`joblist`.`joblist_noofround` AS `joblist_noofround`,`joblist`.`joblist_jobdescription` AS `joblist_jobdescription`,`joblist`.`joblist_jobqualification` AS `joblist_jobqualification`,`joblist`.`joblist_additionalinf` AS `joblist_additionalinf`,`joblist`.`joblist_noofrequirements` AS `joblist_noofrequirements`,`joblist`.`joblist_experience` AS `joblist_experience`,`joblist`.`joblist_salary` AS `joblist_salary`,`joblist`.`joblist_createdon` AS `joblist_createdon`,`joblist`.`joblist_skills` AS `joblist_skills`,`joblist`.`jobist_recuiterid` AS `jobist_recuiterid`,`joblist`.`jobist_designation` AS `jobist_designation`,`joblist`.`joblist_functionalarea` AS `joblist_functionalarea`,`joblist`.`jobist_jobpostingenddate` AS `jobist_jobpostingenddate`,`joblist`.`joblist_logo` AS `joblist_logo`,`joblist`.`joblist_status` AS `joblist_status`,`companydetails`.`companydetails_slno` AS `companydetails_slno`,`companydetails`.`companydetails_companyid` AS `companydetails_companyid`,`companydetails`.`companydetails_name` AS `companydetails_name`,`companydetails`.`companydetails_descritption` AS `companydetails_descritption`,`companydetails`.`companydetails_houseno` AS `companydetails_houseno`,`companydetails`.`companydetails_street` AS `companydetails_street`,`companydetails`.`companydetails_locality` AS `companydetails_locality`,`companydetails`.`companydetails_city` AS `companydetails_city`,`companydetails`.`companydetails_state` AS `companydetails_state`,`companydetails`.`companydetails_country` AS `companydetails_country`,`companydetails`.`companydetails_pincode` AS `companydetails_pincode`,`companydetails`.`companydetails_logo` AS `companydetails_logo`,`companydetails`.`companydetails_startedon` AS `companydetails_startedon`,`companydetails`.`companydetails_createdon` AS `companydetails_createdon`,`companydetails`.`companydetails_category` AS `companydetails_category`,`companydetails`.`companydetails_noofemp` AS `companydetails_noofemp`,`companydetails`.`companydetails_branchname` AS `companydetails_branchname`,`companydetails`.`companydetails_technologies` AS `companydetails_technologies`,`companydetails`.`companydetails_mobile` AS `companydetails_mobile`,`companydetails`.`companydetails_website` AS `companydetails_website`,`companydetails`.`companydetails_recruiterid` AS `companydetails_recruiterid`,`companydetails`.`companydetails_companyrecruitertype` AS `companydetails_companyrecruitertype`,`companydetails`.`companydetails_contactperson` AS `companydetails_contactperson`,`companydetails`.`companydetails_contactmobile` AS `companydetails_contactmobile`,`companydetails`.`companydetails_contactemail` AS `companydetails_contactemail`,`companydetails`.`companydetails_status` AS `companydetails_status` from (`joblist` join `companydetails` on(((`joblist`.`joblist_companyid` = convert(`companydetails`.`companydetails_companyid` using utf8)) and (`joblist`.`jobist_recuiterid` = convert(`companydetails`.`companydetails_recruiterid` using utf8))))) ;

-- --------------------------------------------------------

--
-- Structure for view `joblist_jobapplicants`
--
DROP TABLE IF EXISTS `joblist_jobapplicants`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `joblist_jobapplicants`  AS  select `joblist`.`joblist_slno` AS `joblist_slno`,`joblist`.`joblist_id` AS `joblist_id`,`joblist`.`joblist_jobtitle` AS `joblist_jobtitle`,`joblist`.`joblist_companyid` AS `joblist_companyid`,`joblist`.`joblist_employementjobtype` AS `joblist_employementjobtype`,`joblist`.`joblist_category` AS `joblist_category`,`joblist`.`joblist_interviewmode` AS `joblist_interviewmode`,`joblist`.`joblist_noofround` AS `joblist_noofround`,`joblist`.`joblist_jobdescription` AS `joblist_jobdescription`,`joblist`.`joblist_jobqualification` AS `joblist_jobqualification`,`joblist`.`joblist_additionalinf` AS `joblist_additionalinf`,`joblist`.`joblist_noofrequirements` AS `joblist_noofrequirements`,`joblist`.`joblist_experience` AS `joblist_experience`,`joblist`.`joblist_salary` AS `joblist_salary`,`joblist`.`joblist_createdon` AS `joblist_createdon`,`joblist`.`joblist_skills` AS `joblist_skills`,`joblist`.`jobist_recuiterid` AS `jobist_recuiterid`,`joblist`.`jobist_designation` AS `jobist_designation`,`joblist`.`joblist_functionalarea` AS `joblist_functionalarea`,`joblist`.`jobist_jobpostingenddate` AS `jobist_jobpostingenddate`,`joblist`.`joblist_logo` AS `joblist_logo`,`jobapplicants`.`jobapplicants_slno` AS `jobapplicants_slno`,`jobapplicants`.`jobapplicants_jobpostingid` AS `jobapplicants_jobpostingid`,`jobapplicants`.`jobapplicants_userid` AS `jobapplicants_userid`,`jobapplicants`.`jobapplicants_status` AS `jobapplicants_status`,`jobapplicants`.`jobapplicants_rejectedmessage` AS `jobapplicants_rejectedmessage`,`jobapplicants`.`jobapplicants_appliedon` AS `jobapplicants_appliedon` from (`joblist` join `jobapplicants` on((`joblist`.`joblist_id` = `jobapplicants`.`jobapplicants_jobpostingid`))) ;

-- --------------------------------------------------------

--
-- Structure for view `offerletter_view`
--
DROP TABLE IF EXISTS `offerletter_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `offerletter_view`  AS  select `interviewstatus`.`interviewstatus_slno` AS `interviewstatus_slno`,`interviewstatus`.`interviewstatus_id` AS `interviewstatus_id`,`interviewstatus`.`interviewstatus_jobid` AS `interviewstatus_jobid`,`interviewstatus`.`interviewstatus_userid` AS `interviewstatus_userid`,`interviewstatus`.`interviewstatus_interviewername` AS `interviewstatus_interviewername`,`interviewstatus`.`interviewstatus_round` AS `interviewstatus_round`,`interviewstatus`.`interviewstatus_score` AS `interviewstatus_score`,`interviewstatus`.`interviewstatus_status` AS `interviewstatus_status`,`interviewstatus`.`interviewstatus_addedon` AS `interviewstatus_addedon`,`interviewstatus`.`interviewstatus_interviewstatus` AS `interviewstatus_interviewstatus`,`users`.`user_slno` AS `user_slno`,`users`.`user_userid` AS `user_userid`,`users`.`user_fname` AS `user_fname`,`users`.`user_lname` AS `user_lname`,`users`.`user_email` AS `user_email`,`users`.`user_password` AS `user_password`,`users`.`user_mobile` AS `user_mobile`,`users`.`user_location` AS `user_location`,`users`.`user_image` AS `user_image`,`users`.`user_addedon` AS `user_addedon`,`users`.`user_personalbackground` AS `user_personalbackground`,`users`.`user_preferedlocation` AS `user_preferedlocation`,`users`.`user_jobtype` AS `user_jobtype`,`users`.`user_employementtype` AS `user_employementtype`,`users`.`user_dateofbirth` AS `user_dateofbirth`,`users`.`user_gender` AS `user_gender`,`users`.`user_meritalstatus` AS `user_meritalstatus`,`users`.`user_mailingaddress` AS `user_mailingaddress`,`users`.`user_fathername` AS `user_fathername`,`users`.`user_mothername` AS `user_mothername`,`users`.`user_securityquention` AS `user_securityquention`,`users`.`user_securityanswer` AS `user_securityanswer`,`users`.`user_hashkey` AS `user_hashkey`,`users`.`user_status` AS `user_status`,`users`.`user_workststus` AS `user_workststus`,`users`.`user_blocked` AS `user_blocked` from (`interviewstatus` join `users` on((`interviewstatus`.`interviewstatus_userid` = `users`.`user_userid`))) ;

-- --------------------------------------------------------

--
-- Structure for view `shortlisting_view`
--
DROP TABLE IF EXISTS `shortlisting_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `shortlisting_view`  AS  select `users`.`user_slno` AS `user_slno`,`users`.`user_userid` AS `user_userid`,`users`.`user_fname` AS `user_fname`,`users`.`user_lname` AS `user_lname`,`users`.`user_email` AS `user_email`,`users`.`user_password` AS `user_password`,`users`.`user_mobile` AS `user_mobile`,`users`.`user_location` AS `user_location`,`users`.`user_image` AS `user_image`,`users`.`user_addedon` AS `user_addedon`,`users`.`user_personalbackground` AS `user_personalbackground`,`users`.`user_preferedlocation` AS `user_preferedlocation`,`users`.`user_jobtype` AS `user_jobtype`,`users`.`user_employementtype` AS `user_employementtype`,`users`.`user_dateofbirth` AS `user_dateofbirth`,`users`.`user_gender` AS `user_gender`,`users`.`user_meritalstatus` AS `user_meritalstatus`,`users`.`user_mailingaddress` AS `user_mailingaddress`,`users`.`user_fathername` AS `user_fathername`,`users`.`user_mothername` AS `user_mothername`,`users`.`user_securityquention` AS `user_securityquention`,`users`.`user_securityanswer` AS `user_securityanswer`,`users`.`user_hashkey` AS `user_hashkey`,`users`.`user_status` AS `user_status`,`users`.`user_workststus` AS `user_workststus`,`users`.`user_blocked` AS `user_blocked`,`jobapplicants`.`jobapplicants_slno` AS `jobapplicants_slno`,`jobapplicants`.`jobapplicants_jobpostingid` AS `jobapplicants_jobpostingid`,`jobapplicants`.`jobapplicants_userid` AS `jobapplicants_userid`,`jobapplicants`.`jobapplicants_status` AS `jobapplicants_status`,`jobapplicants`.`jobapplicants_rejectedmessage` AS `jobapplicants_rejectedmessage`,`jobapplicants`.`jobapplicants_appliedon` AS `jobapplicants_appliedon`,`userqualification`.`userqualification_slno` AS `userqualification_slno`,`userqualification`.`userqualification_userid` AS `userqualification_userid`,`userqualification`.`userqualification_coursename` AS `userqualification_coursename`,`userqualification`.`userqualification_specialization` AS `userqualification_specialization`,`userqualification`.`userqualification_college` AS `userqualification_college`,`userqualification`.`userqualification_resume` AS `userqualification_resume`,`userqualification`.`userqualification_coursetype` AS `userqualification_coursetype`,`userqualification`.`userqualification_startingmonth` AS `userqualification_startingmonth`,`userqualification`.`userqualification_startingyear` AS `userqualification_startingyear`,`userqualification`.`userqualification_passingmonth` AS `userqualification_passingmonth`,`userqualification`.`userqualification_passingyear` AS `userqualification_passingyear`,`userqualification`.`userqualification_industry` AS `userqualification_industry`,`userqualification`.`userqualification_grade` AS `userqualification_grade`,`userqualification`.`userqualification_addedon` AS `userqualification_addedon`,`userlanguages`.`userlanguages_slno` AS `userlanguages_slno`,`userlanguages`.`userlanguages_id` AS `userlanguages_id`,`userlanguages`.`userlanguages_name` AS `userlanguages_name`,`userlanguages`.`userlanguages_userid` AS `userlanguages_userid`,`userlanguages`.`userlanguages_addedon` AS `userlanguages_addedon`,`userlanguages`.`userlanguages_read` AS `userlanguages_read`,`userlanguages`.`userlanguages_write` AS `userlanguages_write`,`userlanguages`.`userlanguages_speak` AS `userlanguages_speak`,`usercourse`.`usercourse_slno` AS `usercourse_slno`,`usercourse`.`usercourse_userid` AS `usercourse_userid`,`usercourse`.`usercourse_coursename` AS `usercourse_coursename`,`usercourse`.`usercourse_statrtmonth` AS `usercourse_statrtmonth`,`usercourse`.`usercourse_satrtyear` AS `usercourse_satrtyear`,`usercourse`.`usercourse_endmonth` AS `usercourse_endmonth`,`usercourse`.`usercourse_endyear` AS `usercourse_endyear`,`usercourse`.`usercourse_desc` AS `usercourse_desc`,`usercourse`.`usercourse_institute` AS `usercourse_institute`,`usercourse`.`usercourse_addedon` AS `usercourse_addedon`,`usercoursecertificate`.`usercoursecertificate_slno` AS `usercoursecertificate_slno`,`usercoursecertificate`.`usercoursecertificate_userid` AS `usercoursecertificate_userid`,`usercoursecertificate`.`usercoursecertificate_courseid` AS `usercoursecertificate_courseid`,`usercoursecertificate`.`usercoursecertificate_certificatename` AS `usercoursecertificate_certificatename`,`usercoursecertificate`.`usercoursecertificate_date` AS `usercoursecertificate_date`,`usercoursecertificate`.`usercoursecertificate_institute` AS `usercoursecertificate_institute`,`usercoursecertificate`.`usercoursecertificate_addedon` AS `usercoursecertificate_addedon`,`experience`.`experience_slno` AS `experience_slno`,`experience`.`experience_userid` AS `experience_userid`,`experience`.`experience_companyname` AS `experience_companyname`,`experience`.`experience_designation` AS `experience_designation`,`experience`.`experience_startmonth` AS `experience_startmonth`,`experience`.`experience_startyear` AS `experience_startyear`,`experience`.`experience_endmonth` AS `experience_endmonth`,`experience`.`experience_endyear` AS `experience_endyear`,`experience`.`experience_addedon` AS `experience_addedon` from ((((((`users` join `jobapplicants` on((convert(`users`.`user_userid` using utf8) = `jobapplicants`.`jobapplicants_userid`))) join `userqualification` on((`jobapplicants`.`jobapplicants_userid` = convert(`userqualification`.`userqualification_userid` using utf8)))) join `userlanguages` on((`userqualification`.`userqualification_userid` = `userlanguages`.`userlanguages_userid`))) join `usercourse` on((`userlanguages`.`userlanguages_userid` = `usercourse`.`usercourse_userid`))) join `usercoursecertificate` on((`usercourse`.`usercourse_userid` = `usercoursecertificate`.`usercoursecertificate_userid`))) join `experience` on((`usercoursecertificate`.`usercoursecertificate_userid` = `experience`.`experience_userid`))) ;

-- --------------------------------------------------------

--
-- Structure for view `users_alldetails`
--
DROP TABLE IF EXISTS `users_alldetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_alldetails`  AS  select `users`.`user_slno` AS `user_slno`,`users`.`user_userid` AS `user_userid`,`users`.`user_fname` AS `user_fname`,`users`.`user_lname` AS `user_lname`,`users`.`user_email` AS `user_email`,`users`.`user_password` AS `user_password`,`users`.`user_mobile` AS `user_mobile`,`users`.`user_location` AS `user_location`,`users`.`user_image` AS `user_image`,`users`.`user_addedon` AS `user_addedon`,`users`.`user_personalbackground` AS `user_personalbackground`,`users`.`user_preferedlocation` AS `user_preferedlocation`,`users`.`user_jobtype` AS `user_jobtype`,`users`.`user_employementtype` AS `user_employementtype`,`users`.`user_dateofbirth` AS `user_dateofbirth`,`users`.`user_gender` AS `user_gender`,`users`.`user_meritalstatus` AS `user_meritalstatus`,`users`.`user_mailingaddress` AS `user_mailingaddress`,`users`.`user_fathername` AS `user_fathername`,`users`.`user_mothername` AS `user_mothername`,`users`.`user_securityquention` AS `user_securityquention`,`users`.`user_securityanswer` AS `user_securityanswer`,`users`.`user_hashkey` AS `user_hashkey`,`users`.`user_status` AS `user_status`,`users`.`user_workststus` AS `user_workststus`,`users`.`user_blocked` AS `user_blocked`,`userqualification`.`userqualification_slno` AS `userqualification_slno`,`userqualification`.`userqualification_userid` AS `userqualification_userid`,`userqualification`.`userqualification_coursename` AS `userqualification_coursename`,`userqualification`.`userqualification_specialization` AS `userqualification_specialization`,`userqualification`.`userqualification_college` AS `userqualification_college`,`userqualification`.`userqualification_resume` AS `userqualification_resume`,`userqualification`.`userqualification_coursetype` AS `userqualification_coursetype`,`userqualification`.`userqualification_startingmonth` AS `userqualification_startingmonth`,`userqualification`.`userqualification_startingyear` AS `userqualification_startingyear`,`userqualification`.`userqualification_passingmonth` AS `userqualification_passingmonth`,`userqualification`.`userqualification_passingyear` AS `userqualification_passingyear`,`userqualification`.`userqualification_industry` AS `userqualification_industry`,`userqualification`.`userqualification_grade` AS `userqualification_grade`,`userqualification`.`userqualification_addedon` AS `userqualification_addedon`,`jobapplicants`.`jobapplicants_slno` AS `jobapplicants_slno`,`jobapplicants`.`jobapplicants_jobpostingid` AS `jobapplicants_jobpostingid`,`jobapplicants`.`jobapplicants_userid` AS `jobapplicants_userid`,`jobapplicants`.`jobapplicants_status` AS `jobapplicants_status`,`jobapplicants`.`jobapplicants_rejectedmessage` AS `jobapplicants_rejectedmessage`,`jobapplicants`.`jobapplicants_appliedon` AS `jobapplicants_appliedon` from ((`users` join `userqualification` on((`users`.`user_userid` = `userqualification`.`userqualification_userid`))) join `jobapplicants` on((`jobapplicants`.`jobapplicants_userid` = convert(`users`.`user_userid` using utf8)))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companycontact`
--
ALTER TABLE `companycontact`
  ADD PRIMARY KEY (`companycontact_slno`);

--
-- Indexes for table `companydetails`
--
ALTER TABLE `companydetails`
  ADD PRIMARY KEY (`companydetails_slno`);

--
-- Indexes for table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`experience_slno`);

--
-- Indexes for table `googlecalender`
--
ALTER TABLE `googlecalender`
  ADD PRIMARY KEY (`googlecalender_sno`);

--
-- Indexes for table `interviewstatus`
--
ALTER TABLE `interviewstatus`
  ADD PRIMARY KEY (`interviewstatus_slno`);

--
-- Indexes for table `jobapplicants`
--
ALTER TABLE `jobapplicants`
  ADD PRIMARY KEY (`jobapplicants_slno`);

--
-- Indexes for table `joblist`
--
ALTER TABLE `joblist`
  ADD PRIMARY KEY (`joblist_slno`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`location_slno`);

--
-- Indexes for table `mastercompanycategory`
--
ALTER TABLE `mastercompanycategory`
  ADD PRIMARY KEY (`mastercompanycategory_slno`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_slno`);

--
-- Indexes for table `offerletter`
--
ALTER TABLE `offerletter`
  ADD PRIMARY KEY (`offerletter_slno`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`projects_slno`);

--
-- Indexes for table `recruiter`
--
ALTER TABLE `recruiter`
  ADD PRIMARY KEY (`recruiter_slno`);

--
-- Indexes for table `recruiterexperience`
--
ALTER TABLE `recruiterexperience`
  ADD PRIMARY KEY (`recruiterexperience_slno`);

--
-- Indexes for table `recruiterportfolio`
--
ALTER TABLE `recruiterportfolio`
  ADD PRIMARY KEY (`recruiterportfolio_slno`);

--
-- Indexes for table `recruiterqualification`
--
ALTER TABLE `recruiterqualification`
  ADD PRIMARY KEY (`recruiterqualification_slno`);

--
-- Indexes for table `recruiterskills`
--
ALTER TABLE `recruiterskills`
  ADD PRIMARY KEY (`recruiterskills_slno`);

--
-- Indexes for table `shortisting`
--
ALTER TABLE `shortisting`
  ADD PRIMARY KEY (`shortisting_slno`);

--
-- Indexes for table `usercourse`
--
ALTER TABLE `usercourse`
  ADD PRIMARY KEY (`usercourse_slno`);

--
-- Indexes for table `usercoursecertificate`
--
ALTER TABLE `usercoursecertificate`
  ADD PRIMARY KEY (`usercoursecertificate_slno`);

--
-- Indexes for table `userlanguages`
--
ALTER TABLE `userlanguages`
  ADD PRIMARY KEY (`userlanguages_slno`);

--
-- Indexes for table `userqualification`
--
ALTER TABLE `userqualification`
  ADD PRIMARY KEY (`userqualification_slno`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_slno`);

--
-- Indexes for table `userskills`
--
ALTER TABLE `userskills`
  ADD PRIMARY KEY (`userskills_slno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companycontact`
--
ALTER TABLE `companycontact`
  MODIFY `companycontact_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `companydetails`
--
ALTER TABLE `companydetails`
  MODIFY `companydetails_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `experience`
--
ALTER TABLE `experience`
  MODIFY `experience_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `googlecalender`
--
ALTER TABLE `googlecalender`
  MODIFY `googlecalender_sno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `interviewstatus`
--
ALTER TABLE `interviewstatus`
  MODIFY `interviewstatus_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `jobapplicants`
--
ALTER TABLE `jobapplicants`
  MODIFY `jobapplicants_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `joblist`
--
ALTER TABLE `joblist`
  MODIFY `joblist_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `location_slno` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mastercompanycategory`
--
ALTER TABLE `mastercompanycategory`
  MODIFY `mastercompanycategory_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notification_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `offerletter`
--
ALTER TABLE `offerletter`
  MODIFY `offerletter_slno` double NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `projects_slno` double NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recruiter`
--
ALTER TABLE `recruiter`
  MODIFY `recruiter_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `recruiterexperience`
--
ALTER TABLE `recruiterexperience`
  MODIFY `recruiterexperience_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `recruiterportfolio`
--
ALTER TABLE `recruiterportfolio`
  MODIFY `recruiterportfolio_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `recruiterqualification`
--
ALTER TABLE `recruiterqualification`
  MODIFY `recruiterqualification_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;
--
-- AUTO_INCREMENT for table `recruiterskills`
--
ALTER TABLE `recruiterskills`
  MODIFY `recruiterskills_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;
--
-- AUTO_INCREMENT for table `shortisting`
--
ALTER TABLE `shortisting`
  MODIFY `shortisting_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `usercourse`
--
ALTER TABLE `usercourse`
  MODIFY `usercourse_slno` double NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usercoursecertificate`
--
ALTER TABLE `usercoursecertificate`
  MODIFY `usercoursecertificate_slno` double NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userlanguages`
--
ALTER TABLE `userlanguages`
  MODIFY `userlanguages_slno` double NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userqualification`
--
ALTER TABLE `userqualification`
  MODIFY `userqualification_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `userskills`
--
ALTER TABLE `userskills`
  MODIFY `userskills_slno` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
